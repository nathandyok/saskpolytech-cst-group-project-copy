<?php

namespace App\tests\Entity;


use App\Entity\MemberPayment;

use Symfony\Bundle\FrameworkBundle\Tests\TestCase;


class ShowTest extends TestCase
{


    /*
     * This test will verify that the stripe token is valid
     * Cory N and Dylan S
     * March 27, 2019
     *
     * stripe_token valid example: tok_KPte7942xySKBKyrBu11yEpf
     */
    public function testStripeTokenValid()
    {
        $memberPayment = new MemberPayment();
        $memberPayment->setStripeToken("tok_KPte7942xySKBKyrBu11yEpf");

        $this->assertEquals("tok_KPte7942xySKBKyrBu11yEpf", $memberPayment->getStripeToken());
    }

    /*
     * This test will verify that the stripe token is too short
     * Cory N and Dylan S
     * March 27, 2019
     *
     * stripe_token valid example: tok_KPte7942xySKBKyrBu11yEpf
     */
    public function testStripeTokenTooShort()
    {
        $memberPayment = new MemberPayment();

        $this->expectException(\InvalidArgumentException::class);

        $memberPayment->setStripeToken("tok_KPte7942xySKBKyrBu11yEp");
        $this->assertEquals("", $memberPayment->getStripeToken());
    }

    /*
     * This test will verify that the stripe token is too long
     * Cory N and Dylan S
     * March 27, 2019
     *
     * stripe_token valid example: tok_KPte7942xySKBKyrBu11yEpf
     */
    public function testStripeTokenTooLong()
    {
        $memberPayment = new MemberPayment();

        $this->expectException(\InvalidArgumentException::class);
        $memberPayment->setStripeToken("tok_KPte7942xySKBKyrBu11yEpf7");
        $this->assertEquals("", $memberPayment->getStripeToken());
    }

    /*
     * This test will verify that the stripe token must begin with "tok_"
     * Cory N and Dylan S
     * March 27, 2019
     *
     * stripe_token valid example: tok_KPte7942xySKBKyrBu11yEpf
     */
    public function testStripeTokenDoesntStartWithtok_()
    {
        $memberPayment = new MemberPayment();

        $this->expectException(\InvalidArgumentException::class);
        $memberPayment->setStripeToken("tk_KPte7942xySKBKyrBu11yEpf");
        $this->assertEquals("", $memberPayment->getStripeToken());
    }

    /*
     * This test will verify that the stripe token must begin with lowercase "tok_"
     * Cory N and Dylan S
     * March 27, 2019
     *
     * stripe_token valid example: tok_KPte7942xySKBKyrBu11yEpf
     */
    public function testStripeTokenValidTokCaseSensitive()
    {
        $memberPayment = new MemberPayment();

        $this->expectException(\InvalidArgumentException::class);
        $memberPayment->setStripeToken("TOK_KPte7942xySKBKyrBu11yEpf");
        $this->assertEquals("", $memberPayment->getStripeToken());
    }

    /*
     * This test will verify that the stripe token cannot contain any spaces
     * Cory N and Dylan S
     * March 27, 2019
     *
     * stripe_token valid example: tok_KPte7942xySKBKyrBu11yEpf
     */
    public function testStripeTokenNoSpaces()
    {
        $memberPayment = new MemberPayment();

        $this->expectException(\InvalidArgumentException::class);
        $memberPayment->setStripeToken("tok_KPte79 42xySKBKyrBu11yEpf");
        $this->assertEquals("", $memberPayment->getStripeToken());
    }


    /*
     * This test will verify that the given customer ID is valid
     * Dylan S and Cory N
     * March 27, 2019
     * Example valid stripe_customer_id: cus_V9T7vofUbZMqpv
     */
    public function testCustIdValid()
    {
        $memberPayment = new MemberPayment();
        $memberPayment->setStripeCustomerId("cus_V9T7vofUbZMqpv");

        $this->assertEquals("cus_V9T7vofUbZMqpv", $memberPayment->getStripeCustomerId());
    }

    /*
     * This test will verify that the given customer ID is too long
     * Dylan S and Cory N
     * March 27, 2019
     * Example valid stripe_customer_id: cus_V9T7vofUbZMqpv
     */
    public function testCustIdTooLong()
    {
        $memberPayment = new MemberPayment();

        $this->expectException(\InvalidArgumentException::class);
        $memberPayment->setStripeCustomerId("cus_V9T7vofUbZMqpvf");
        $this->assertEquals("", $memberPayment->getStripeCustomerId());
    }

    /*
     * This test will verify that the given customer ID is too short
     * Dylan S and Cory N
     * March 27, 2019
     * Example valid stripe_customer_id: cus_V9T7vofUbZMqpv
     */
    public function testCustIdTooShort()
    {
        $memberPayment = new MemberPayment();

        $this->expectException(\InvalidArgumentException::class);
        $memberPayment->setStripeCustomerId("cus_V9T7vofUbZMqp");
        $this->assertEquals("", $memberPayment->getStripeCustomerId());
    }

    /*
     * This test will verify that the given customer ID begins with "cus_"
     * Dylan S and Cory N
     * March 27, 2019
     * Example valid stripe_customer_id: cus_V9T7vofUbZMqpv
     */
    public function testCustIdDoesntStartWithcus_()
    {
        $memberPayment = new MemberPayment();

        $this->expectException(\InvalidArgumentException::class);
        $memberPayment->setStripeCustomerId("bus_V9T7vofUbZMqpv");
        $this->assertEquals("", $memberPayment->getStripeCustomerId());
    }

    /*
     * This test will verify that the given customer ID begins with a lowercase "cus_"
     * Dylan S and Cory N
     * March 27, 2019
     * Example valid stripe_customer_id: cus_V9T7vofUbZMqpv
     */
    public function testCustIdcus_CaseSensitive()
    {
        $memberPayment = new MemberPayment();

        $this->expectException(\InvalidArgumentException::class);
        $memberPayment->setStripeCustomerId("CUS_V9T7vofUbZMqpv");
        $this->assertEquals("", $memberPayment->getStripeCustomerId());
    }

    public function testCustIdNoSpaces()
    {
        $memberPayment = new MemberPayment();

        $this->expectException(\InvalidArgumentException::class);
        $memberPayment->setStripeCustomerId("cu s_V9T7vofUbZMqpv");
        $this->assertEquals("", $memberPayment->getStripeCustomerId());
    }


    /*
     * This test will verify that the given amount is within correct range
     * Cory N and Dylan S
     * March 27, 2019
     * Valid amounts: payment_amount >= 1 and < 1,000,000
     */

    public function testPaymentValid()
    {
        $memberPayment = new MemberPayment();
        $memberPayment->setPaymentAmount(35.00);

        $this->assertEquals(35.00, $memberPayment->getPaymentAmount());
    }

    /*
     * This test will verify that the given amount is within correct range
     * Cory N and Dylan S
     * March 27, 2019
     * Valid amounts: payment_amount >= 1 and < 1,000,000
     */
    public function testPaymentUnderOneDollar()
    {
        $memberPayment = new MemberPayment();

        $this->expectException(\InvalidArgumentException::class);
        $memberPayment->setPaymentAmount(0.99);
        $this->assertEquals("", $memberPayment->getPaymentAmount());
    }

    /*
     * This test will verify that the given amount is within correct range
     * Cory N and Dylan S
     * March 27, 2019
     * Valid amounts: payment_amount >= 1 and < 1,000,000
     */
    public function testPaymentOneDollar()
    {
        $memberPayment = new MemberPayment();
        $memberPayment->setPaymentAmount(1);
        $this->assertEquals(1, $memberPayment->getPaymentAmount());
    }

    /*
     * This test will verify that the given amount is within correct range
     * Cory N and Dylan S
     * March 27, 2019
     * Valid amounts: payment_amount >= 1 and < 1,000,000
     */
    public function testPaymentTooHigh()
    {
        $memberPayment = new MemberPayment();

        $this->expectException(\InvalidArgumentException::class);
        $memberPayment->setPaymentAmount(1000000.00);
        $this->assertEquals("", $memberPayment->getPaymentAmount());
    }

    /*
     * This test will verify that the given amount is within correct range
     * Cory N and Dylan S
     * March 27, 2019
     * Valid amounts: payment_amount >= 1 and < 1,000,000
     */
    public function testPaymentAlmostTooHigh()
    {
        $memberPayment = new MemberPayment();
        $memberPayment->setPaymentAmount(999999.99);
        $this->assertEquals(999999.99, $memberPayment->getPaymentAmount());
    }

    /*
     * This test will verify that the given amount is within correct range
     * Cory N and Dylan S
     * March 27, 2019
     * Valid amounts: payment_amount >= 1 and < 1,000,000
     */
    public function testPaymentOnlyAcceptsNumericalValues()
    {
        $memberPayment = new MemberPayment();

        $this->expectException(\TypeError::class);
        $memberPayment->setPaymentAmount("abc");
        $this->assertEquals("", $memberPayment->getPaymentAmount());
    }

    /*
     * This function will test that it will return the payment amount in pennies
     * @author Dylan, Cory
     * March 27, 2019
     */
    public function testConvertAmountWorks()
    {
        $memberPayment = new MemberPayment();
        $memberPayment->setPaymentAmount(15.00);

        $this->assertEquals(1500, $memberPayment->convertAmountToPennies());
    }







}