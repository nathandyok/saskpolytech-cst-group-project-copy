<?php

namespace App\Tests\Entity;

use App\DataFixtures\AppFixtures;
use App\Entity\Member;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use DMore\ChromeDriver\ChromeDriver;
use Behat\Mink\Mink;
use Behat\Mink\Session;
//use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class MemberControllerTest
 * @package App\Tests\Entity
 * @author Kate Z, Cory N
 * @date 10/25/2018
 *
 * This class contains function tests that will make sure appropriate error messages are shown for a members
 * personal information if the data is invalid.
 */
class MemberControllerTest extends WebTestCase
{
    public function setUp()
    {
        $this->loadFixtures(array(
            'App\DataFixtures\MemberLoginTestFixture',
            'App\DataFixtures\AllShowsFixtures',
            'App\DataFixtures\AppFixtures',
            'App\DataFixtures\AddMembersFixtures',
        ));
    }

    /**
     * This test will make sure that the member registration page exists
     * @author Kate Z, Cory N
     */
    public function testThatMemberRegPageExists()
    {
        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);


        $this->assertEquals($client->getCrawler()->getUri(), "http://localhost/member/edit/167");


    }

    /**
     *This test will assert that the form elements are on the registration page
     * @author Kate Z, Cory N
     */
    public function testThatFormElementsAreOnPage()
    {
        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        //Assert that the form and its elements are on the registration page
        $this->assertContains('form', $client->getResponse()->getContent());
        $this->assertContains('id="member_update_firstName"', $client->getResponse()->getContent());
        $this->assertContains('id="member_update_lastName"', $client->getResponse()->getContent());
        $this->assertContains('id="member_update_userName"', $client->getResponse()->getContent());


        $this->assertContains('id="member_update_addressLineOne"', $client->getResponse()->getContent());
        $this->assertContains('id="member_update_addressLineTwo"', $client->getResponse()->getContent());
        $this->assertContains('id="member_update_city"', $client->getResponse()->getContent());
        $this->assertContains('id="member_update_postalCode"', $client->getResponse()->getContent());
        $this->assertContains('id="member_update_province"', $client->getResponse()->getContent());
        $this->assertContains('id="member_update_company"', $client->getResponse()->getContent());
        $this->assertContains('id="member_update_phone"', $client->getResponse()->getContent());

    }


    /**
     * This function will test that an error message is shown when the firstname field is too short / blank
     * @author Kate Z, Cory N
     * start chrome --disable-gpu --headless --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222
     */
    public function testFirstNameRequired()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();


        // Invalid first name
        $formUpdate['member_update[firstName]']->setValue("");

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("First name must be between 1-20 characters without spaces", $errText);

    }

    /**
     * This function will test that an error message is shown when the firstname field is over 20 characters
     * @author Kate Z, Cory N
     */
    public function testFirstNameTooLong()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        //$this->assertEquals($client->getCrawler()->getUri(), "http://localhost/member/edit/15");

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[firstName]']->setValue(str_repeat("a", 25));

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("First name must be between 1-20 characters without spaces", $errText);
    }






    /**
     * This function will test that an error message is shown when the lastname field is over 20 characters
     * @author Kate Z, Cory N
     */
    public function testLastNameTooLong()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        //$this->assertEquals($client->getCrawler()->getUri(), "http://localhost/member/edit/15");

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[lastName]']->setValue(str_repeat("a", 25));

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Last name must be between 1-20 characters without spaces", $errText);
    }

    /**
     * This function will test that an error message is shown when the lastname field is blank
     * @author Kate Z, Cory N
     */
    public function testLastNameTooShort()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        //$this->assertEquals($client->getCrawler()->getUri(), "http://localhost/member/edit/15");

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[lastName]']->setValue("");

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Last name must be between 1-20 characters without spaces", $errText);
    }

    /**
     * This function will test that an error message is shown when an invalid UserName/email has been entered
     *@author Cory N and Kate Z
     */
    public function testUserNameWithSymbol()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        //$this->assertEquals($client->getCrawler()->getUri(), "http://localhost/member/edit/15");

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[userName]']->setValue("AbgGoogle.com");

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Email must be in standard email format", $errText);
    }

    /**
     * This function will test that an error message is shown when an invalid UserName/email has been entered, no suffix
     *@author Cory N and Kate Z
     */
    public function testUserNameSuffix()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        //$this->assertEquals($client->getCrawler()->getUri(), "http://localhost/member/edit/15");

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[userName]']->setValue("AbgGooglecom");

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Email must be in standard email format", $errText);
    }



    /**
     * This will test that the user is redirected to a new page after entering a user name that is too long
     * Kate Z and Cory N
     */
    public function testUserNameTooLong()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[userName]']->setValue(str_repeat("a", 101) . "@gmail.com");

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Email exceeds 50 characters", $errText);
    }

    /**
     * This will test taht a username/email is required
     * Kate Z and Cory N
     */
    public function testUserNameRequired()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[userName]']->setValue("");

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Email is required", $errText);
    }

    /**
     * Test that email/Username is unique
     * Cory N and Kate Z
     */
    public function testUserNameIsUnique()
    {
        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[userName]']->setValue("testHyphen@gmail.com");

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("A user with that email already exists", $errText);

    }


    /**
     * Assert that the addressLineOne field is required and can not be left empty. Error message should be shown.
     *
     * @author Kate Z, Cory N
     */
    public function testaddressLineOneRequired()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[addressLineOne]']->setValue("");

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Address Line One is required", $errText);
    }


    /**
     * Assert that an error message is displayed when the addressLineOne is too long
     * @author Kate Z, Cory N
     */
    public function testaddressLineOneTooLong()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[addressLineOne]']->setValue(str_repeat("a", 101));

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Billing address must be under 100 characters", $errText);
    }


    /**
     * Assert that an error message is displayed when the addressLineTwo is too long
     * @author Kate Z, MacKenzie W
     */
    public function testaddressLineTwoTooLong()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[addressLineTwo]']->setValue(str_repeat("a", 101));

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Address Line Two must be under 100 characters", $errText);
    }

    /**
     * Assert that an error message is shown when nothing is in the City field because it is required
     * @author Kate Z, Cory N
     */
    public function testCityRequired()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[city]']->setValue("");

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("City is required", $errText);
    }


    /**
     * Assert that there is an error message that is shown when the City field is too long
     * @author Kate Z, Cory N
     */
    public function testCityTooLong()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[city]']->setValue(str_repeat("a", 101));

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("City must be under 100 characters", $errText);
    }

    /**
     * Assert that there is an error message shown when there is nothing in the postal code field because it is required
     * @author Kate Z, Cory N
     */
    public function testPostalCodeRequired()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[postalCode]']->setValue("");

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Postal Code is required", $errText);
    }

    /**
     * Assert that an error message is shown when the postal code does not contain a number
     * @author Kate Z, Cory N
     */
    public function testPostalCodeDoesNotContainANumber()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[postalCode]']->setValue("ABCDEF");

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Postal Code should be in the format of A2A3B3 or A2A 3B3", $errText);
    }
    /**
     * Assert that an error message is shown when the postal code does not contain a letter
     * @author Kate Z, Cory N
     */
    public function testPostalCodeDoesNotContainALetter()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[postalCode]']->setValue("444555");

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Postal Code should be in the format of A2A3B3 or A2A 3B3", $errText);
    }


    /**
     * Assert that an error message is shown when the postal code is too long
     * @author Kate Z, Cory N
     */
    public function testPostalCodeIsTooLong()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[postalCode]']->setValue(str_repeat("a", 101));

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Postal Code should be in the format of A2A3B3 or A2A 3B3", $errText);
    }

    /**
     * Assert that an error message is shown when the postal code is too Short
     * @author Kate Z, Cory N
     */
    public function testPostalCodeIsTooShort()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[postalCode]']->setValue("a");

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Postal Code should be in the format of A2A3B3 or A2A 3B3", $errText);
    }




    /**
     * Assert that an error message is shown when you enter an invalid province
     * @expectedException InvalidArgumentException
     * @author Kate Z, Cory N, Kate Z, MacKenzie W
     */
    public function testProvinceInvalidValue()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[province]']->setValue("a");

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Need to enter Province", $errText);

    }
    /**
     * Assert that an error message is shown when you enter an invalid province
     * @expectedException InvalidArgumentException
     * @author Kate Z, Cory N, Kate Z, MacKenzie W
     */
    public function testProvinceBlankValue()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[province]']->setValue("NA");

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Need to enter Province", $errText);

    }


    /**
     * Assert that you are redirected to the success page when you leave the company field blank because it is not required
     * @author Kate Z, Cory N
     */
    public function testCompanyNotRequired()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        $page->find("css", "a#updateProfile")->click();

        $page->find("css", "#member_update_company")->setValue('');

        $page->find("css", "#updateButton")->click();

        $url = $driver->getCurrentUrl();

        $this->assertEquals("http://localhost:8000/show?isUpdated=1", $url);

        $successMessage = $page->find("css", ".modal-body")->getText();
        $this->assertEquals("Your personal information has been changed!", $successMessage);
    }

    /**
     * Assert that an error message is shown when the company field is too long
     * @author Kate Z, Cory N
     */
    public function testCompanyTooLong()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[company]']->setValue(str_repeat("a", 101));

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Company is invalid", $errText);
    }





    /**
     * Assert that an error message is shown when the phone number is too long
     * @author Kate Z, Cory N
     */
    public function testPhoneTooLong()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[phone]']->setValue(str_repeat("a", 101));

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Please enter a 10-digit phone number", $errText);
    }

    /**
     * Assert that an error message is shown when the phone number is too short
     * @author Kate Z, Cory N
     */
    public function testPhoneTooShort()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[phone]']->setValue("33");

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Please enter a 10-digit phone number", $errText);
    }


    /**
     * Assert that an error message is shown if there are any invalid characters in the phone number
     * @author Kate Z, Cory N
     */
    public function testPhoneContainsInvalidCharacters()
    {
        //LOGIN AS A MEMBER

        $client = static::createClient();

        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginButton = $crawler->selectLink('Login')->link();
        $client->click($loginButton);

        $crawler = $client->getCrawler();

        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'samprj4reset@gmail.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);

        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $updateLink = $crawler->selectLink('Update Profile')->link();
        $client->click($updateLink);

        $crawler = $client->getCrawler();

        $formUpdate = $crawler->selectButton('Update')->form();

        // Invalid first name
        $formUpdate['member_update[phone]']->setValue("306242ABC4");

        $crawler = $client->submit($formUpdate);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Please enter a 10-digit phone number", $errText);
    }


}
