<?php

namespace App\Tests\Entity;

use App\DataFixtures\AppFixtures;
use App\Entity\ContactSSP;
use Liip\FunctionalTestBundle\Test\WebTestCase;

/**
 * Class ContactSSPControllerTest
 * @package App\Tests\Entity
 * @author Christopher, Ankita
 *

 */

class ContactSSPControllerTest extends WebTestCase
{
    private $client;
    private $crawler;
    private $formContactUs;

    public function setUp()
    {
        global $client, $crawler, $formContactUs;

        $client = static::createClient();
        $client->request('GET', '/contact_us/');

        $crawler = $client->getCrawler();

        $formContactUs = $crawler->selectButton('Submit')->form();


        $formContactUs['contact_ssp[name]']->setValue("Steven Jo");
        $formContactUs['contact_ssp[subject]']->setValue("upcoming shows");
        $formContactUs['contact_ssp[email]']->setValue("email@mail.com");
        $formContactUs['contact_ssp[phone]']->setValue("3061231234");
        $formContactUs['contact_ssp[comments]']->setValue("Job Opportunities");
    }

    /**
     * This function will test that the name is required and cannot be blank.
     *
     * @author Christopher
     */

    public function testNameIsRequired()
    {
        global $client, $crawler, $formContactUs;

        $formContactUs['contact_ssp[name]']->setValue("");

        $crawler = $client->submit($formContactUs);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Name must be 1-40 characters in length", $errText);
    }

    /**
     * This function will test that the name is not more than 40 characters
     *
     * @author Christopher
     */
    public function testNameIsTooLong()
    {
        global $client, $crawler, $formContactUs;

        $formContactUs['contact_ssp[name]']->setValue(str_repeat("a", 41));

        $crawler = $client->submit($formContactUs);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Name must be 1-40 characters in length", $errText);
    }


    /**
     * This function will test that the phone number  and/or email is required
     *
     * @author Christopher
     */
    public function testEmailAndPhoneIsRequired()
    {
        global $client, $crawler, $formContactUs;

        $formContactUs['contact_ssp[email]']->setValue("");
        $formContactUs['contact_ssp[phone]']->setValue("");

        $crawler = $client->submit($formContactUs);

        $errText = $crawler->filter(".alert.alert-danger")->text();
        $errText2 = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Email address or phone number is required", $errText);
        $this->assertContains("Email address or phone number is required", $errText2);
    }


    /**
     * This function will test that the subject is required
     *
     * @author Christopher
     */
    public function testSubjectIsRequired()
    {
        global $client, $crawler, $formContactUs;

        $formContactUs['contact_ssp[subject]']->setValue("");

        $crawler = $client->submit($formContactUs);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Choose an option from list", $errText);
    }

    /**
     * This function will test that the comment is required
     *
     * @author Christopher
     */
    public function testCommentIsRequired()
    {
        global $client, $crawler, $formContactUs;

        $formContactUs['contact_ssp[comments]']->setValue("");

        $crawler = $client->submit($formContactUs);

        $errText = $crawler->filter(".alert.alert-danger")->text();

        $this->assertContains("Comment is required", $errText);
    }




}
