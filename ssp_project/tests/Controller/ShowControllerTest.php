<?php
namespace App\Tests;

use App\Entity\Address;
use App\Entity\SSPShow;
use DMore\ChromeDriver\ChromeDriver;
use Behat\Mink\Mink;
use Behat\Mink\Session;
use Liip\FunctionalTestBundle\Test\WebTestCase;

class ShowControllerTest extends WebTestCase
{
    public function setUp()
    {
        $this->loadFixtures(array(
            'App\DataFixtures\AllShowsFixtures',
            'App\DataFixtures\AppFixtures',
            'App\DataFixtures\AddMembersFixtures'
        ));
    }

    /**
     * Testing that the webpage only displays shows that are in the future
     * Ensure that the server and Chrome has been run in headless mode.
     *
     * From command prompt:
     * 1) php bin/console server:run
     * 2) start chrome --disable-gpu --headless --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222
     *
     * Author: Cory N and NathanD
     * @throws
     */
    public function testFutureShow()
    {
        // Load ChromeDriver
        $mink = new Mink(array(
            'FutureShowBrowser' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // set the default session name
        $mink->setDefaultSessionName('FutureShowBrowser');

        // visit homepage
        $mink->getSession()->visit('http://localhost:8000/show?show=1');

        /** @var ChromeDriver $driver */
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();

        // Target future show link button
        $driver->click("//a[@id='futureShowsBtn']");

        // Capture image to verify content
        // $driver->captureScreenshot('ShowControllerTestImages/futureShows.png');

        // Verify the correct text is being displayed
        $futureText1 = $driver->getHtml("/html/body/div[4]/div[2]/div[1]/h1");
        //$page->filter("css", )
        $this->assertEquals('Christmas Story', $futureText1);

        // Verify the correct text is being displayed
        $futureText2 = $driver->getHtml("/html/body/div[4]/div[3]/div[1]/h1");
        $this->assertEquals('Mary Poppins', $futureText2);

        // Very the status is correct
        $currentStatus = $driver->getText("/html/body/div[4]/div[1]/div/p");

        $this->assertEquals('Upcoming Shows', $currentStatus);

        // Check to ensure the future shows are visible
        $isVisible = $page->find("css", "body > div.container-fluid.futureShow")->isVisible();
        $this->assertTrue($isVisible);

        // Check to ensure current shows are not visible
        $isNotVisible = $driver->isVisible("//div[@class='container-fluid currentShow']");
        $this->assertFalse($isNotVisible);

        $futureDisplayAttr = $driver->getAttribute("//div[@class='container-fluid futureShow']", 'style');
        $this->assertEquals('', $futureDisplayAttr);

        $currentDisplayAttr = $driver->getAttribute("//div[@class='container-fluid currentShow']", 'style');
        $this->assertEquals('display: none;', $currentDisplayAttr);


    }


    /**
     * Testing that the webpage only displays the current show
     * Author: Cory N and Nathan D
     * @throws
     */
    public function testCurrentShow()
    {
        // Load Mink
        $mink = new Mink(array(
            'CurrentShowBrowser' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('CurrentShowBrowser');

        // Visit homepage
        $mink->getSession()->visit('http://localhost:8000/show');

        /** @var ChromeDriver $driver */
        $driver = $mink->getSession()->getDriver();

        // Target future show link button
        $driver->click("//a[@id='currentShowsBtn']");

        // Capture screen shot to verify content
        // $driver->captureScreenshot('ShowControllerTestImages/currentShows.png');

        // Check to ensure text is displayed properly
        $currentText = $driver->getHtml("/html/body/div[3]/div[2]/div[1]/h1");
        $this->assertEquals('Sweeney Todd', $currentText);

        // Check to ensure the current status is correct
        $currentStatus = $driver->getHtml("/html/body/div[3]/div[1]/div/p");
        $this->assertEquals('Now Playing', $currentStatus);

        // Check to ensure the future shows are visible
        $isVisible = $driver->isVisible("//div[@class='container-fluid currentShow']");
        $this->assertTrue($isVisible);

        // Check to ensure current shows are not visible
        $isNotVisible = $driver->isVisible("//div[@class='container-fluid futureShow']");
        $this->assertFalse($isNotVisible);

        // Check to make sure that current shows display style is not set to none
        $currentDisplayAttr = $driver->getAttribute("//div[@class='container-fluid currentShow']", 'style');
        $this->assertEquals('', $currentDisplayAttr);

        $futureDisplayAttr = $driver->getAttribute("//div[@class='container-fluid futureShow']", 'style');
        $this->assertEquals('display: none;', $futureDisplayAttr);
    }

    /**
     * Testing that the webpage only displays shows that have already happened
     * Author: Cory N and Nathan D
     * @throws
     */
    public function testPastShow()
    {
        // Load ChromeDriver
        $mink = new Mink(array(
            'PastShowBrowser' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // set the default session name
        $mink->setDefaultSessionName('PastShowBrowser');

        // visit homepage
        $mink->getSession()->visit('http://localhost:8000/show');

        /** @var ChromeDriver $driver */
        $driver = $mink->getSession()->getDriver();

        // Target future show link button
        $driver->click("//a[@id='prevShowsBtn']");
        // Capture screen shot to verify content
        // $driver->captureScreenshot('ShowControllerTestImages/pastShows.png');

        // Check to ensure text is displayed properly
        $pastText1 = $driver->getHtml("/html/body/div[3]/div[2]/div[1]/h1");
        $pastText2 = $driver->getHtml("/html/body/div[3]/div[3]/div[1]/h1");

        $this->assertEquals('Hamlet', $pastText1);
        $this->assertEquals('Come From Away', $pastText2);

        $currentStatusText = $driver->getHtml("//*[@class='currentStatus']");
        $this->assertEquals('Previous Shows', $currentStatusText);

        // Check to ensure the future shows are visible
        $isVisible = $driver->isVisible("//div[@class='container-fluid pastShow']");
        $this->assertTrue($isVisible);

    }

    /**
     * Tests that a button is not created if the show doesn't exist
     */
    public function testNoButtonCreatedIfPageDoesntExist()
    {
        //Create client, crawler and request a page for a show that doesn't exist
        $client = static::createClient();
        $client->request('GET','/show/cnsjdfnsdoisdfdds/dfsf');
        $crawler = $client->getCrawler();

        //Ensure page doesn't exist and no page nor button was generated (button isn't made if the twig is never called ie. Controller never rendered the twig)
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }


    /**
     * This test will ensure that the logo redirects back
     * to the home page after clicking on it
     */
    public function testThatLogoRedirects()
    {
        // Get client to start on the show page
        $client = static::createClient();
        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Verify that we're on the home page
        $currentURI = $crawler->getUri();
        $this->assertEquals('http://localhost/', $currentURI);

        // Verify that the logo link redirects properly to home page
        $logo = $crawler->filter('a.navbar-brand')->link();
        $crawler = $client->click($logo);
        $this->assertEquals('http://localhost/', $crawler->getUri());
        $targetText = $crawler->filter('h1')->eq(0)->text();
        $this->assertEquals('Welcome to Saskatoon Summer Players', $targetText);
    }


    /**
     * This test will ensure that the user is redirected to the shows page
     * after clicking on the "Our shows" link from the navigation menu
     */
    public function testThatUserIsRedirectedToOurShows()
    {
        // Get client to start on the show page
        $client = static::createClient();
        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target the second list item and verify text is "Our Shows"
        $showLinkText = $crawler->filter('li.nav-item')->eq(1)->text();
        $this->assertEquals('Our Shows', $showLinkText);
        // Verify that the "Our Shows" link redirects properly
        $showLink = $crawler->filter('li.nav-item a')->eq(1)->link();
        $crawler = $client->click($showLink);
        $this->assertEquals('http://localhost/show', $crawler->getUri());
        $text = $crawler->filter('h1')->eq(1)->text();
        $this->assertEquals('Sweeney Todd',$text);
    }

}


