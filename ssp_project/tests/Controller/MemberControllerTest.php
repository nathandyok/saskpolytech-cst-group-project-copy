<?php

namespace App\Tests\Controller;

use App\DataFixtures\AppFixtures;
use Liip\FunctionalTestBundle\Test\WebTestCase;
//use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Entity\Member;

/**
 * Class MemberControllerTest
 * @package App\Tests\Entity
 * @author Dylan S, Cory N
 * @date 10/25/2018
 *
 * This class contains function tests that will make sure appropriate error messages are shown for a members
 * personal information if the data is invalid.
 */
class MemberControllerTest extends WebTestCase
{
    private $crawler;
    private $client;
    private $form;

    public function setUp()
    {

        $this->client = static::createClient();

        $this->client->request('GET', '/registration');

        $this->crawler = $this->client->getCrawler();

        $test = $this->client->getResponse()->getContent();

        $this->form = $this->crawler->selectButton('Submit')->form();

        $form = $this->form;
        $crawler = $this->crawler;
        $client = $this->client;


        // Invalid first name
        $form['member[firstName]']->setValue("Maxxx2");
        $form['member[lastName]']->setValue("Smitdsah");
        //$form['member[userName]']->setValue("James" . rand(1, 999999));
        $form['member[userName]']->setValue("Cor" . rand(1,999999) . "@here.com");
        $form['member[password][first]']->setValue("Cory1111");
        $form['member[password][second]']->setValue("Cory1111");

        //billing registration
        $form['member[addressLineOne]']->setValue("123 Fake Street");
        $form['member[addressLineTwo]']->setValue("456 Bake Street");
        $form['member[city]']->setValue("Saskatoon");
        $form['member[postalCode]']->setValue("S4N 4B7");
        //$form['member[country]']->setValue("CA");
        $form['member[province]']->setValue("SK");
        $form['member[company]']->setValue("ajlfalshfklja");
        $form['member[phone]']->setValue("");

        //Testing Individual and 1 year paid membership
        $form['member[memberType]']->setValue("Individual");
        $form['member[memberOption]']->setValue("1-year Paid Membership");
        $form['member[membershipAgreement]']->tick();

        // Random one off the internet
        //\Stripe\Stripe::setApiKey("sk_test_BQokikJOvBiI2HlWgH4olfQ2");

        // Out SK key
        \Stripe\Stripe::setApiKey("sk_test_opt9SuN4L1Z842PPbzlW8a6K00rVtDdwfb");

        // We need to create a new unique valid token each time
        $card = \Stripe\Token::create(array(
            "card" => array(
                "number" => "4242424242424242",
                "exp_month" => 1,
                "exp_year" => 2022,
                "cvc" => "314"
            )
        ));

        // Set the token from our newly created card
        $form['member[stripeToken]']->setValue($card->id);
    }

    /**
     * This test will make sure that the member registration page exists
     * @author Dylan S, Cory N
     */
    public function testThatMemberRegPageExists()
    {

        $client = static::createClient();

        $client->request('GET', '/registration');


        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    /**
     *This test will assert that the form elements are on the registration page
     * @author Dylan S, Cory N
     */
    public function testThatFormElementsAreOnPage()
    {
        $client = $this->client;

        //Assert that the form and its elements are on the registration page
        $this->assertContains('form', $client->getResponse()->getContent());
        $this->assertContains('id="member_firstName"', $client->getResponse()->getContent());
        $this->assertContains('id="member_lastName"', $client->getResponse()->getContent());
        $this->assertContains('id="member_userName"', $client->getResponse()->getContent());
        $this->assertContains('id="member_password_first"', $client->getResponse()->getContent());

        $this->assertContains('id="member_addressLineOne"', $client->getResponse()->getContent());
        $this->assertContains('id="member_addressLineTwo"', $client->getResponse()->getContent());
        $this->assertContains('id="member_city"', $client->getResponse()->getContent());
        $this->assertContains('id="member_postalCode"', $client->getResponse()->getContent());
        $this->assertContains('id="member_province"', $client->getResponse()->getContent());
        $this->assertContains('id="member_company"', $client->getResponse()->getContent());
        $this->assertContains('id="member_phone"', $client->getResponse()->getContent());



    }


    /**
     * This function will test that an error message is shown when the firstname field is too short / blank
     * @author Dylan S, Cory N
     */
    public function testFirstNameTooShort()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $this->assertEquals('New Member', $crawler->filter('title')->text());

        // Invalid first name
        $form['member[firstName]']->setValue("");

        $client->submit($form);


        $this->crawler = $this->client->getCrawler();
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("First name must be between 1-20 characters", $this->crawler->filter('.alert.alert-danger')->text());
    }

    /**
     * This function will test that an error message is shown when the firstname field is over 20 characters
     * @author Dylan S, Cory N
     */
    public function testFirstNameTooLong()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $this->assertEquals('New Member', $crawler->filter('title')->text());

        // firstname Beyond 20 Characters
        $form['member[firstName]']->setValue("plokijuhygtfrdeswaqzx");

        $client->submit($form);
        $this->crawler = $this->client->getCrawler();
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("First name must be between 1-20 characters", $this->crawler->filter('.alert.alert-danger')->text());

    }

    /**
     * This will test that the user is redirected to a new page after entering a valid first name
     * @author Dylan S, Cory N
     */
    public function testFirstNameIsValid()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $this->assertEquals('New Member', $crawler->filter('title')->text());

        // Valid first name
        $form['member[firstName]']->setValue("Maxxx2");

        $client->submit($form);

        $client->followRedirect();

        $crawler = $client->getCrawler();

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        //Assert that we are redirected to a new page and that the page contains the success message
        // Check that we've been redirected properly
        $this->assertEquals("http://localhost/login?justRegistered=1", $client->getCrawler()->getUri());

        // Assert text
        $successMessage = $crawler->filter(".modal-body")->text();
        $this->assertEquals("Welcome to Saskatoon Summer Players. You can now log in using your account.", $successMessage);
    }


    /**
     * This will test that the user is redirected to a new page after entering a valid last name
     * Dylan S and Cory N
     */
    public function testIsValidLastName()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $this->assertEquals('New Member', $crawler->filter('title')->text());

        $form['member[lastName]']->setValue("Smitty99");

        $client->submit($form);
        $client->followRedirect();
        $crawler = $client->getCrawler();

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost/login?justRegistered=1", $client->getCrawler()->getUri());

        // Assert text
        $successMessage = $crawler->filter(".modal-body")->text();
        $this->assertEquals("Welcome to Saskatoon Summer Players. You can now log in using your account.", $successMessage);
    }

    /**
     * This function will test that an error message is shown when the lastname field is over 20 characters
     * @author Dylan S, Cory N
     */
    public function testLastNameTooLong()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $this->assertEquals('New Member', $crawler->filter('title')->text());

        $form['member[lastName]']->setValue("qwertyuiopasdfghjklzx");

        $client->submit($form);
        $this->crawler = $this->client->getCrawler();
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Last name must be between 1-20 characters", $this->crawler->filter('.alert.alert-danger')->text());

    }

    /**
     * This function will test that an error message is shown when the lastname field is blank
     * @author Dylan S, Cory N
     */
    public function testLastNameTooShort()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $this->assertEquals('New Member', $crawler->filter('title')->text());

        $form['member[lastName]']->setValue("");

        $client->submit($form);


        $this->crawler = $this->client->getCrawler();
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Last name must be between 1-20 characters without spaces", $this->crawler->filter('.alert.alert-danger')->text());
    }

    /**
     * This function will test that an error message is shown when an invalid UserName/email has been entered
     *@author Cory N and Dylan S
     */
    public function testUserNameWithSymbol()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $this->assertEquals('New Member', $crawler->filter('title')->text());

        $form['member[userName]']->setValue("abc" . rand(1, 999999) . "gmail.com");

        $client->submit($form);
        $this->crawler = $this->client->getCrawler();
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Email must be in standard email format", $this->crawler->filter('.alert.alert-danger')->text());

    }

    /**
     * This function will test that an error message is shown when an invalid UserName/email has been entered, no suffix
     *@author Cory N and Dylan S
     */
    public function testUserNameSuffix()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $this->assertEquals('New Member', $crawler->filter('title')->text());

        // Entry is missing the suffix .com, .ca , etc...
        $form['member[userName]']->setValue("abc" . rand(1, 999999) . "@gmail");

        $client->submit($form);

        $this->crawler = $this->client->getCrawler();
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Email must be in standard email format", $this->crawler->filter('.alert.alert-danger')->text());
    }


    /**
     * This will test that the user is redirected to a new page after entering a valid UserName/email
     * Dylan S and Cory N
     */
    public function testUserNameValid()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $this->assertEquals('New Member', $crawler->filter('title')->text());

        // Valid email
        $form['member[userName]']->setValue("abc" . rand(1, 999999) . "@gmail.com");

        $client->submit($form);
        $client->followRedirect();
        $crawler = $client->getCrawler();

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost/login?justRegistered=1", $client->getCrawler()->getUri());

        // Assert text
        $successMessage = $crawler->filter(".modal-body")->text();
        $this->assertEquals("Welcome to Saskatoon Summer Players. You can now log in using your account.", $successMessage);

    }

    /**
     * This will test that the user is redirected to a new page after entering a valid UserName/email
     * Dylan S and Cory N
     */
    public function testUserNameTooLong()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;


        $this->assertEquals('New Member', $crawler->filter('title')->text());


        $form['member[userName]']->setValue("abcplokijuhygtfrdeswaqzsxcdvfgbhnjmklopiuytgftr" . rand(100000, 999999) . "@gmail.com");
        $client->submit($form);

        $this->crawler = $this->client->getCrawler();
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Email exceeds 50 characters", $this->crawler->filter('.alert.alert-danger')->text());

    }

    /**
     * This will test that the user is redirected to a new page after entering a valid UserName/email
     * Dylan S and Cory N
     */
    public function testUserNameRequired()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $this->assertEquals('New Member', $crawler->filter('title')->text());


        $form['member[userName]']->setValue("");

        $client->submit($form);

        $this->crawler = $this->client->getCrawler();
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Email is required", $this->crawler->filter('.alert.alert-danger')->text());
    }

    /**
     * Test that email/Username is unique
     * Cory N and Dylan S
     */
    public function testUserNameIsUnique()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;


        $this->assertEquals('New Member', $crawler->filter('title')->text());


        $form['member[userName]']->setValue("mymail@gmail.com");

        $client->submit($form);

        $form['member[userName]']->setValue("mymail@gmail.com");

        $client->submit($form);
        $this->crawler = $this->client->getCrawler();
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("A user with that email already exists", $this->crawler->filter('.alert.alert-danger')->text());

    }

    /**
     * This function will test that an error message is shown when a password is too short
     *@author Cory N and Dylan S
     */
    public function testPasswordTooShort()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $this->assertEquals('New Member', $crawler->filter('title')->text());


        $form['member[password][first]']->setValue("12345");
        $form['member[password][second]']->setValue("12345");


        $client->submit($form);

        $this->crawler = $this->client->getCrawler();
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Password must consist of 6-20 alpha characters", $this->crawler->filter('.alert.alert-danger')->text());

    }

    /**
     * This function will test that an error message is shown when a password exceeds the allowed length
     *@author Cory N and Dylan S
     */
    public function testPasswordTooLong()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $this->assertEquals('New Member', $crawler->filter('title')->text());


        // Password is beyond 4096 characters
        $passToMatch = "";
        for ($i=0; $i<4098; $i++)
        {
            $passToMatch .= "g";
        }

        $form['member[password][first]']->setValue($passToMatch);
        $form['member[password][second]']->setValue($passToMatch);

        $client->submit($form);

        $this->crawler = $this->client->getCrawler();
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Password must consist of 6-20 alpha characters", $this->crawler->filter('.alert.alert-danger')->text());
    }

    /**
     * This function will test that an error message is shown when a password without a capital letter has been entered
     * has been entered
     *@author Cory N and Dylan S
     */
    public function testNoUpperCaseInPassword()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $this->assertEquals('New Member', $crawler->filter('title')->text());


        // Missing an uppercase
        $form['member[password][first]']->setValue("chocolate1");
        $form['member[password][second]']->setValue("chocolate1");


        $client->submit($form);

        $this->crawler = $this->client->getCrawler();
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Password must consist of one upper-case, one-lower-case, one number", $this->crawler->filter('.alert.alert-danger')->text());
    }

    /**
     * This function will test that an error message is shown when a password without a number has been entered
     * has been entered
     *@author Cory N and Dylan S
     */
    public function testNoNumberInPassword()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $this->assertEquals('New Member', $crawler->filter('title')->text());

        // Missing a number
        $form['member[password][first]']->setValue("Chocolate");
        $form['member[password][second]']->setValue("Chocolate");

        //Testing Individual and 1 year paid membership
        $form['member[memberType]']->setValue("Individual");
        $form['member[memberOption]']->setValue("1-year Paid Membership");

        $client->submit($form);


        $this->crawler = $this->client->getCrawler();
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Password must consist of one upper-case, one-lower-case, one number", $this->crawler->filter('.alert.alert-danger')->text());
    }


    /**
     * This function will test that an error message is shown when a password without a capital letter has been entered
     * has been entered
     *@author Cory N and Dylan S
     */
    public function testNoLowerCaseInPassword()
    {
        $this->assertEquals('New Member', $this->crawler->filter('title')->text());
        $this->form['member[password][first]']->setValue("QWERTY123");
        $this->form['member[password][second]']->setValue("QWERTY123");
        $this->client->submit($this->form);
        $this->crawler = $this->client->getCrawler();
        $this->assertContains("Password must consist of one upper-case, one-lower-case, one number", $this->crawler->filter('.alert.alert-danger')->text());
    }


    /**
     * This will test that the user is redirected to a new page after entering a valid password
     * Dylan S and Cory N
     */
    public function testValidPassword()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $this->assertEquals('New Member', $crawler->filter('title')->text());

        // Valid Password
        $form['member[password][first]']->setValue("Cory007");
        $form['member[password][second]']->setValue("Cory007");

        $client->submit($form);
        $client->followRedirect();
        $crawler = $client->getCrawler();

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost/login?justRegistered=1", $client->getCrawler()->getUri());

        // Assert text
        $successMessage = $crawler->filter(".modal-body")->text();
        $this->assertEquals("Welcome to Saskatoon Summer Players. You can now log in using your account.", $successMessage);
    }


    /**
     * This test will ensure that the user is redirected to the registration page
     * after clicking on the "New Member" link from the navigation menu
     */
    public function testThatUserIsRedirectedToRegistrationPage()
    {
        // Get client to start on the registration page
        $client = static::createClient();
        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target the third list item and verify text is "Member Registration"
        $memberLinkText = $crawler->filter('#regNav > a')->text();
        $this->assertEquals('Become a Member', $memberLinkText);
        // Verify that the "Member Registration" link redirects properly
        $memberLink = $crawler->filter('#regNav > a')->link();
        $crawler = $client->click($memberLink);
        $this->assertEquals('http://localhost/registration', $crawler->getUri());
        $text = $crawler->filter('h1')->eq(0)->text();
        $this->assertEquals('Member Registration',$text);
    }



    /****************************** Story 2c Payment Information*********************************/
    /**
     *This test will assert that the form elements are on the registration page
     * @author Kate and MacKenzie
     */
    public function testThatMemberTypeAndPaymentOptionElementsAreOnPage()
    {
        $client = static::createClient();

        $client->request('GET', '/registration');

        //Assert that the form and its elements are on the registration page
        $this->assertContains('form', $client->getResponse()->getContent());
        $this->assertContains('id="member_memberType"', $client->getResponse()->getContent());
        $this->assertContains('id="member_memberOption"', $client->getResponse()->getContent());
        //$this->assertContains('id="member_currency"', $client->getResponse()->getContent());
        //$this->assertContains('id="member_paymentType"', $client->getResponse()->getContent());
    }


    /**
     * This test will make sure that all valid options are on the page for the user to select (memberType and memberOption)
     * @author Christopher & Dylan
     */
    public function testThatMemberTypeAndPaymentOptionsAreOnPage()
    {
        $client = static::createClient();

        $client->request('GET', '/registration');


        $this->assertContains('form', $client->getResponse()->getContent());

        //Test that all options are available to be selected on the page
        $this->assertContains('<option value="Individual">Individual</option>', $client->getResponse()->getContent());
        $this->assertContains('<option value="Family">Family</option>', $client->getResponse()->getContent());

        $this->assertContains('<option value="1-year Paid Membership">1-year Membership</option>', $client->getResponse()->getContent());
        $this->assertContains('<option value="Subscription">Auto renew 1-year Membership</option>', $client->getResponse()->getContent());
    }


    /**
     * Tests that the system will submit the form when a valid memberType and memberOption are entered
     * @author Dylan and Chris
     */
    public function testValidMemberTypeAndValidMemberOption()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        //Testing Individual and 1 year paid membership
        $form['member[memberType]']->setValue("Individual");
        $form['member[memberOption]']->setValue("1-year Paid Membership");

        $client->submit($form);

        $client->followRedirect();
        $crawler = $client->getCrawler();

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost/login?justRegistered=1", $client->getCrawler()->getUri());

        // Assert text
        $successMessage = $crawler->filter(".modal-body")->text();
        $this->assertEquals("Welcome to Saskatoon Summer Players. You can now log in using your account.", $successMessage);
    }

    /**
     * Tests that the system will submit the form when a valid memberType and memberOption are entered
     * @author Dylan and Chris
     */
    public function testValidMemberTypeAndValidMemberOption2()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        //Testing Individual and 1 year paid membership with auto-renew (subscription)
        $form['member[memberType]']->setValue("Family");
        $form['member[memberOption]']->setValue("Subscription");

        $client->submit($form);

        $client->followRedirect();

        $crawler = $client->getCrawler();

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost/login?justRegistered=1", $client->getCrawler()->getUri());

        // Assert text
        $successMessage = $crawler->filter(".modal-body")->text();
        $this->assertEquals("Welcome to Saskatoon Summer Players. You can now log in using your account.", $successMessage);
    }

    /**
     * Assert that an invalid entry for Member List displays an error and does not redirect
     * @author Kate and MacKenzie // Dylan and Chris
     */
    public function testInvalidMemberType()
    {
        $this->expectException(\InvalidArgumentException::class);

        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $form = $crawler->selectButton('Submit')->form();

        //memberType is invalid
        $form['member[memberType]']->setValue("Invalid");
        $form['member[memberOption]']->setValue("Subscription");


        $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('<li>This value is not valid.', $client->getResponse()->getContent());
    }

    /**
     * Assert that an invalid entry for Membership Option displays an error and does not redirect
     * @author Kate and MacKenzie // Dylan and Chris
     */
    public function testInvalidMemberOption()
    {
        $this->expectException(\InvalidArgumentException::class);

        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $form = $crawler->selectButton('Submit')->form();

        //memberoption is invalid
        $form['member[memberType]']->setValue("Family");
        $form['member[memberOption]']->setValue("Invalid");

        $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('<li>This value is not valid.', $client->getResponse()->getContent());
    }

    /**
     * Assert that an invalid entry for Membership Option and Member List displays errors and does not redirect
     * @author Dylan and Chris
     */
    public function testInvalidMemberOptionAndInvalidMemberType()
    {
        $this->expectException(\InvalidArgumentException::class);

        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;
        $form = $crawler->selectButton('Submit')->form();

        //Everything is invalid
        $form['member[memberType]']->setValue("Invalid");
        $form['member[memberOption]']->setValue("Invalid");

        $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('<li>This value is not valid.', $client->getResponse()->getContent());
    }


    /******************************************** 2B -- Billing Information *******************************************/

    /**
     * Assert that the addressLineOne field is required and can not be left empty. Error message should be shown.
     *
     * @author Dylan S, Christopher B
     */
    public function testaddressLineOneRequired()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;


        //addressLineOne is left empty
        $form['member[addressLineOne]']->setValue("");

        $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Address Line One is required", $client->getResponse()->getContent());
    }

    /**
     * Assert that AddressLineOne field accepts a value one under the maximum value of 100 characters
     * @author Dylan S, Christopher B
     */
    public function testaddressLineOneAlmostTooLong()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;


        //addressLineOne is almost too long
        $form['member[addressLineOne]']->setValue("thistextiswaytooolongitsohuldprobablynotgooverabout100charactersmaybeleftstryitoutmyguyyesyesyes");

        $client->submit($form);

        $client->followRedirect();

        $crawler = $client->getCrawler();

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost/login?justRegistered=1", $client->getCrawler()->getUri());

        // Assert text
        $successMessage = $crawler->filter(".modal-body")->text();
        $this->assertEquals("Welcome to Saskatoon Summer Players. You can now log in using your account.", $successMessage);
    }

    /**
     * Assert that an error message is displayed when the addressLineOne is too long
     * @author Dylan S, Christopher B
     */
    public function testaddressLineOneTooLong()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;



        //addressLineOne too long
        $form['member[addressLineOne]']->setValue("thistextiswaytooolongitsohuldprobablynot gooverabout100charactersmaybeleftstryitout myguyyesyesyesyesd");


        $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Billing address must be under 100 characters", $client->getResponse()->getContent());
    }


    /**
     * Assert that addressLineTwo field accepts a value one under the maximum value of 100 characters
     * @author MacKenzie W, Kate Z
     */
    public function testaddressLineTwoAlmostTooLong()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;


        $form['member[addressLineTwo]']->setValue("thistextiswaytooolongitsohuldprobablynotgooverabout100charactersmaybeleftstryitoutmyguyyesyesyesyes");

        $client->submit($form);

        $client->followRedirect();

        $crawler = $client->getCrawler();

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        //Assert that we are redirected to a new page and that the page contains the success message
        // Check that we've been redirected properly
        $this->assertEquals("http://localhost/login?justRegistered=1", $client->getCrawler()->getUri());

        // Assert text
        $successMessage = $crawler->filter(".modal-body")->text();
        $this->assertEquals("Welcome to Saskatoon Summer Players. You can now log in using your account.", $successMessage);
    }

    /**
     * Assert that an error message is displayed when the addressLineTwo is too long
     * @author Kate Z, MacKenzie W
     */
    public function testaddressLineTwoTooLong()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $form = $crawler->selectButton('Submit')->form();

        $form['member[addressLineTwo]']->setValue("thistextiswaytooolongitsohuldprobablynot gooverabout100charactersmaybeleftstryitout myguyyesyesyesyesd");

        $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Address Line Two must be under 100 characters", $client->getResponse()->getContent());
    }

    /**
     * Assert that an error message is shown when nothing is in the City field because it is required
     * @author Dylan S, Christopher B
     */
    public function testCityRequired()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $form = $crawler->selectButton('Submit')->form();

        $form['member[city]']->setValue("");

        $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("City is required", $client->getResponse()->getContent());
    }

    /**
     * Assert that you are redirected to a new page if the city field is almost too long but still ok
     * @author Dylan S, Christopher B
     */
    public function testCityAlmostTooLong()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;


        $form['member[city]']->setValue("ajlfalshfkljadhfkasdfhk fdafhjlasdfhlkjsadhfjklashflkshfjlsdahfksdfflafdsakjlfhksfhahfaksfhlaskhfjk");


        $client->submit($form);

        $client->followRedirect();

        $crawler = $client->getCrawler();

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost/login?justRegistered=1", $client->getCrawler()->getUri());

        // Assert text
        $successMessage = $crawler->filter(".modal-body")->text();
        $this->assertEquals("Welcome to Saskatoon Summer Players. You can now log in using your account.", $successMessage);
    }

    /**
     * Assert that there is an error message that is shown when the City field is too long
     * @author Dylan S, Christopher B
     */
    public function testCityTooLong()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $form = $crawler->selectButton('Submit')->form();

        $form['member[city]']->setValue("thistextiswaytooolongitsohuldprobablynot gooverabout100charactersmaybeleftstryitout myguyyesyesyesyesd");

        $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("City must be under 100 characters", $client->getResponse()->getContent());
    }

    /**
     * Assert that there is an error message shown when there is nothing in the postal code field because it is required
     * @author Dylan S, Christopher B
     */
    public function testPostalCodeRequired()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $form = $crawler->selectButton('Submit')->form();

        $form['member[postalCode]']->setValue("");

        $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Postal Code is required", $client->getResponse()->getContent());
    }

    /**
     * Assert that an error message is shown when the postal code does not contain a number
     * @author Dylan S, Christopher B
     */
    public function testPostalCodeDoesNotContainANumber()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $form = $crawler->selectButton('Submit')->form();

        $form['member[postalCode]']->setValue("ABCABC");

        $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Postal Code should be in the format of A2A3B3 or A2A 3B3", $client->getResponse()->getContent());
    }
    /**
     * Assert that an error message is shown when the postal code does not contain a letter
     * @author Dylan S, Christopher B
     */
    public function testPostalCodeDoesNotContainALetter()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $form['member[postalCode]']->setValue("222333");


        $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Postal Code should be in the format of A2A3B3 or A2A 3B3", $client->getResponse()->getContent());
    }

    /**
     * Assert that you are redirected to the success page when the postal code is almost too long but not quite
     * Boundary test
     * @author Dylan S, Christopher B
     */
    public function testPostalCodeIsAlmostTooLong()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $form['member[postalCode]']->setValue("S4N 4B7");

        $client->submit($form);

        $client->followRedirect();

        $crawler = $client->getCrawler();

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        //Assert that we are redirected to a new page and that the page contains the success message
        // Check that we've been redirected properly
        $this->assertEquals("http://localhost/login?justRegistered=1", $client->getCrawler()->getUri());

        // Assert text
        $successMessage = $crawler->filter(".modal-body")->text();
        $this->assertEquals("Welcome to Saskatoon Summer Players. You can now log in using your account.", $successMessage);
    }

    /**
     * Assert that an error message is shown when the postal code is too long
     * @author Dylan S, Christopher B
     */
    public function testPostalCodeIsTooLong()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $form = $crawler->selectButton('Submit')->form();

        $form['member[postalCode]']->setValue("A7N 0H07");

        $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Postal Code should be in the format of A2A3B3 or A2A 3B3", $client->getResponse()->getContent());
    }

    /**
     * Assert that an error message is shown when the postal code is too Short
     * @author Dylan S, Christopher B
     */
    public function testPostalCodeIsTooShort()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $form = $crawler->selectButton('Submit')->form();

        $form['member[postalCode]']->setValue("A7N0H");

        $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Postal Code should be in the format of A2A3B3 or A2A 3B3", $client->getResponse()->getContent());
    }


    /**
     * Assert that you are redirected to the success page when you select the valid value is entered
     * @author Dylan S, Christopher B, MacKenzie W, Kate Z
     */
    public function testProvinceValidValue()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $form['member[province]']->setValue("NL");

        $client->submit($form);

        $client->followRedirect();

        $crawler = $client->getCrawler();

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost/login?justRegistered=1", $client->getCrawler()->getUri());

        // Assert text
        $successMessage = $crawler->filter(".modal-body")->text();
        $this->assertEquals("Welcome to Saskatoon Summer Players. You can now log in using your account.", $successMessage);
    }

    /**
     * Assert that an error message is shown when you enter an invalid province
     * @expectedException InvalidArgumentException
     * @author Dylan S, Christopher B, Kate Z, MacKenzie W
     */
    public function testProvinceInvalidValue()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $form['member[province]']->setValue("aj");


        $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Need to enter Province", $client->getResponse()->getContent());

    }
    /**
     * Assert that an error message is shown when you enter an invalid province
     * @author Dylan S, Christopher B, Kate Z, MacKenzie W
     */
    public function testProvinceBlankValue()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $form = $crawler->selectButton('Submit')->form();

        $form['member[province]']->setValue("");

        $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Need to enter Province", $client->getResponse()->getContent());

    }


    /**
     * Assert that you are redirected to the success page when you leave the company field blank because it is not required
     * @author Dylan S, Christopher B
     */
    public function testCompanyNotRequired()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;



        $form['member[company]']->setValue("");

        $client->submit($form);

        $client->followRedirect();

        $crawler = $client->getCrawler();

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost/login?justRegistered=1", $client->getCrawler()->getUri());

        // Assert text
        $successMessage = $crawler->filter(".modal-body")->text();
        $this->assertEquals("Welcome to Saskatoon Summer Players. You can now log in using your account.", $successMessage);
    }

    /**
     * Assert that an error message is shown when the company field is too long
     * @author Dylan S, Christopher B
     */
    public function testCompanyTooLong()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $form = $crawler->selectButton('Submit')->form();

        $form['member[company]']->setValue(str_repeat('a', 101));

        $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Company is invalid", $client->getResponse()->getContent());
    }

    /**
     * Assert that you are redirected to the success page when you enter the maximum amount of chracters for the company field
     * @author Dylan S, Christopher B
     */
    public function testCompanyAlmostTooLong()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;


        $form['member[company]']->setValue(str_repeat('a', 100));

        $client->submit($form);

        $client->followRedirect();

        $crawler = $client->getCrawler();

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost/login?justRegistered=1", $client->getCrawler()->getUri());

        // Assert text
        $successMessage = $crawler->filter(".modal-body")->text();
        $this->assertEquals("Welcome to Saskatoon Summer Players. You can now log in using your account.", $successMessage);
    }

    /**
     * Assert that you are redirected to the success page when you leave the phone field blank because it is not required
     * @author Dylan S, Christopher B
     */
    public function testPhoneNotRequired()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;


        $form['member[phone]']->setValue("");

        $client->submit($form);

        $client->followRedirect();

        $crawler = $client->getCrawler();

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost/login?justRegistered=1", $client->getCrawler()->getUri());

        // Assert text
        $successMessage = $crawler->filter(".modal-body")->text();
        $this->assertEquals("Welcome to Saskatoon Summer Players. You can now log in using your account.", $successMessage);
    }


    /**
     * Assert that an error message is shown when the phone number is too long
     * @author Dylan S, Christopher B
     */
    public function testPhoneTooLong()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $form = $crawler->selectButton('Submit')->form();

        $form['member[phone]']->setValue("(293) 456-78999");

        $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Please enter a 10-digit phone number", $client->getResponse()->getContent());
    }

    /**
     * Assert that an error message is shown when the phone number is too short
     * @author Dylan S, Christopher B
     */
    public function testPhoneTooShort()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $form['member[phone]']->setValue("(293) 456-789");

        $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Please enter a 10-digit phone number", $client->getResponse()->getContent());
    }


    /**
     * Assert that an error message is shown if there are any invalid characters in the phone number
     * @author Dylan S, Christopher B
     */
    public function testPhoneContainsInvalidCharacters()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $form['member[phone]']->setValue("(293) 456-78A9");

        $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains("Please enter a 10-digit phone number", $client->getResponse()->getContent());
    }

    /**
     * This test will make sure that a public user can successfully navigate to the terms and conditions page by clicking on the appropriate link
     * @author Nathan, Taylor
     */
    public function testThatTermsAndConditionsPageAccessibleThroughRegistrationPage()
    {
        $crawler= $this->crawler;
        $client = $this->client;

        $link = $crawler->selectLink('Terms and Conditions')->link();
        $crawler = $client->click($link);

        //Assert that the link goes to the terms and conditions page
        $this->assertEquals($crawler->getUri(), "http://localhost/document/1");
        $text = $crawler->filter('h1')->eq(0)->text();
        $this->assertEquals($text, 'Terms and Conditions');
    }

    /**
     * This test will make sure that the terms and conditions page exists
     * @author Nathan, Taylor
     */
    public function testThatTermsAndConditionsPageExists()
    {

        $client = static::createClient();
        $container = $client->getContainer();
        $doctrine = $container->get('doctrine');
        $entityManager = $doctrine->getManager();
        $fixture = new AppFixtures();
        $fixture->load($entityManager);

        $client->request('GET', '/document/1');


        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        //compare title on page to expected one
    }

    /**
     * This will test that the user is successfully redirected after a valid member registration is filled out and submitted (agree to terms checkbox selected)
     * @author Nathan, Taylor
     */
    public function testTermsCheckboxSelected()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;

        $client->submit($form);

        $client->followRedirect();

        $crawler = $client->getCrawler();

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost/login?justRegistered=1", $client->getCrawler()->getUri());

        // Assert text
        $successMessage = $crawler->filter(".modal-body")->text();
        $this->assertEquals("Welcome to Saskatoon Summer Players. You can now log in using your account.", $successMessage);
    }

    /**
     * This will test that the user is not redirected after a valid member registration is filled out and submitted (agree to terms checkbox not selected)
     * @author Nathan, Taylor
     */
    public function testFormNotSubmittedIfCheckboxUnchecked()
    {
        $crawler= $this->crawler;
        $form= $this->form;
        $client = $this->client;
        //deselect the membership tick
        $form['member[membershipAgreement]']->untick();
        $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        //Assert that we are still on the registration page and the form has not been submitted
        $this->assertEquals($client->getCrawler()->getUri(), "http://localhost/registration");
        $this->assertContains('Member Registration', $client->getResponse()->getContent());
    }

    /**
     *This test will test to see if the checkbox was generated on the page
     * @author Nathan, Taylor
     */
    public function testThatTermsCheckboxIsOnPage()
    {
        $client = static::createClient();

        //get the registration page
        $client->request('GET', '/registration');

        $crawler = $client->getCrawler();

        $this->assertContains('id="member_membershipAgreement"', $client->getResponse()->getContent());
    }

    // ******************** Story 17 Tests! *********************//





    /**
     * This test will check that the criminal record/vulnerable person checkbox exists on the registration page.
     * @author MacKenzie Wison and Ankita Rastogi
     */

    public function testThatCriminalRecordCheckBoxIsOnThePage()
    {
        $client = static::createClient();

        //get the registration page
        $client->request('GET', '/registration');

        $crawler = $client->getCrawler();

        $checkbox = $crawler->filter('#member_criminalRecord')->text();

        $this->assertNotNull($checkbox);
    }

    /**
     * This test will check the database and confirm if the checkbox was selected or not.
     * @author MacKenzie Wison and Ankita Rastogi
     */
    public function testThatCriminalRecordStatusIsSavedWhenCheckBoxIsSelected()
    {

        $this->loadFixtures(array(
            'App\DataFixtures\BlankFixtures'
        ));


        //Ticking checkbox for criminal record check.
        $this->form['member[criminalRecord]']->tick();

        $this->client->submit($this->form);

        $container = $this->client->getContainer();
        $memberFromDB = $container->get('doctrine')->getRepository(Member::class)->findOneBy(array('firstName' => "Maxxx2"));

        //Assert that the database stores the value as true.
        $this->assertEquals(1, $memberFromDB->getCriminalRecord());

    }

    /**
     * This function tests to make sure that when the checkbox is not selected the value is saved to the database.
     * @author MacKenzie Wison and Ankita Rastogi
     */
    public function testThatCriminalRecordStatusIsSavedWhenCheckBoxIsNotSelected()
    {
        $this->loadFixtures(array(
            'App\DataFixtures\BlankFixtures'
        ));

        $this->client->submit($this->form);

        $container = $this->client->getContainer();
        $memberFromDB = $container->get('doctrine')->getRepository(Member::class)->findOneBy(array('firstName' => "Maxxx2"));

        //Assert that the database stores the value as false.
        $this->assertEquals(0,$memberFromDB->getCriminalRecord());

    }

    /**
     * This test will ensure that the "Learn More" link in the criminal record checkbox is clickable and
     * it redirects to the document page
     *
     * @author Ankita Rastogi and MacKenzie Wilson
     */
    public function testThatLearnMoreRedirectsOnCriminalRecord()
    {
        $this->loadFixtures(array(
            'App\DataFixtures\AppFixtures'
        ));
        // Get client to start on the show page
        $crawler = $this->client->getCrawler();

        // Verify that the logo link redirects properly to home page
        $learnMore = $crawler->filter('#CriminalRecordCheck')->link();

        $this->client->click($learnMore);

        //$this->client->followRedirect();

        $crawler = $this->client->getCrawler();

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertEquals('http://localhost/document/2', $crawler->getUri());
        $pageTitle = $crawler->filter('h1')->text();
        $this->assertEquals('Why agree to a Criminal Record/Vulnerable Persons check?', $pageTitle);
    }



    /**
     *  This will test that a valid payment token will successfully register the user. It will redirect them to the
     * login page and display a success modal
     *  @author Cory N and Dylan S
     */
    public function testThatTokenIsValidAndRegistrationWorks()
    {
        //$this->form = $this->crawler->selectButton('Pay Now')->form();

        $crawler = $this->crawler;
        $form = $this->form;
        $client = $this->client;

        $client->submit($form);

        $content = $client->getResponse()->getContent();

        $client->followRedirect();
        $crawler = $client->getCrawler();

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost/login?justRegistered=1", $client->getCrawler()->getUri());

        // Assert text
        $successMessage = $crawler->filter(".modal-body")->text();
        $this->assertEquals("Welcome to Saskatoon Summer Players. You can now log in using your account.", $successMessage);

    }

    /*
     * This function will test that a payment token that is too long (29) and is not valid
     * @author Cory N and Dylan S
     */
    public function testThatTokenIsTooLong()
    {
        //$this->form = $this->crawler->selectButton('Pay Now')->form();

        $crawler = $this->crawler;
        $form = $this->form;
        $client = $this->client;

        $form['member[stripeToken]']->setValue("tok_KPte7942xySKBKyrBu11yEpf8");
        $crawler = $client->submit($form);

        //Assert that error message is on the page because token is too long
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals("http://localhost/registration", $client->getCrawler()->getUri());

        $errorText = $crawler->filter(".alert.alert-danger")->text();
        $this->assertContains("Sorry there was a problem with your payment. Please try again.", $errorText);

    }


    /*
     * This function will test that a payment token is too short and is not valid
     * @author Cory N and Dylan S
     */
    public function testThatTokenIsTooShort()
    {

        $crawler = $this->crawler;
        $form = $this->form;
        $client = $this->client;

        $form['member[stripeToken]']->setValue("tok_KPte7942xySKBKyrBu11yEp");
        $crawler = $client->submit($form);

        //Assert that error message is on the page because token is too long
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals("http://localhost/registration", $client->getCrawler()->getUri());

        $errorText = $crawler->filter(".alert.alert-danger")->text();
        $this->assertContains("Sorry there was a problem with your payment. Please try again.", $errorText);
    }


    /*
     * This function will test that a payment token that does not start with "tok_" is not valid
     * @author Cory N and Dylan S
     */
    public function testThatTokenDoesntStartWithTok_()
    {

        $crawler = $this->crawler;
        $form = $this->form;
        $client = $this->client;

        $form['member[stripeToken]']->setValue("tob_KPte7942xySKBKyrBu11yEpf8");
        $crawler = $client->submit($form);

        //Assert that error message is on the page because token is too long
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals("http://localhost/registration", $client->getCrawler()->getUri());

        $errorText = $crawler->filter(".alert.alert-danger")->text();
        $this->assertContains("Sorry there was a problem with your payment. Please try again.", $errorText);
    }


    /*
     * This function will test that the starting "tok_" is case sensitive
     * @author Cory N and Dylan S
     */
    public function testThatTokenTokIsCaseSensitive()
    {
        $crawler = $this->crawler;
        $form = $this->form;
        $client = $this->client;

        $form['member[stripeToken]']->setValue("TOK_KPte7942xySKBKyrBu11yEpf8");
        $crawler = $client->submit($form);

        //Assert that error message is on the page because token is too long
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals("http://localhost/registration", $client->getCrawler()->getUri());

        $errorText = $crawler->filter(".alert.alert-danger")->text();
        $this->assertContains("Sorry there was a problem with your payment. Please try again.", $errorText);
    }


    /*
     * This function will test that no special characters are allowed in the payment token
     * @author Cory N and Dylan S
     */
    public function testThatNoSpecialCharactersInToken()
    {

        $crawler = $this->crawler;
        $form = $this->form;
        $client = $this->client;

        $form['member[stripeToken]']->setValue("tob_KPte7942@ySKBKyrB*11yEpf");
        $crawler = $client->submit($form);

        //Assert that error message is on the page because token is too long
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals("http://localhost/registration", $client->getCrawler()->getUri());

        $errorText = $crawler->filter(".alert.alert-danger")->text();
        $this->assertContains("Sorry there was a problem with your payment. Please try again.", $errorText);
    }

    /*
     * This function will test that spaces are not allowed in the payment token
     * @author Cory N and Dylan S
     */
    public function testThatTokenDoesNOtContainSpaces()
    {
        $crawler = $this->crawler;
        $form = $this->form;
        $client = $this->client;

        $form['member[stripeToken]']->setValue("tob_KPte7942xy SKBKyrBu11yEpf");
        $crawler = $client->submit($form);

        //Assert that error message is on the page because token is too long
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals("http://localhost/registration", $client->getCrawler()->getUri());

        $errorText = $crawler->filter(".alert.alert-danger")->text();
        $this->assertContains("Sorry there was a problem with your payment. Please try again.", $errorText);
    }

}

