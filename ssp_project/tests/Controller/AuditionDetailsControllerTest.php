<?php
/**
 * Created by PhpStorm.
 * User: cst237
<<<<<<< HEAD
 * Date: 1/24/2019
 * Time: 1:41 PM
 */

namespace App\Tests\Controller;

use App\Entity\AuditionDetails;
use App\Entity\Member;
use Liip\FunctionalTestBundle\Test\WebTestCase;

class AuditionDetailsControllerTest extends WebTestCase
{
    public function setUp()
    {
        $this->loadFixtures(array(
            'App\DataFixtures\MemberLoginTestFixture',
            'App\DataFixtures\AllShowsFixtures'
        ));
    }

    /**
     * This will test that blank audition details are created upon the successful submission of a new show.
     * It will attempt to grab the blank audition details from the DB and then check if the fields are indeed blank.
     * @author Christopher and Nathan
     * @throws
     */
    public function testThatBlankAuditionDetailsAreCreatedUponSuccessfulShowSubmission()
    {
        $client = static::createClient();
        //login to the website as a general manager and navigate to the new show page
        $client->request('GET', 'http://localhost:8000/login');
        $crawler = $client->getCrawler();
        $test = $client->getResponse()->getContent();
        $url = $crawler->getUri();
        $form = $crawler->selectButton('Log In')->form();
        $form['_username']->setValue("gmember@member.com");
        $form['_password']->setValue("P@ssw0rd");
        $client->submit($form);
        $client->followRedirect();
        $client->request('GET', 'http://localhost:8000/show/admin/new');
        $crawler = $client->getCrawler();

        //filling out the new show form and submitting it
        $form = $crawler->selectButton('submitNewShow')->form();
        $form['show[name]']->setValue("Our Show");
        $form['show[budget]']->setValue(500);
        $form['show[ticketPrice]']->setValue(50);
        $form['show[synopsis]']->setValue("Testing testing");
        $client->submit($form);
        $client->followRedirect();
        $crawler = $client->getCrawler();

        //assert that we are on the correct page, if the submission was not successful we would not be redirected to this page
        $this->assertEquals($crawler->getUri(), "http://localhost:8000/show/admin/edit_index");

        //get the newly created audition details from the database
        $container = $client->getContainer();
        $someAudition = $container->get('doctrine')->getRepository(AuditionDetails::class)->findBy(array('show'=>6))[0];

        //assert that the audition details match the default audition detail message.
        $this->assertEquals("No Audition Details are available at the moment, please check back later ♥", $someAudition->getAuditionDetails());
    }

    /**
     * This will test that audition details for a show can be accessed from edit show index page, edited, and actually saved.
     * @author Christopher and Nathan
     * @throws
     */
    public function testThatAuditionDetailsForShowCanBeAccessedEditedSaved()
    {
        $client = static::createClient();
        //login to the website as a general manager and navigate to the edit audition details
        $client->request('GET', '/login');
        $crawler = $client->getCrawler();
        $form = $crawler->selectButton('Log In')->form();
        $form['_username']->setValue("gmember@member.com");
        $form['_password']->setValue("P@ssw0rd");
        $client->submit($form);
        $client->followRedirect();
        $client->request('GET', 'http://localhost:8000/auditiondetails/admin/edit/?showID=4');
        $crawler = $client->getCrawler();

        //change the synopsis to a different value
        $form = $crawler->selectButton('submitAuditionDetails')->form();
        $form['audition_details[synopsis]']->setValue("A different synopsis");
        $client->submit($form);
        $client->followRedirect();
        $crawler = $client->getCrawler();

        //assert that we are on the correct page after making the change to the audition details
        $this->assertEquals($crawler->getUri(), "http://localhost:8000/show/admin/edit_index");

        //get the newly created audition details from the database
        $container = $client->getContainer();
        $afterChange = $container->get('doctrine')->getRepository(AuditionDetails::class)->findBy(array('show'=>4))[0];

        //assert that the synopsis match the new synopsis.
        $this->assertEquals("A different synopsis", $afterChange->getSynopsis());
    }

    /**
     * This will test that the audition details for a particular show can indeed be viewed when clicking a link on the appropriate show index
     * @author Christopher and Nathan
     * @throws
     */
    public function testThatAuditionDetailsForShowAreViewable()
    {
        $client = static::createClient();
        //login to the website as a general manager and navigate to the edit audition details
        $client->request('GET', '/login');
        $crawler = $client->getCrawler();
        $form = $crawler->selectButton('Log In')->form();
        $form['_username']->setValue("member@member.com");
        $form['_password']->setValue("P@ssw0rd");
        $client->submit($form);
        $client->followRedirect();
        $client->request('GET', 'http://localhost:8000/show');
        $crawler = $client->getCrawler();

        $form = $crawler->selectButton('showID')->form();
        $client->submit($form);

        $crawler = $client->getCrawler();;

        //assert that we are on the correct page after making the change to the audition details
        $this->assertEquals($crawler->getUri(), "http://localhost:8000/auditiondetails/member/audition/?showID=3");

        $this->assertEquals("Audition Details for Sweeney Todd", $crawler->filter('body > div > div > div > h1')->text());
    }

    /**
     * This test will make sure that audition details is not viewable by non-members
     * @author Christopher and Nathan
     * @throws
     */
    public function testThatAPublicUserCannotViewAuditionDetails()
    {
        $client = static::createClient();
        $client->request('GET', 'http://localhost:8000/show');
        $crawler = $client->getCrawler();

        $count = $crawler->filter(".btn-primary")->count();
        $this->assertEquals($count, 2);

        $client->request('GET', '/login');
        $crawler = $client->getCrawler();
        $form = $crawler->selectButton('Log In')->form();
        $form['_username']->setValue("member@member.com");
        $form['_password']->setValue("P@ssw0rd");
        $client->submit($form);
        $client->followRedirect();
        $client->request('GET', 'http://localhost:8000/show');
        $crawler = $client->getCrawler();

        $count = $crawler->filter(".btn-primary")->count();
        $this->assertTrue($count > 2);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///                                    Story 43 -- Audition Interest Tests                                       ///
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * This test will ensure that a user's interest in auditioning is actually recorded to the database when they click the button.
     *
     * @author Taylor B, Kate Z
     */
    public function testThatAuditionInterestIsRecordedToDB()
    {
        $client = static::createClient();

        $client->request('GET', '/login');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'member@member.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);
        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $this->assertEquals($client->getCrawler()->getUri(), "http://localhost/");
        $client->request('GET', '/auditiondetails/member/audition/?showID=4');
        $this->assertEquals($client->getCrawler()->getUri(), "http://localhost/auditiondetails/member/audition/?showID=4");
//        $crawler = $client->getCrawler();


        $crawler = $client->getCrawler();

        $form = $crawler->selectButton("Notify me About Auditions")->form();
        $client->submit($form);

        $container = $client->getContainer();
        $memberAudition = $container->get('doctrine')->getRepository(AuditionDetails::class)->findOneBy(array('id' => 1));
        $this->assertNotNull($memberAudition);
    }



    /**
     * This test will ensure that a user can audition for more than one show (but only once per show, tested above).
     *
     * @author Taylor B, Kate Z
     */
    public function testThatUserCanAuditionForMultipleShows()
    {
        $client = static::createClient();

        $client->request('GET', '/login');
        $crawler = $client->getCrawler();

        // Target form by using login button
        $loginForm = $crawler->selectButton('Log In')->form();

        // Fill in valid username and password
        $loginForm->setValues([
            '_username' => 'member@member.com',
            '_password' => 'P@ssw0rd',
        ]);

        // Login
        $client->submit($loginForm);
        // Follow redirect to main page
        $client->followRedirect();
        $crawler = $client->getCrawler();

        $this->assertEquals($client->getCrawler()->getUri(), "http://localhost/");
        $client->request('GET', '/auditiondetails/member/audition/?showID=4');
        $this->assertEquals($client->getCrawler()->getUri(), "http://localhost/auditiondetails/member/audition/?showID=4");

        $crawler = $client->getCrawler();

        $form = $crawler->selectButton("Notify me About Auditions")->form();
        $client->submit($form);

        // Ensure member has one audition detail
        $container = $client->getContainer();
        $memberAudition = $container->get('doctrine')->getRepository(Member::class)->findBy(array('id' => 1));
        $this->assertEquals(sizeof($memberAudition), 1);

        $client->request('GET', '/auditiondetails/member/audition/?showID=3');
        $this->assertEquals($client->getCrawler()->getUri(), "http://localhost/auditiondetails/member/audition/?showID=3");

        $crawler = $client->getCrawler();

        $form = $crawler->selectButton("Notify me About Auditions")->form();
        $client->submit($form);

        // Ensure member now has two audition details
        $container = $client->getContainer();
        $registeredMember = $container->get('doctrine')->getRepository(Member::class)->findOneBy(array('id' => 1));


        $this->assertEquals(sizeof($registeredMember->getAuditionDetails()), 2);
    }


}