<?php
namespace App\Tests;

use app\Entity\AuditionDetails;

use DMore\ChromeDriver\ChromeDriver;
use Behat\Mink\Mink;
use Behat\Mink\Session;
use Liip\FunctionalTestBundle\Test\WebTestCase;

/**
 * MINK tests for desktop and mobile on edit audition details
 * @author Christopher and Nathan
 * @package App\Tests
 *
 *
 */
class audition_edit extends WebTestCase
{
    /**
     * @throws \Behat\Mink\Exception\DriverException
     * @throws \Behat\Mink\Exception\UnsupportedDriverActionException
     */
    public function setUp()
    {
        $this->loadFixtures(array(
            'App\DataFixtures\MemberLoginTestFixture',
            'App\DataFixtures\AllShowsFixtures'
        ));
    }

    /**
     * Christopher and Nathan
     * This function will test that:
     * - the card is centered
     * - inputs will be of width 540 when the browser is 1920x1080
     * - labels are displayed to the left of the inputs with their text being right aligned
     * - card header is left aligned
     * - button will be left aligned to input at the bottom of the page
     */
    //start chrome --disable-gpu --headless --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222
    public function testDesktopAlignment()
    {
        $mink = new Mink(array(
            'AuditionDetails' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('AuditionDetails');
        $mink->getSession()->visit('http://localhost:8000/show');

        $session = $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $page = $mink->getSession()->getPage();

        $mink->getSession()->resizeWindow(1000, 1040);

        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("gmember@member.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        $page->find("css", "a#editShows")->click();
        $page->find("css", "button#auditions4")->click();

        $xyInputCoor = <<<JS
        $('.form-control').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;

        // Store input coordinates in arrays
        $inputXY = $mink->getSession()->evaluateScript($xyInputCoor);

        $xyLabelCoor = <<<JS
        $('label').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth(), class: $(element).attr("class")};
        }).get();
JS;

        // Store Label elements in arrays
        $labelXY = $mink->getSession()->evaluateScript($xyLabelCoor);

        $xyButtonCoor = <<<JS
        $('button').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;
        // Store Button elements in arrays
        $buttonXY = $mink->getSession()->evaluateScript($xyButtonCoor);


        $xyCardCoor = <<<JS
        $('div.card').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, right: $(element).outerWidth() + $(element).offset().left, text: $(element).text(),
                    class: $(element).attr("class"), childClass:$(element).children().eq(0).attr("class")};
        }).get();
JS;
        // Store Button elements in arrays
        $cardXY = $mink->getSession()->evaluateScript($xyCardCoor);

        // Get top input element to compare others to
        $xInputVal = $inputXY[0]["left"];
        $inputWidth = 429;

        for ($i = 0; $i < count($inputXY); $i++) {
            // Test that all inputs are left aligned
            $this->assertEquals($xInputVal, $inputXY[$i]["left"]);

            // Each label is in the same row as its input box
            $this->assertEquals($inputXY[$i]["top"], $labelXY[$i]["top"]);

            // Test that each input is the correct width
            $this->assertEquals($inputWidth, $inputXY[$i]["width"]);

            // Assert that the text is right aligned by checking the class attribute
            $this->assertContains("text-sm-right", $labelXY[$i]["class"]);
        }

        // Assert that the button is below the last input tag
        $lastInputTop = end($inputXY)["top"];
        $buttonTop = $buttonXY[1]["top"];

        $this->assertLessThan($buttonTop, $lastInputTop);

        // Assert that the card title is "Update Profile" and is left aligned
        $headerText = $page->find("css", "h2.card-header")->getText();
        $this->assertEquals("Edit Audition Details for Mary Poppins", $headerText);

        $this->assertContains("card-header text-sm-left text-xs-left", $cardXY[0]["childClass"]);
    }

 /**
 * Christopher and Nathan
 * This function will test that:
 * - the card is centered
 * - inputs will be of width 545 when the browser is 575 x 812
 * - labels are displayed above the inputs with their text being left aligned
 * - card header is left aligned
 * - button will be the same width as the input at the bottom of the page
 */
    //start chrome --disable-gpu --headless --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222
    public function testMobileAlignment()
    {

        $mink = new Mink(array(
            'AuditionDetails' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('AuditionDetails');
        $mink->getSession()->visit('http://localhost:8000/show');

        $session = $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $page = $mink->getSession()->getPage();

        $mink->getSession()->resizeWindow(575, 812);

        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("gmember@member.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        $page->find("css", "a#editShows")->click();
        $page->find("css", "button#auditions4")->click();

        $xyInputCoor = <<<JS
        $('.form-control').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;

        // Store input coordinates in arrays
        $inputXY = $mink->getSession()->evaluateScript($xyInputCoor);

        $xyLabelCoor = <<<JS
        $('label').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth(), class: $(element).attr("class")};
        }).get();
JS;

        // Store Label elements in arrays
        $labelXY = $mink->getSession()->evaluateScript($xyLabelCoor);

        $xyButtonCoor = <<<JS
        $('button').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;
        // Store Button elements in arrays
        $buttonXY = $mink->getSession()->evaluateScript($xyButtonCoor);


        $xyCardCoor = <<<JS
        $('div.card').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, right: $(element).outerWidth() + $(element).offset().left, text: $(element).text(),
                    class: $(element).attr("class"), childClass:$(element).children().eq(0).attr("class")};
        }).get();
JS;
        // Store Button elements in arrays
        $cardXY = $mink->getSession()->evaluateScript($xyCardCoor);

        // Get top input element to compare others to
        $xInputVal = $inputXY[0]["left"];
        $xLabelVal = $labelXY[0]["left"];
        $inputWidth = 486;
        $yValLabel = $labelXY[0]["top"];

        for ($i = 0; $i < count($inputXY); $i++) {
            $this->assertEquals($yValLabel, $labelXY[$i]["top"]);

            // The height of the label
            $yValLabel += 51;

            // The height of the input and padding
            $yValLabel += 76;

            // Test that all inputs are left aligned
            $this->assertEquals($xInputVal, $inputXY[$i]["left"]);

            // Test that all labels are left aligned
            $this->assertEquals($xLabelVal, $labelXY[$i]["left"]);

            // Test that each input is the correct width
            $this->assertEquals($inputWidth, $inputXY[$i]["width"]);

            // Assert that the text is left aligned by checking the class attribute
            $this->assertContains("text-sm-right text-xs-left", $labelXY[$i]["class"]);
        }

        // Assert that the button has the same width as the inputs
        $this->assertEquals($inputWidth, $buttonXY[1]["width"]);

        // Assert that the button is below the last input tag
        $lastInputTop = end($inputXY)["top"];
        $buttonTop = $buttonXY[1]["top"];

        $this->assertLessThan($buttonTop, $lastInputTop);

        // Assert that the card is in the center (subtract 17 pixels for the scrollbar)
        $this->assertEquals($cardXY[0]["left"], 575 - $cardXY[0]["right"] - 17);

        // Assert that the card title is "Update Profile" and is left aligned
        $headerText = $page->find("css", "h2.card-header")->getText();
        $this->assertEquals("Edit Audition Details for Mary Poppins", $headerText);

        $this->assertContains("card-header text-sm-left text-xs-left", $cardXY[0]["childClass"]);
    }
}