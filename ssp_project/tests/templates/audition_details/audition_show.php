<?php
namespace App\Tests;

use app\Entity\member;
use app\Entity\auditionDetails;


use DMore\ChromeDriver\ChromeDriver;
use Behat\Mink\Mink;
use Behat\Mink\Session;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use function PHPSTORM_META\elementType;
use WebDriver\Exception;


/**
 * MINK tests for desktop and mobile on show audition details
 * @author Christopher and Nathan
 * @package App\Tests
 *
 *
 */
class audition_show extends WebTestCase
{
    private $fixtures;

    public $mink;
    // https://jqueryvalidation.org/rules/
    /** start chrome --disable-gpu --headless --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222
     * Set up will load fixtures and by default log you in as a board member
     * @throws
     */
    public function setUp()
    {
        $this->fixtures = $this->loadFixtures(array(
            'App\DataFixtures\AddOnlinePollsFixtures',
            'App\DataFixtures\MemberLoginTestFixture',
            'App\DataFixtures\AddMembersFixtures',
            'App\DataFixtures\AllShowsFixtures'
        ));

        $this->mink = new Mink(array(
            'Auditions' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $this->mink ->setDefaultSessionName('Auditions');
        $this->mink ->getSession()->visit('http://localhost:8000/login');

        $page = $this->mink->getSession()->getPage();

    }

    /** 2
     * This will test that a member can apply for an audition and that the appropriate message is displayed
     *
     * @author Kate Zawada and Taylor Beverly
     */
    public function testMemberClicksApplyForAudition()
    {
        $mink = $this->mink;
        $page = $mink->getSession()->getPage();

        // Login
        $page->find("css","#_username")->setValue("member@member.com");
        $page->find("css","#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        //Should now be on homepage, navigate to the option and click to get to the Audition Details page for Marry Poppins
        $page->find("css","#showsNav > a")->click();
        $page->find("css", "#show4")->click();
        $page->find("css", "#showDescription > form > button")->click();

        $pageHTML = $page->getHtml();

        // Check that the thank you message is showing instead of the audition button
        $this->assertNotContains("Notify me About Auditions", $pageHTML);
        $this->assertContains("Thank you for applying to audition!", $pageHTML);
    }

    /** 3
     * This will test that a member who has already applied for an audition cannot do so again
     *
     * @author Kate Zawada and Taylor Beverly
     */
    public function testMemberCannotApplyTwice()
    {
        $mink = $this->mink;
        $page = $mink->getSession()->getPage();

        // Login
        $page->find("css","#_username")->setValue("member@member.com");
        $page->find("css","#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        //Should now be on homepage, navigate to the option and click to get to the Audition Details page for Marry Poppins
        $page->find("css","#showsNav > a")->click();
        $page->find("css", "#show4")->click();
        $page->find("css", "#showDescription > form > button")->click();

        // Check that the thank you message is visible
        $this->assertTrue($page->find("css","#showDescription > span:nth-child(1)")->isVisible());

        // Navigate to the home page, then back again
        $mink ->getSession()->visit('http://localhost:8000/');

        $page->find("css","#showsNav > a")->click();
        $page->find("css", "#show4")->click();

        // Ensure that the button is still not there and the thank you message is
        $pageHTML = $page->getHtml();
        $this->assertNotContains("Notify me About Auditions", $pageHTML);
        $this->assertContains("Thank you for applying to audition!", $pageHTML);
    }

    /** 4
     * This will test for the Audition Details page on Desktop that:
     *
     * The title is above the audition information
     * The audition information is above the "Back to Our Shows" button
     * The picture is to the right of the information
     * The audition button is directly below the title
     *
     * @author Kate Zawada and Taylor Beverly
     */
    public function testPositionOnAuditionDetailsPageDesktop()
    {
        $mink = $this->mink;
        $page = $mink->getSession()->getPage();
        $session =  $mink->getSession();
        $session->wait(2000, '(0 === jQuery.active)');

        // Login
        $page->find("css","#_username")->setValue("member@member.com");
        $page->find("css","#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        $page->find("css","#showsNav > a")->click();
        $page->find("css", "#show4")->click();

        // Select the elements from the page
        $allignedInformationJS = <<<JS

        var returnArray = [];

        returnArray.push($("body > div > div.row.justify-content-center > div > h1").offset().top);                                             // 0 The top offset of the title
        returnArray.push($("#showDescription > form > button").offset().top);                                                                   // 1 The top offset of the audition button
        returnArray.push($("#showDescription > span:nth-child(3)").offset().top);                                                               // 2 The top offset of the Audition Information
        returnArray.push($("#showDescription").offset().left + $("#showDescription").outerWidth);                                               // 3 The right offset of the Audition Details Information
        returnArray.push($("body > div > div.row.showCard.p-sm-1.p-lg-5 > div.col-md-12.col-lg-8.d-none.d-lg-block > img").offset().left);      // 4 The left offset of the Image
          
        return returnArray;
JS;

        $offsetArray = $session->evaluateScript($allignedInformationJS);

        $this->assertTrue($offsetArray[0] < $offsetArray[1]); //top of title is higher than top of audition button
        $this->assertTrue($offsetArray[1] < $offsetArray[2]); //top of audition button is higher than audition information
        $this->assertTrue($offsetArray[3] > $offsetArray[4]); //right of information is further left than left of photo
    }

    /** 5
     * This will test for the Audition Details page on mobile that:
     *
     * The title is above the audition information
     * The audition information is above the "Back to Our Shows" button
     * The picture is above the title
     * The audition button is directly below the title
     *
     * @author Kate Zawada and Taylor Beverly
     */
    public function testButtonIsInTheRightPositionOnAuditionDetailsPageMobile()
    {
        $mink = $this->mink;
        $page = $mink->getSession()->getPage();
        $session =  $mink->getSession();
        $session->wait(2000, '(0 === jQuery.active)');

        // Login
        $page->find("css","#_username")->setValue("member@member.com");
        $page->find("css","#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        $page->find("css","#showsNav > a")->click();
        $page->find("css", "#show4")->click();

        // Select the elements from the page
        $allignedInformationJS = <<<JS

        var returnArray = [];

        returnArray.push($("body > div > div.row.justify-content-center > div > h1").offset().top);                                             // 0 The top offset of the title
        returnArray.push($("#showDescription > form > button").offset().top);                                                                   // 1 The top offset of the audition button
        returnArray.push($("#showDescription > span:nth-child(3)").offset().top);                                                               // 2 The top offset of the Audition Information
        returnArray.push($("body > div > div.row.showCard.p-sm-1.p-lg-5 > div.col-md-12.col-lg-8.d-none.d-lg-block > img").offset().top);       // 3 The top offset of the Image
        return returnArray;
JS;

        $offsetArray = $mink->getSession()->evaluateScript($allignedInformationJS);

        $this->assertTrue($offsetArray[0] < $offsetArray[1]); //top of title is higher than top of audition button
        $this->assertTrue($offsetArray[1] < $offsetArray[2]); //top of audition button is higher than audition information
        $this->assertTrue($offsetArray[3] < $offsetArray[0]); //top of the image is higher than the top of the title
    }

    /**
     * This test will ensure that a user cannot have two audition requests stored on the database for the same show.
     *
     * @author Taylor B, Kate Z
     */
    public function testThatUserCannotAuditionMoreThanOnceForSameShow()
    {

        $mink = $this->mink;
        $page = $mink->getSession()->getPage();
        $session =  $mink->getSession();
        $session->wait(2000, '(0 === jQuery.active)');

        // Login
        $page->find("css","#_username")->setValue("member@member.com");
        $page->find("css","#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        $page->find("css","#showsNav > a")->click();
        $page->find("css", "#show4")->click();
        $page->find("css", "#showDescription > form > button")->click();

        $pageHTML = $page->getHtml();

        // Check that the thank you message is showing instead of the audition button
        $this->assertNotContains("Notify me About Auditions", $pageHTML);
        $this->assertContains("Thank you for applying to audition!", $pageHTML);

        $page->find("css", "#logoutButton")->click();

        // Logout
        $mink ->getSession()->visit('http://localhost:8000/login');

        // Login
        $page->find("css","#_username")->setValue("member@member.com");
        $page->find("css","#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        $page->find("css","#showsNav > a")->click();
        $page->find("css", "#show4")->click();

        $pageHTML = $page->getHtml();

        // Ensure that the "Notify me About Auditions" button is not on the page
        $this->assertNotContains("Notify me About Auditions", $pageHTML);
        $this->assertContains("Thank you for applying to audition!", $pageHTML);

    }

}