<?php
/**
 * User: CST221
 * Date: 1/24/2019
 * SERVER LOGIC BLOCK
 */

namespace App\Tests\Controller;

use app\Entity\ContactSSP;

use DMore\ChromeDriver\ChromeDriver;
use Behat\Mink\Mink;
use Behat\Mink\Session;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use function PHPSTORM_META\elementType;
use WebDriver\Exception;

class contact_index extends WebTestCase
{

    //start chrome --disable-gpu --headless --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222

    /**
     * This function will test that the elements are properly aligned on desktop
     *  - Labels are left to input fields, and right aligned
     *  - Input fields are right to input fields, and right aligned
     *  -Rows are stacked
     *  - Card is center aligned on page
     *  - Button is aligned with input fields
     *
     */
    public function testDesktopAlignment()
    {
        $mink = new Mink(array(
            'ContactUs' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ContactUs');
        $mink->getSession()->visit('http://localhost:8000/contact_us');

        $session =  $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $page = $mink->getSession()->getPage();

        $mink->getSession()->resizeWindow(1000, 1040);

         $xyInputCoor = <<<JS
        $('.form-control').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;

        $xyLabelCoor = <<<JS
        $('label').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;

        $xyButtonCoor = <<<JS
        $('button').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;

        $leftOffsetScript = <<<JS
        return  $(".card").offset().left;
JS;

        $rightOffsetScript = <<<JS
        return $(window).width() - ($(".card").offset().left + $(".card").outerWidth());
JS;

        // Store Label elements in arrays
        $labelXY = $mink->getSession()->evaluateScript($xyLabelCoor);

        // Store input coordinates in arrays
        $inputXY = $mink->getSession()->evaluateScript($xyInputCoor);

        // Store Button elements in arrays
        $buttonXY = $mink->getSession()->evaluateScript($xyButtonCoor);

        // get the card left and right coordinates
        $leftCardXY = $mink->getSession()->evaluateScript($leftOffsetScript);
        $rightCardXY = $mink->getSession()->evaluateScript($rightOffsetScript);

        // Get top input element to compare others to
        $xInputVal = $inputXY[0]["left"];
        $xLabelVal = $labelXY[0]["left"];
        $inputWidth = 429;

        for ($i = 0; $i < count($inputXY); $i++)
        {
            // Test that all inputs are left aligned
            $this->assertEquals($xInputVal, $inputXY[$i]["left"]);

            // Each label is in the same row as its input box
            $this->assertEquals($inputXY[$i]["top"], $labelXY[$i]["top"]);

            // Test that each input is the correct width
            $this->assertEquals($inputWidth, $inputXY[$i]["width"]);
        }

        // Assert that the button is left aligned with inputs
        $this->assertEquals($xInputVal, $buttonXY[1]["left"]);

        // Assert that the button is below the last input tag
        $lastInputTop = end($inputXY)["top"];
        $buttonTop = $buttonXY[1]["top"];

        $this->assertLessThan($buttonTop, $lastInputTop);

        // Assert that the card is in the center of the page
        $this->assertEquals($leftCardXY,$rightCardXY);

        // Assert that the card title is "Update Profile" and is left aligned
        $headerText = $page->find("css", "h2.card-header")->getText();
        $this->assertEquals("Contact Us", $headerText);

       // $this->assertContains("card-header text-sm-left text-xs-left", $cardXY[0]["childClass"]);
    }


    /**
     * This function will test that the elements are properly aligned on desktop
     *  - Labels and inputs take full width
     *  - Input fields and labels are stacked
     *  - Card is center aligned on page
     *  - Button takes full width on screen
     *
     */
    public function testMobileAlignment()
    {
        $mink = new Mink(array(
            'ContactUs' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ContactUs');
        $mink->getSession()->visit('http://localhost:8000/contact_us');

        $session =  $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $page = $mink->getSession()->getPage();

        $mink->getSession()->resizeWindow(575, 812);

        $xyInputCoor = <<<JS
        $('.form-control').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;

        $xyLabelCoor = <<<JS
        $('label').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;

        $xyButtonCoor = <<<JS
        $('button').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;

        $leftOffsetScript = <<<JS
        return  $(".card").offset().left;
JS;

        $rightOffsetScript = <<<JS
        return $(window).width() - ($(".card").offset().left + $(".card").outerWidth());
JS;

        // get the card left and right coordinates
        $leftCardXY = $mink->getSession()->evaluateScript($leftOffsetScript);
        $rightCardXY = $mink->getSession()->evaluateScript($rightOffsetScript);

        // Store input coordinates in arrays
        $inputXY = $mink->getSession()->evaluateScript($xyInputCoor);

        // Store Label elements in arrays
        $labelXY = $mink->getSession()->evaluateScript($xyLabelCoor);

        // Store Button elements in arrays
        $buttonXY = $mink->getSession()->evaluateScript($xyButtonCoor);

        // Get top input element to compare others to
        $xInputVal = $inputXY[0]["left"];
        $xLabelVal = $labelXY[0]["left"];
        $inputWidth = 486;
        $yValLabel = $labelXY[0]["top"];

        for ($i = 0; $i < count($inputXY); $i++)
        {
            $this->assertEquals($yValLabel, $labelXY[$i]["top"]);

            // The height of the label
            $yValLabel += 51;

            // The height of the input and padding
            $yValLabel += 54;

            // Test that all inputs are left aligned
            $this->assertEquals($xInputVal, $inputXY[$i]["left"]);

            // Test that all labels are left aligned
            $this->assertEquals($xLabelVal, $labelXY[$i]["left"]);

            // Test that each input is the correct width
            $this->assertEquals($inputWidth, $inputXY[$i]["width"]);
        }

        // Assert that the button has the same width as the inputs
        $this->assertEquals($inputWidth, $buttonXY[1]["width"]);

        // Assert that the button is below the last input tag
        $lastInputTop = end($inputXY)["top"];
        $buttonTop = $buttonXY[1]["top"];

        $this->assertLessThan($buttonTop, $lastInputTop);

        // Assert that the card is in the center of the page
        $this->assertEquals($leftCardXY,$rightCardXY);

        // Assert that the card title is "Update Profile" and is left aligned
        $headerText = $page->find("css", "h2.card-header")->getText();
        $this->assertEquals("Contact Us", $headerText);
    }

    /**
     * This function will test that the error messages are displayed below respective input fields
     */
    public function testErrorMessagesDisplayedProperlyDesktop()
    {
        $mink = new Mink(array(
            'ContactUs' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ContactUs');
        $mink->getSession()->visit('http://localhost:8000/contact_us');

        $session =  $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $page = $mink->getSession()->getPage();

       $mink->getSession()->resizeWindow(1000, 1040);

        $page->find("css", "#contact_ssp_name")->setValue("");
        $page->find("css", "#contact_ssp_email")->setValue("");
        $page->find("css", "#contact_ssp_phone")->setValue("");
        $page->find("css", "#contact_ssp_subject")->setValue("");
        $page->find("css", "#contact_ssp_comments")->setValue("");
        $page->find("css", "#sendContact")->click();


        $errorScript = <<<JS
        $("[id$=error]").map(function(index, element) {
            return { left : $(element).offset().left, top : $(element).offset().top,  class: $(element).attr("class")  };
            }).get();
JS;
        $inputScript= <<<JS
        $('.form-control').map(function(index, element) {
            return { left : $(element).offset().left, top : $(element).offset().top  };
            }).get();
JS;

        $error = $mink->getSession()->evaluateScript($errorScript);
        $input = $mink->getSession()->evaluateScript($inputScript);

        for( $i = 0; $i < count($input); $i++)
        {
            //Test to make sure that the error message is underneath the input
            $this->assertTrue($input[$i]['top'] <= $error[$i]['top'] );

            //Test to make sure that the error messages left offset is the same as input fields left offset.
            $this->assertEquals($input[$i]['left'], $error[$i]['left']);

            // Test that error field contain a class of error_message
            $this->assertContains("alert alert-danger", $error[$i]["class"]);
        }

    }


    /**
     * This function will test that the error messages are displayed below respective input fields
     */
    public function testErrorMessagesDisplayedProperlyMobile()
    {
        $mink = new Mink(array(
            'ContactUs' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ContactUs');
        $mink->getSession()->visit('http://localhost:8000/contact_us');

        $session =  $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $page = $mink->getSession()->getPage();

        $mink->getSession()->resizeWindow(1000, 1040);

        $page->find("css", "#contact_ssp_name")->setValue("");
        $page->find("css", "#contact_ssp_email")->setValue("");
        $page->find("css", "#contact_ssp_phone")->setValue("");
        $page->find("css", "#contact_ssp_subject")->setValue("");
        $page->find("css", "#contact_ssp_comments")->setValue("");
        $page->find("css", "#sendContact")->click();


        $errorScript = <<<JS
        $("[id$=error]").map(function(index, element) {
            return { left : $(element).offset().left, top : $(element).offset().top,  class: $(element).attr("class")  };
            }).get();
JS;
        $inputScript= <<<JS
        $('.form-control').map(function(index, element) {
            return { left : $(element).offset().left, top : $(element).offset().top  };
            }).get();
JS;

        $mink->getSession()->resizeWindow(575, 812);

        $error = $mink->getSession()->evaluateScript($errorScript);
        $input = $mink->getSession()->evaluateScript($inputScript);


        for( $i = 0; $i < count($error); $i++)
        {
            //Test to make sure that the error message is underneath the input
            $this->assertTrue($input[$i]['top'] < $error[$i]['top'] );

            //Test to make sure that the error messages left offset is the same as input fields left offset.
            $this->assertEquals($input[$i]['left'], $input[$i]['left']);
        }
    }


    /**
     * This function tests to make sure that the user cannot submit the form when an name field is empty
     *
     * @author Ankita Rastogi
     */
    public function testNameEmptyIsInvalid()
    {
        $mink = new Mink(array(
            'ContactUs' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ContactUs');
        $mink->getSession()->visit('http://localhost:8000/contact_us');

        $session =  $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $page = $mink->getSession()->getPage();

        $page->find("css", "#contact_ssp_name")->setValue("");
        $page->find("css", "#contact_ssp_email")->setValue("email@a.com");
        $page->find("css", "#contact_ssp_subject")->setValue("upcoming shows");
        $page->find("css", "#contact_ssp_name")->click();

        $errorText = $page->find("css", "#contact_ssp_name-error" )->getText();
        $this->assertContains("Name is required",$errorText);
    }

    /**
     *  This function tests to make sure that the user cannot submit the form when an name field is more than 40 characters
     *
     * @author Ankita Rastogi
     */
    public function testNameTooLongIsInvalid()
    {
        $mink = new Mink(array(
            'ContactUs' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ContactUs');
        $mink->getSession()->visit('http://localhost:8000/contact_us');

        $session =  $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $page = $mink->getSession()->getPage();

        $page->find("css", "#contact_ssp_name")->setValue("AnkitaAnkitaAnkitaAnkitaAnkitaAnkitaAnkita");
        $page->find("css", "#contact_ssp_email")->setValue("mail@mail.com");
        $page->find("css", "#contact_ssp_subject")->setValue("upcoming shows");
        $page->find("css", "#contact_ssp_name")->click();
        $errorText = $page->find("css", "#contact_ssp_name-error" )->getText();
        $this->assertContains("Name must be 1-40 characters in length",$errorText);
    }

    /**
     * This function tests to make sure that the user can submit the form when an email format is correct
     *
     * @author Ankita Rastogi
     */
    public function testEmailFormatInValid()
    {
        $mink = new Mink(array(
            'ContactUs' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ContactUs');
        $mink->getSession()->visit('http://localhost:8000/contact_us');

        $session =  $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $page = $mink->getSession()->getPage();

        $page->find("css", "#contact_ssp_name")->setValue("Ankita");
        $page->find("css", "#contact_ssp_email")->setValue("com");
        $page->find("css", "#contact_ssp_subject")->setValue("upcoming shows");
        $page->find("css", "#contact_ssp_name")->click();
        $errorText = $page->find("css", "#contact_ssp_email-error" )->getText();
        $this->assertContains("Please enter a valid email address.",$errorText);
    }


    /**
     *  This function tests to make sure that the user cannot submit the form when an email field is more than 50 characters
     *
     * @author Ankita Rastogi
     */
    public function testEmailTooLongIsInvalid()
    {
        $mink = new Mink(array(
            'ContactUs' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ContactUs');
        $mink->getSession()->visit('http://localhost:8000/contact_us');

        $session =  $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $page = $mink->getSession()->getPage();

        $page->find("css", "#contact_ssp_name")->setValue("Ankita");
        $page->find("css", "#contact_ssp_email")->setValue("avvvvvvvvvvvvvvvvvwwqvvqwwwwwwwwwwwewewewewqq@a.com");
        $page->find("css", "#contact_ssp_subject")->setValue("upcoming shows");
        $page->find("css", "#contact_ssp_name")->click();

        $errorText = $page->find("css", "#contact_ssp_email-error" )->getText();
        $this->assertContains("Email must be less than 50 characters without spaces",$errorText);
    }
    /**
     * This function tests to make sure that the user cannot submit the form when an phone field is empty
     *
     * @author Ankita Rastogi
     */
    public function testPhoneEmptyIsInvalid()
    {
        $mink = new Mink(array(
            'ContactUs' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ContactUs');
        $mink->getSession()->visit('http://localhost:8000/contact_us');

        $session =  $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $page = $mink->getSession()->getPage();

        $page->find("css", "#contact_ssp_name")->setValue("Ankita");
        $page->find("css", "#contact_ssp_phone")->setValue("");
        $page->find("css", "#contact_ssp_email")->setValue("adfs");
        $page->find("css", "#contact_ssp_subject")->setValue("upcoming shows");
        $page->find("css", "#contact_ssp_name")->click();

        $errorText = $page->find("css", "#contact_ssp_phone-error" )->getText();
        $this->assertContains("Email address or phone number is required",$errorText);
    }

    /**
     * This function tests to make sure that the user cannot submit the form when an Phone field format is incorrect
     *
     * @author Ankita Rastogi
     */
    public function testPhoneFormatInvalid()
    {
        $mink = new Mink(array(
            'ContactUs' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ContactUs');
        $mink->getSession()->visit('http://localhost:8000/contact_us');

        $session =  $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $page = $mink->getSession()->getPage();

        $page->find("css", "#contact_ssp_name")->setValue("Ankita");
        $page->find("css", "#contact_ssp_phone")->setValue("88899988");
        $page->find("css", "#contact_ssp_email")->setValue("");
        $page->find("css", "#contact_ssp_subject")->setValue("upcoming shows");
        $page->find("css", "#contact_ssp_name")->click();

        $errorText = $page->find("css", "#contact_ssp_phone-error" )->getText();
        $this->assertContains("Please enter a 10-digit phone number",$errorText);
    }


    /**
     * This function tests to make sure that the user can submit the form when an either one of the phone or email is filled
     * correctly
     *
     * @author Ankita Rastogi
     */
    public function testEmailEmptyPhoneFilledIsValid()
    {
        $mink = new Mink(array(
            'ContactUs' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ContactUs');
        $mink->getSession()->visit('http://localhost:8000/contact_us');

        $session =  $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $page = $mink->getSession()->getPage();

        $page->find("css", "#contact_ssp_name")->setValue("Ankita");
        $page->find("css", "#contact_ssp_phone")->setValue("3067897890");
        $page->find("css", "#contact_ssp_email")->setValue("");
        $page->find("css", "#contact_ssp_subject")->setValue("upcoming shows");
        $page->find("css", "#contact_ssp_name")->click();

        $errorText = $page->find("css", "#contact_ssp_phone-error" );
        $errorText2 = $page->find("css", "#contact_ssp_email-error" );
        $this->assertNull($errorText);
        $this->assertNull($errorText2);
    }

    /**
     * This function tests to make sure that the user can submit the form when an either one of the phone or email is filled
     * correctly
     *
     * @author Ankita Rastogi
     */
    public function testEmailFilledPhoneEmptyIsValid()
    {
        $mink = new Mink(array(
            'ContactUs' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ContactUs');
        $mink->getSession()->visit('http://localhost:8000/contact_us');

        $session =  $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $page = $mink->getSession()->getPage();

        $page->find("css", "#contact_ssp_name")->setValue("Ankita");
        $page->find("css", "#contact_ssp_email")->setValue("a@a.com");
        $page->find("css", "#contact_ssp_phone")->setValue("");
        $page->find("css", "#contact_ssp_subject")->setValue("upcoming shows");
        $page->find("css", "#contact_ssp_name")->click();

        $errorText = $page->find("css", "#contact_ssp_phone-error" );
        $errorText2 = $page->find("css", "#contact_ssp_email-error" );
        $this->assertNull($errorText);
        $this->assertNull($errorText2);
    }


    /**
 * This function tests to make sure that the user can submit the form when an option is
 * selected in from the subject field.
 *
 *  @author Ankita Rastogi
 */
    public function testSubjectFilledIsValid()
    {
        $mink = new Mink(array(
            'ContactUs' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ContactUs');
        $mink->getSession()->visit('http://localhost:8000/contact_us');

        $session =  $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $page = $mink->getSession()->getPage();

        $page->find("css", "#contact_ssp_name")->setValue("Ankita");
        $page->find("css", "#contact_ssp_email")->setValue("a@a.com");
        $page->find("css", "#contact_ssp_phone")->setValue("3067897890");
        $page->find("css", "#contact_ssp_subject")->setValue("upcoming shows");
        $page->find("css", "#contact_ssp_name")->click();

        $subjectField = $page->find("css", "#contact_ssp_subject")->getValue();
        $this->assertEquals("upcoming shows", $subjectField);

        $errorText = $page->find("css", "#contact_ssp_subject-error" );
        $this->assertNull($errorText);
    }

    /**
     * This function tests to make sure that the user cannot submit the form when no option is
     *  selected in from the subject field.
     *
     *  @author Ankita Rastogi
     */
    public function testSubjectEmptyIsInvalid()
    {
        $mink = new Mink(array(
            'ContactUs' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ContactUs');
        $mink->getSession()->visit('http://localhost:8000/contact_us');

        $session =  $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $page = $mink->getSession()->getPage();

        $page->find("css", "#contact_ssp_name")->setValue("Ankita");
        $page->find("css", "#contact_ssp_email")->setValue("a@a.com");
        $page->find("css", "#contact_ssp_phone")->setValue("3067897890");
        $page->find("css", "#contact_ssp_subject")->setValue("");
        $page->find("css", "#contact_ssp_comments")->setValue("This is test");
        $page->find("css", "#contact_ssp_name")->click();
        $errorText = $page->find("css", "#contact_ssp_subject-error" )->getText();
        $this->assertContains("Choose an option from list",$errorText);
    }


    /**
     * This function tests to make sure that the user can submit the form when comment is filled
     *
     *  @author Ankita Rastogi
     */
    public function testCommentFilledIsValid()
    {
        $mink = new Mink(array(
            'ContactUs' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ContactUs');
        $mink->getSession()->visit('http://localhost:8000/contact_us');

        $session =  $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $page = $mink->getSession()->getPage();

        $page->find("css", "#contact_ssp_name")->setValue("Ankita");
        $page->find("css", "#contact_ssp_email")->setValue("a@a.com");
        $page->find("css", "#contact_ssp_phone")->setValue("3067897890");
        $page->find("css", "#contact_ssp_subject")->setValue("upcoming shows");
        $page->find("css", "#contact_ssp_comments")->setValue("This is test");
        $page->find("css", "#contact_ssp_name")->click();

        $errorText = $page->find("css", "#contact_ssp_comments-error" );
        $this->assertNull($errorText);
    }

    /**
     * This function tests to make sure that the user can submit the form when an option is
     * selected in from the subject field.
     *
     *  @author Ankita Rastogi
     */
    public function testCommentsEmptyIsInvalid()
    {
        $mink = new Mink(array(
        'ContactUs' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
    ));
        $mink->setDefaultSessionName('ContactUs');
        $mink->getSession()->visit('http://localhost:8000/contact_us');

        $session =  $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $page = $mink->getSession()->getPage();

        $page->find("css", "#contact_ssp_name")->setValue("Ankita");
        $page->find("css", "#contact_ssp_email")->setValue("a@a.com");
        $page->find("css", "#contact_ssp_phone")->setValue("3067897890");
        $page->find("css", "#contact_ssp_subject")->setValue("upcoming shows");
        $page->find("css", "#contact_ssp_comments")->setValue("");
        $page->find("css", "#contact_ssp_name")->click();

        $errorText = $page->find("css", "#contact_ssp_comments-error" )->getText();
        $this->assertContains("Comment is required",$errorText);
    }



    /**
     * This function tests to make sure that the user can submit the form when all fields are filled correctly
     *
     *  @author Ankita Rastogi and Chris
     */
    public function testModalVisibleOnAllValidFields()
    {
        $mink = new Mink(array(
            'ContactUs' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ContactUs');
        $mink->getSession()->visit('http://localhost:8000/contact_us');

        $session =  $mink->getSession();
        $driver = $mink->getSession()->getDriver();

        $session->wait(60000, '(0 === jQuery.active)');

        $page = $mink->getSession()->getPage();

        $page->find("css", "#contact_ssp_name")->setValue("Ankita");
        $page->find("css", "#contact_ssp_email")->setValue("a@a.com");
        $page->find("css", "#contact_ssp_phone")->setValue("3067897890");
        $page->find("css", "#contact_ssp_subject")->setValue("upcoming shows");
        $page->find("css", "#contact_ssp_comments")->setValue("Test");
        $page->find("css", "#sendContact")->click();
        $url = $driver->getCurrentUrl();
        $this->assertContains("contactUsRequest", $url);
    }


    /**
     * This function tests to make sure that the user can submit the form when an either one of the phone or email is filled
     * correctly
     *
     * @author Ankita Rastogi
     */
    public function testSubmitEmailEmptyPhoneFilledIsValid()
    {
        $mink = new Mink(array(
            'ContactUs' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ContactUs');
        $mink->getSession()->visit('http://localhost:8000/contact_us');

        $session =  $mink->getSession();
        $driver = $mink->getSession()->getDriver();
        $session->wait(60000, '(0 === jQuery.active)');

        $page = $mink->getSession()->getPage();

        $page->find("css", "#contact_ssp_name")->setValue("Ankita");
        $page->find("css", "#contact_ssp_phone")->setValue("3067897890");
        $page->find("css", "#contact_ssp_subject")->setValue("upcoming shows");
        $page->find("css", "#contact_ssp_comments")->setValue("Test");
        $page->find("css", "#contact_ssp_email")->setValue("");
        $page->find("css", "#sendContact")->click();

        $url = $driver->getCurrentUrl();
        $this->assertContains("contactUsRequest", $url);
    }

    /**
     * This function tests to make sure that the user can submit the form when an either one of the phone or email is filled
     * correctly
     *
     * @author Ankita Rastogi
     */
    public function testSubmitEmailFilledPhoneEmptyIsValid()
    {
        $mink = new Mink(array(
            'ContactUs' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ContactUs');
        $mink->getSession()->visit('http://localhost:8000/contact_us');

        $session =  $mink->getSession();
        $driver = $mink->getSession()->getDriver();
        $session->wait(60000, '(0 === jQuery.active)');

        $page = $mink->getSession()->getPage();

        $page->find("css", "#contact_ssp_name")->setValue("Ankita");
        $page->find("css", "#contact_ssp_email")->setValue("a@a.com");
        $page->find("css", "#contact_ssp_subject")->setValue("upcoming shows");
        $page->find("css", "#contact_ssp_comments")->setValue("Test");
        $page->find("css", "#contact_ssp_phone")->setValue("");
        $page->find("css", "#sendContact")->click();

        $url = $driver->getCurrentUrl();
        $this->assertContains("contactUsRequest", $url);
    }

}