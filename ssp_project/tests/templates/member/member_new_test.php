<?php
namespace App\Tests;

use DMore\ChromeDriver\ChromeDriver;
use Behat\Mink\Mink;
use Behat\Mink\Session;
use Liip\FunctionalTestBundle\Test\WebTestCase;

/**
 * Class ShowControllerTest
 * @author MacKenzie Wilson and Ankita Rastogi
 * @package App\Tests
 *
 *
 * This class tests layout and styling options for the member registration page.
 *
 * *************************************************************
 * * From command prompt:
 * 1) start chrome --disable-gpu --headless --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222
 *
 */
class MemberRegistrationClientSideValidationTest extends WebTestCase
{

    /**
     * @throws \Behat\Mink\Exception\DriverException
     * @throws \Behat\Mink\Exception\UnsupportedDriverActionException
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function setUp()
    {

        $mink = new Mink(array(
            'showBrowser' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('showBrowser');

        $mink->getSession()->visit('http://localhost:8000/registration');

        /** @var ChromeDriver $driver */
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_firstName")->setValue("Ankita");
        $page->find("css", "#member_lastName")->setValue("Rastogi");
        $page->find("css", "#member_userName")->setValue("ankita@gmail.com");
        $page->find("css", "#member_password_first")->setValue("Ankita1234");
        $page->find("css", "#member_password_second")->setValue("Ankita1234");
        $page->find("css", "#member_addressLineOne")->setValue("123 Main Street");
        $page->find("css", "#member_addressLineTwo")->setValue("123 Fake St");
        $page->find("css", "#member_city")->setValue("Saskatoon");
        $page->find("css", "#member_postalCode")->setValue("S7W 0H4");
        $page->find("css", "#member_province")->setValue("SK");
        $page->find("css", "#member_company")->setValue("SSP");
        $page->find("css", "#member_phone")->setValue("3069896565");
        $page->find("css", "#member_memberType")->setValue("Individual");
        $page->find("css", "#member_memberOption")->setValue("1-Year Paid Membership");
        $page->find("css", "#member_membershipAgreement")->setValue(true);


    }

    /*******************************************************RESPONSIVENESS TESTS ********************************/
    /**
     * This function will test that the member registration page is responsive on mobile and the elements
     * take the full screen width.
     * @author Ankita Rastogi
     */
    public function testRegistrationPageIsResponsiveOnMobile()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $mink->getSession()->resizeWindow(575, 667); //width and height of mobile
        $registrationFormScript = <<<JS
            return formWidth = $("#memberReg").outerWidth();
JS;
        //outerwidth
        $resizeDimension = $mink->getSession()->evaluateScript($registrationFormScript);
        $this->assertEquals(498, $resizeDimension);

    }

    /**
     * This function test to make sure the registration form is center aligned on the page.
     *
     * @author MacKenzie Wilson & Ankita
     */
    public function testDesktopFormIsCenterAligned()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $mink->getSession()->resizeWindow(1000, 760); //width and height of iphone

        $leftOffsetScript = <<<JS
        return  $(".card").offset().left;
JS;

        $rightOffsetScript = <<<JS
        return $(window).width() - ($(".card").offset().left + $(".card").outerWidth());
JS;

        $leftOffset = $mink->getSession()->evaluateScript($leftOffsetScript);
        $rightOffset = $mink->getSession()->evaluateScript($rightOffsetScript);

        $this->assertEquals($leftOffset, $rightOffset);

    }

    /**
     * This function will test that the labels and input fields will take 100 percent width on mobile view
     * take the full screen width.
     * @author Ankita Rastogi & MacKenzie Wilson
     */
    public function testMobileTheLabelAndInputFieldsTake100Width()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $mink->getSession()->wait(60000, '(0 === jQuery.active)');
        $mink->getSession()->resizeWindow(575, 667); //width and height of iphone

        //Get the width of all the labels
        $widthScript = <<<JS
            $('.form-group').map(function(index, element) {
              return { width : $(element).outerWidth() };
            }).get();
JS;
        $width = $mink->getSession()->evaluateScript($widthScript);

        //look through all the Label's left offset and confirm that they equal and have
        foreach($width as $key=>$val)
        {
            $this->assertEquals(456, $val["width"]);
        }

    }



    /**
     *This function will test that the label is aligned left of input fields and select boxes
     * @author Ankita Rastogi and MacKenzie Wilson
     */
    public function testDesktopLabelAreLeftToInputField()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        //Wait for page to load
        $mink->getSession()->wait(60000, '(0 === jQuery.active)');
        $mink->getSession()->resizeWindow(1000, 768); //width and height of desktop
        $labelWidthScript = <<<JS
            $('label').map(function(index, element) {
              return { left : $(element).offset().left, top :  $(element).offset().top   };
            }).get();
JS;
        $labelWidth = $mink->getSession()->evaluateScript($labelWidthScript);

        //Get the width of all the labels
        $inputWidthScript = <<<JS
            $('.form-control').map(function(index, element) {
              return { left : $(element).offset().left,  top :  $(element).offset().top };
            }).get();
JS;
        $inputWidth = $mink->getSession()->evaluateScript($inputWidthScript);

        //look through all the Label's left offset and confirm that they equal and have
        for( $i = 0; $i < count($inputWidth); $i++)
        {
            $this->assertTrue($labelWidth[$i]["left"] < $inputWidth[$i]["left"] );
            $this->assertEquals($labelWidth[$i]["top"], $inputWidth[$i]["top"] );
        }
    }



    /**
     *This function will test that the label and input fields are stacked
     * @author Ankita Rastogi and MacKenzie Wilson
     */
    public function testMobileLabelAndInputFieldAreStacked()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        //Wait for page to load
        $mink->getSession()->wait(60000, '(0 === jQuery.active)');
        $mink->getSession()->resizeWindow(575, 667); //width and height of mobile
        $labelWidthScript = <<<JS
            $('label').map(function(index, element) {
              return {  top :  $(element).offset().top   };
            }).get();
JS;
        $labelWidth = $mink->getSession()->evaluateScript($labelWidthScript);

        //Get the width of all the labels
        $inputWidthScript = <<<JS
            $('.form-control').map(function(index, element) {
              return { top :  $(element).offset().top };
            }).get();
JS;
        $inputWidth = $mink->getSession()->evaluateScript($inputWidthScript);

        //look through all the Label's left offset and confirm that they equal and have
        for( $i = 0; $i < count($inputWidth); $i++)
        {
            $this->assertTrue($labelWidth[$i]["top"] < $inputWidth[$i]["top"] );
        }
    }


    /**
     * This function will test the submit button is left aligned with labels on desktop
     *
     * @author Ankita Rastogi and MacKenzie Wilson
     */
    public function testDesktopSubmitButtonLeftAlign()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        //Wait for page to load
        $mink->getSession()->wait(60000, '(0 === jQuery.active)');
        $mink->getSession()->resizeWindow(1000, 768); //width and height of desktop
        $labelLeftScript = <<<JS
            return $("input:first-child").offset().left 
           
JS;
        $labelLeft = $mink->getSession()->evaluateScript($labelLeftScript);

        //Get the width of all the labels
        $submitButtonScript = <<<JS
           return $("#submitButton").offset().left 
JS;
        $submitBtnLeft = $mink->getSession()->evaluateScript($submitButtonScript);

        //look through all the Label's left offset and submit button left offset, confirm that they equal
        $this->assertEquals( $submitBtnLeft, $labelLeft);
    }


    /**
     * This function will test the submit button is centered on the page
     *
     * @author Ankita Rastogi and MacKenzie Wilson
     */
    public function testMobileSubmitButtonCenterAlign()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        //Wait for page to load
        $mink->getSession()->wait(60000, '(0 === jQuery.active)');
        $mink->getSession()->resizeWindow(575, 667); //width and height of mobile

        $leftOffsetScript = <<<JS
        return  $("button").offset().left;
JS;

        $rightOffsetScript = <<<JS
        return  $(window).width() - ($("button").offset().left + $("button").outerWidth());
JS;


        $submitCentered = $mink->getSession()->evaluateScript($leftOffsetScript);
        $submitCentered = $mink->getSession()->evaluateScript($rightOffsetScript);

        //look through all the Label's left offset and submit button left offset, confirm that they equal
        $this->assertEquals( $submitCentered, $submitCentered);
    }



    /**
     * This function will test that the width of the submit button is correct @250px
     *
     * @author Ankita Rastogi and MacKenzie Wilson
     */
    public function testDesktopSubmitButtonWidth()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $mink->getSession()->wait(60000, '(0 === jQuery.active)');
        $mink->getSession()->resizeWindow(1000, 768); //width and height of desktop

        //Get the width of submit button
        $submitBtnWidthScript = <<<JS
        return  $("#submitButton").outerWidth();
JS;
        $submitBtnWidth = $mink->getSession()->evaluateScript($submitBtnWidthScript);

        $this->assertEquals(159.672, $submitBtnWidth);

    }


    /**
     * This function will test that the width of the submit button takes the whole width of the screen size.
     *
     * @author Ankita Rastogi and MacKenzie Wilson
     */
    public function testMobileSubmitButtonWidth()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $mink->getSession()->wait(60000, '(0 === jQuery.active)');
        $mink->getSession()->resizeWindow(575, 667); //width and height of phone

        //Get the width of submit button
        $submitBtnWidthScript = <<<JS
        return  $("#submitButton").outerWidth();
JS;
        $submitBtnWidth = $mink->getSession()->evaluateScript($submitBtnWidthScript);

        $this->assertEquals(159.672, $submitBtnWidth);
    }


    /**
     * This function will test that the checkboxes are correctly left aligned with the labels
     *
     * @author  Ankita Rastogi and MacKenzie Wilson
     */
    public function testDesktopCheckboxAlignment()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $mink->getSession()->wait(60000, '(0 === jQuery.active)');
        $mink->getSession()->resizeWindow(1000, 667); //width and height of mobile

        $checkboxLeftAlignmentScript = <<<JS
        return  $("input:last-child").offset().left;
JS;

        $labelLeftAlignment = <<<JS
        return  $("input:first-child").offset().left;
JS;

        $checkbox2LeftAlignmentScript = <<<JS
        return  $("#member_membershipAgreement").offset().left;
JS;
        $checkboxLeft = $mink->getSession()->evaluateScript($checkboxLeftAlignmentScript);
        $labelLeft = $mink->getSession()->evaluateScript($labelLeftAlignment);
        $checkbox2Left = $mink->getSession()->evaluateScript($checkbox2LeftAlignmentScript);

        $this->assertEquals($checkboxLeft, $labelLeft);
        $this->assertEquals($checkbox2Left, $labelLeft);
    }



    /**
     * This function will test that the checkboxes are correctly left aligned with the inputs
     *
     * @author  Ankita Rastogi and MacKenzie Wilson
     */
    public function testMobileCheckboxAlignment()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $mink->getSession()->wait(60000, '(0 === jQuery.active)');
        $mink->getSession()->resizeWindow(575, 667); //width and height of mobile

        $checkboxLeftAlignmentScript = <<<JS
        return  $("input:last-child").offset().left;
JS;

        $labelLeftAlignment = <<<JS
        return  $("label:last-child").offset().left;
JS;

        $checkbox2LeftAlignmentScript = <<<JS
        return  $("#member_membershipAgreement").offset().left;
JS;
        $checkboxLeft = $mink->getSession()->evaluateScript($checkboxLeftAlignmentScript);
        $labelLeft = $mink->getSession()->evaluateScript($labelLeftAlignment);
        $checkbox2Left = $mink->getSession()->evaluateScript($checkbox2LeftAlignmentScript);

        $this->assertEquals($checkboxLeft, $labelLeft);
        $this->assertEquals($checkbox2Left, $labelLeft);

    }

    /**
     * This function test that the Member Registration title is center aligned on the page for desktop platforms.
     *
     * @author MacKenzie Wilson & Ankita Rastogi
     */
    public function testDesktopMemberRegistrationTitleCentered()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $mink->getSession()->wait(60000, '(0 === jQuery.active)');
        $mink->getSession()->resizeWindow(1000, 768); //width and height of desktop

        $leftOffsetScript = <<<JS
        return  $("h1").offset().left;
JS;

        $rightOffsetScript = <<<JS
        return  $(window).width() - ($("h1").offset().left + $("h1").outerWidth());
JS;

        $leftOffset = $mink->getSession()->evaluateScript($leftOffsetScript);
        $rightOffset = $mink->getSession()->evaluateScript($rightOffsetScript);

        $this->assertEquals($leftOffset, $rightOffset);

    }

    /**
     * This function test that on mobile devices the Member Registration title is center aligned.
     *
     * @author MacKenzie Wilson & Ankita Rastogi
     */
    public function testMobileMemberRegistrationTitleCentered()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $mink->getSession()->wait(60000, '(0 === jQuery.active)');
        $mink->getSession()->resizeWindow(575, 667); //width and height of mobile phone

        $leftOffsetScript = <<<JS
        return  $("h1").offset().left;
JS;

        $rightOffsetScript = <<<JS
        return  $(window).width() - ($("h1").offset().left + $("h1").outerWidth());
JS;

        $leftOffset = $mink->getSession()->evaluateScript($leftOffsetScript);
        $rightOffset = $mink->getSession()->evaluateScript($rightOffsetScript);

        $this->assertEquals($leftOffset, $rightOffset);
    }

    /**
     * This function test that on desktop devices the Billing Information Title is aligned with the Labels of the form.
     *
     * @author MacKenzie Wilson & Ankita Rastogi
     */
    public function testDesktopBillingInformationTitleAlignment()
    {

        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $mink->getSession()->wait(60000, '(0 === jQuery.active)');
        $mink->getSession()->resizeWindow(1000, 768); //width and height of desktop

        $leftOffsetScript = <<<JS
        return  $("h2:first-child").offset().left;
JS;

        $rightOffsetScript = <<<JS
        return   $(window).width() - ($("h2:first-child").offset().left + $("h2:first-child").outerWidth());
JS;

        $leftLabelOffset = $mink->getSession()->evaluateScript($leftOffsetScript);
        $leftTitleOffset = $mink->getSession()->evaluateScript($rightOffsetScript);

        $this->assertEquals($leftLabelOffset, $leftTitleOffset);
    }

    /**
     * This function tests that on mobile devices the Billing Information Title is center aligned.
     *
     * @author MacKenzie Wilson & Ankita Rastogi
     */
    public function testMobileBillingInformationTitleAlignment()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $mink->getSession()->wait(60000, '(0 === jQuery.active)');
        $mink->getSession()->resizeWindow(575, 667); //width and height of mobile

        $leftOffsetScript = <<<JS
        return  $("h2:nth-child(3)").offset().left;
JS;

        $rightOffsetScript = <<<JS
        return   $(window).width() - ($("h2:nth-child(3)").offset().left + $("h2:nth-child(3)").outerWidth());
JS;

        $leftOffset = $mink->getSession()->evaluateScript($leftOffsetScript);
        $rightOffset = $mink->getSession()->evaluateScript($rightOffsetScript);

        $this->assertEquals($leftOffset, $rightOffset);
    }

    /**
     * This function test that desktop devices the error messages are aligned underneath the input boxes.
     *
     * @author MacKenzie Wilson & Ankita Rastogi
     */
    public function testDesktopErrorMessageAlignment()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $mink->getSession()->wait(60000, '(0 === jQuery.active)');
        $page = $mink->getSession()->getPage();
        $mink->getSession()->resizeWindow(1000, 768); //width and height of desktop

        $page->find("css", "#member_firstName")->setValue("");
        $page->find("css", "#member_lastName")->setValue("");
        $page->find("css", "#member_userName")->setValue("");
        $page->find("css", "#member_password_first")->setValue("");
        $page->find("css", "#member_password_second")->setValue("");
        $page->find("css", "#member_addressLineOne")->setValue("");
        $page->find("css", "#member_addressLineTwo")->setValue("Lorem123IpsumissimplydummytextoftheprintingandtypesettingindustryLoremIpsumhasbeentheindustrysstandaerer12");
        $page->find("css", "#member_city")->setValue("");
        $page->find("css", "#member_postalCode")->setValue("");
        $page->find("css", "#member_province")->setValue("SK");
        $page->find("css", "#member_company")->setValue("Lorem123IpsumissimplydummytextoftheprintingandtypesettingindustryLoremIpsumhasbeentheindustrysstanda123456");
        $page->find("css", "#member_phone")->setValue("12345786sd789");

        $page->find("css", "#member_company")->click();


        $errorScript = <<<JS
        $('.alert.alert-danger').map(function(index, element) {
            return { left : $(element).offset().left, top : $(element).offset().top  };
            }).get();
JS;
        $inputScript= <<<JS
        $('input').map(function(index, element) {
            return { left : $(element).offset().left, top : $(element).offset().top  };
            }).get();
JS;

        $error = $mink->getSession()->evaluateScript($errorScript);
        $input = $mink->getSession()->evaluateScript($inputScript);

        for( $i = 0; $i < count($error); $i++)
        {
            //Test to make sure that the error message is underneath the input
            $this->assertTrue($input[$i]['top'] < $error[$i]['top'] );

            //Test to make sure that the error messages left offset is the same as input fields left offset.
            $this->assertEquals($input[$i]['left'], $input[$i]['left']);
        }
    }

    /**
     * This function tests that on mobile devices the error messages are aligned underneath the input boxes.
     *
     * @author MacKenzie Wilson & Ankita Rastogi
     */
    public function testMobileErrorMessageAlignment()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $mink->getSession()->wait(60000, '(0 === jQuery.active)');
        $page = $mink->getSession()->getPage();
        $mink->getSession()->resizeWindow(575, 667); //width and height of mobile

        $page->find("css", "#member_firstName")->setValue("");
        $page->find("css", "#member_lastName")->setValue("");
        $page->find("css", "#member_userName")->setValue("");
        $page->find("css", "#member_password_first")->setValue("");
        $page->find("css", "#member_password_second")->setValue("");
        $page->find("css", "#member_addressLineOne")->setValue("");
        $page->find("css", "#member_addressLineTwo")->setValue("Lorem123IpsumissimplydummytextoftheprintingandtypesettingindustryLoremIpsumhasbeentheindustrysstandaerer12");
        $page->find("css", "#member_city")->setValue("");
        $page->find("css", "#member_postalCode")->setValue("");
        $page->find("css", "#member_province")->setValue("SK");
        $page->find("css", "#member_company")->setValue("Lorem123IpsumissimplydummytextoftheprintingandtypesettingindustryLoremIpsumhasbeentheindustrysstanda123456");
        $page->find("css", "#member_phone")->setValue("12345786sd789");

        $page->find("css", "#member_company")->click();


        $errorScript = <<<JS
        $('.alert.alert-danger').map(function(index, element) {
            return { left : $(element).offset().left, top : $(element).offset().top  };
            }).get();
JS;
        $inputScript= <<<JS
        $('input').map(function(index, element) {
            return { left : $(element).offset().left, top : $(element).offset().top  };
            }).get();
JS;

        $error = $mink->getSession()->evaluateScript($errorScript);
        $input = $mink->getSession()->evaluateScript($inputScript);


        for( $i = 0; $i < count($error); $i++)
        {
            //Test to make sure that the error message is underneath the input
            $this->assertTrue($input[$i]['top'] < $error[$i]['top'] );

            //Test to make sure that the error messages left offset is the same as input fields left offset.
            $this->assertEquals($input[$i]['left'], $input[$i]['left']);
        }
    }


    /**
     * This function test that the color of the submit button should be yellow as in SSP website
     *
     * @author Ankita Rastogi and MacKenzie Wilson
     */
    public function testBaseCssIsLoaded()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        //Wait for page to load
        $mink->getSession()->wait(60000, '(0 === jQuery.active)');
        $page = $mink->getSession()->getPage();

        $css = $page->find("css", "link:nth-child(5)")->getAttribute("href");

        $this->assertContains("styles.css", $css);

    }


    //// Moved from Member registration client validation file*******************************

    /**
     * This function will test to make sure that when a properly formatted email is
     * entered the user can submit the form.
     *
     * @author MacKenzie Wilson and Ankita Rastogi
     */
    public function testEmailValidFormat()
    {

        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_userName")->setValue("ankita@gmail.com");
        $page->find("css", "#member_password_first")->click();

        $errorText = $page->find("css", "#member_userName-error");
        $this->assertNull($errorText);

    }


    /**
     * This function will test to make sure that when an invalid formatted email is
     * entered the user cannot submit the form.
     *
     * @author MacKenzie Wilson and Ankita Rastogi
     */
    public function testEmailInvalidValidFormat()
    {

        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_userName")->setValue("ankita.com");
        $page->find("css", "#member_password_first")->click();

        $errorText = $page->find("css", "#member_userName-error")->getText();
        $this->assertContains("Email must be in standard email format",$errorText);

    }

    /**
     * This function tests to make sure that the user cannot submit the form when the
     * Email field is empty
     *
     * @author MacKenzie Wilson
     */
    public function testEmailEmptyIsInvalid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_userName")->setValue("");

        $email = $page->find("css", "#member_userName")->getAttribute("pattern");
        $page->find("css", "#member_password_first")->click();

        $errorText = $page->find("css", "#member_userName-error")->getText();
        $this->assertContains("Email is required",$errorText);

    }

    /**
     * This function will test that an error message is shown when the email is over the maximum length of 20 characters
     *
     * @author MacKenzie Wilson
     */
    public function testEmailOverMaxInvalid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_userName")->setValue("nostrudexercitatio@ullamco9laborisnisiut.aliquipexe");

        $email = $page->find("css", "#member_userName")->getAttribute("pattern");
        $page->find("css", "#member_password_first")->click();

        $errorText = $page->find("css", "#member_userName-error")->getText();
        $this->assertContains("Email can be no more than 50 characters",$errorText);
    }

    /**
     * This function will test that when 50 characters are entered into the email field no error message is shown
     *
     * @author MacKenzie Wilson
     */
    public function testEmailMaxValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_userName")->setValue("nostrudexercitatio@ullamcolaboris3nisiut.aliquipex");

        $page->find("css", "#member_password_first")->click();

        $errorText = $page->find("css", "#member_userName-error");
        $this->assertNull($errorText);
    }


    /**
     * This function will test that when the icon is mousedOver the password is visible in plaintext to the user.
     * Essentially will change the input tag from password to input
     *
     * @author MacKenzie Wilson & Ankita Rastogi
     */
    public function testPasswordTagChanged()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $passwordBefore = $page->find("css", "#member_password_first")->getAttribute("type");
        $this->assertEquals($passwordBefore, "password");

        $page->find("css", "#member_passwordFirstBtn")->mouseOver();

        $passwordAfter = $page->find("css", "#member_password_first")->getAttribute("type");
        $this->assertEquals($passwordAfter, "input");

    }
    /**
     * This function will test that if the password entered  match the criteria the user can submit the form
     *
     * @author MacKenzie Wilson and Ankita Rastogi
     */
    public function testPasswordValidFormat()
    {

        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_password_first")->setValue("Ankita1234");
        $page->find("css", "#member_password_second")->click();

        $errorText = $page->find("css", "#member_password_first-error");
        $this->assertNull($errorText);

    }

    /**
     * This function will test that if the password entered doesn't match the criteria the user cannot submit the form
     *
     * @author  Ankita Rastogi
     */
    public function testPasswordInvalidFormat()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_password_first")->setValue("a1234");
        $page->find("css", "#member_password_second")->click();

        $errorText = $page->find("css", "#member_password_first-error")->getText();
        $this->assertContains("Password must consist of one upper-case, one-lower-case, one number",$errorText);

    }

    /**
     * This function tests to make sure that the user cannot submit the form when the
     * Password field is empty
     *
     * @author MacKenzie Wilson
     */
    public function testPasswordEmptyIsInvalid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_password_first")->setValue("");
        $password = $page->find("css", "#member_password_first")->getAttribute("pattern");

        $page->find("css", "#member_password_second")->click();

        $errorText = $page->find("css", "#member_password_first-error")->getText();
        $this->assertContains("Password is required",$errorText);
    }



    /**
     * This function will test that a password cannot be over length of 4096
     * an error message will be shown.
     *
     * @author MacKenzie Wilson and Ankita Rastogi
     *
     */
    public function testPasswordOverMaxInvalid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_password_first")->setValue("Lorem123IpsumissimplydummytextoftheprintingandtypesettingindustryLoremIpsumhasbeentheindustrysstanda2");

        $page->find("css", "#member_password_second")->click();

        $errorText = $page->find("css", "#member_password_first-error")->getText();
        $this->assertContains("Password must consist of 6-100 alpha characters", $errorText);
    }
    public function testPasswordMaxValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();


        $page->find("css", "#member_password_first")->setValue("Lorem123IpsumissimplydummytextofthepdntingandtypesettingindustryLoremIpsumhasbeentheindustrysstanda");

        $page->find("css", "#member_password_second")->click();

        $errorText = $page->find("css", "#member_password_first-error");
        $this->assertNull( $errorText);
    }



    /**
     * This function will test if the password and repeat password entered by user matches,
     * user can submit the form
     * @author  Ankita Rastogi
     */
    public function testRepeatPasswordMatchPassword()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_password_first")->setValue("Ankita1234");
        $password = $page->find("css", "#member_password_first")->getValue();

        $page->find("css", "#member_password_second")->setValue("Ankita1234");
        $repeatPassword = $page->find("css", "#member_password_second")->getValue();

        $page->find("css", "#member_addressLineOne")->click();
        $this->assertEquals($repeatPassword, $password);

        $errorText = $page->find("css", "#member_password_first-error");
        $this->assertNull($errorText);

    }

    /**
     * This function will test if the password and repeat password entered by user does not match, user cannot submit
     * the form
     *
     * @author Ankita Ratsogi and MacKenzie Wilson
     */
    public function testRepeatPasswordNotMatchPassword()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();


        $page->find("css", "#member_password_first")->setValue("Ankita1234");
        $password = $page->find("css", "#member_password_first")->getValue();


        $page->find("css", "#member_password_second")->setValue("Ankita12");
        $repeatPassword = $page->find("css", "#member_password_second")->getValue();

        $page->find("css", "#member_addressLineOne")->click();
        $this->assertNotEquals($repeatPassword, $password);


        $errorText = $page->find("css", "#member_password_second-error")->getText();
        $this->assertContains("Passwords must match.",$errorText);

    }

    /**
     * This function tests to make sure that the user cannot submit the form when the
     * Repeat Password field is empty
     *
     * @author MacKenzie Wilson
     */
    public function testRepeatPasswordEmptyIsInvalid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');

        $mink->getSession()->visit('http://localhost:8000/registration');

        $session = $mink->getSession();
        $page = $session->getPage();


        $page->find("css", "#member_password_second")->setValue("");
        $page->find("css", "#member_addressLineOne")->click();

        $errorText = $page->find("css", "#member_password_second-error")->getText();
        $this->assertContains("Repeat Password is required", $errorText);

    }


    /**
     * This function will test that a password cannot be over length of 4096
     * an error message will be shown.
     *
     * @author MacKenzie Wilson and Ankita Rastogi
     *
     */
    public function testRepeatPasswordOverMaxInvalid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();


        $page->find("css", "#member_password_second")->setValue("Lorem123IpsumiiissimplydummytextoftheprintingandtypesettingindustryLoremIpsumhasbeentheindustrysstanda");


        $page->find("css", "#member_password_first")->click();

        $errorText = $page->find("css", "#member_password_second-error")->getText();
        $this->assertContains("Repeat Password must consist of 6-100 alpha characters", $errorText);
    }

    /**
     * This function tests that when 100 characters are entered into the Repeat Password field
     * no error message is shown
     *
     * @author MacKenzie Wilson & Ankita Rastogi
     */
    public function testRepeatPasswordMaxValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_password_first")->setValue("Lorem123IpsumiiissimplydummytextoftheprintingandtypesettingindustryLoremIpsumhasbeentheindustrystand");
        $page->find("css", "#member_password_second")->setValue("Lorem123IpsumiiissimplydummytextoftheprintingandtypesettingindustryLoremIpsumhasbeentheindustrystand");

        $page->find("css", "#member_password_first")->click();

        $errorText = $page->find("css", "#member_password_second-error");
        $this->assertNull( $errorText);
    }

    /**
     * This function will test when valid formatted postal code is entered the user can submit the form.
     *
     * @author Ankita Rastogi
     */
    public function testPostalCodeValidFormat()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_postalCode")->setValue("S6O 8k8");

        $page->find("css", "#member_password_second")->click();

        $errorText = $page->find("css", "#member_postalCode-error");
        $this->assertNull( $errorText);

    }



    /**
     * This function will test when an invalid formatted postal code is entered the user cannot submit the form.
     *
     * @author Ankita Rastogi
     */
    public function testPostalCodeInValidFormat()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_postalCode")->setValue("SAA 888");

        $page->find("css", "#member_password_second")->click();

        $errorText = $page->find("css", "#member_postalCode-error")->getText();


        $this->assertContains("Postal Code should be in the format of A2A3B3 or A2A 3B3", $errorText);

    }
    /**
     * This function tests to make sure that the user cannot submit the form when the
     * Postal Code field is empty
     *
     * @author MacKenzie Wilson
     */
    public function testPostalCodeEmptyIsInvalid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_postalCode")->setValue("");

        $page->find("css", "#member_password_second")->click();

        $errorText = $page->find("css", "#member_postalCode-error")->getText();
        $this->assertContains("Postal code is required", $errorText);
    }


    /**
     * This function will test that the user can submit the form when criminal record checkbox is not checked
     * @author Ankita Rastogi and MacKenzie Wilson
     */
    public function testCriminalRecordCheckWhenEmptyIsValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_criminalRecord")->check();
        $errorText = $page->find("css", "#member_criminalRecord-error");
        $this->assertNull($errorText);

    }


    /**
     * This function will test that the user can submit the form when criminal record checkbox is not checked
     * @author Ankita Rastogi and MacKenzie Wilson
     */
    public function testCriminalRecordCheckWhenCheckedIsValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_criminalRecord")->check();
        $page->find("css", "#member_criminalRecord")->uncheck();
        $errorText = $page->find("css", "#member_criminalRecord-error");
        $this->assertNull($errorText);

    }


    //********************************Other Fields Test **********************************//


    /**
     *  This function will test that the user can submit the form when the first name is filled
     * is selected
     *
     * @author Ankita Rastogi and MacKenzie Wilson
     */
    public function testFirstNameFilledIsValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_firstName")->setValue("Ankita");
        $page->find("css", "#member_password_second")->click();

        $errorText = $page->find("css", "#member_firstName-error");

        $this->assertNull( $errorText);

    }

    /**
     *  This function will test that the user cannot submit the form when the first name is not filled
     * is selected
     *
     * @author Ankita Rastogi and MacKenzie Wilson
     */
    public function testFirstNameEmptyIsInValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');

        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_firstName")->setValue("");
        $page->find("css", "#member_password_second")->click();

        $errorText = $page->find("css", "#member_firstName-error" )->getText();

        $this->assertEquals( "First Name is required" ,$errorText);

    }

    /**
     * This function tests that when entered a first name longer than 20 characters the appropriate error message shows when clicked on some place else.
     *
     * @author MacKenzie Wilson
     */
    public function testFirstNameOverMaxLengthInvalid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');

        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_firstName")->setValue("Wolfeschlegelsteinhas");
        $page->find("css", "#member_password_second")->click();

        $errorText = $page->find("css", "#member_firstName-error" )->getText();

        $this->assertEquals( "First name must be between 1-20 characters without spaces" ,$errorText);
    }
    /**
     * This function tests that when entered a first name at 20 characters, no error message is shown
     *
     * @author MacKenzie Wilson
     */
    public function testFirstNameMaxLengthValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');

        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_firstName")->setValue("Wolfeschlegelsteinha");
        $page->find("css", "#member_password_second")->click();

        $errorText = $page->find("css", "#member_firstName-error" );

        $this->assertNull($errorText);
    }



    /**
     *  This function will test that the user can submit the form when the last name is filled
     * is selected
     *
     * @author Ankita Rastogi and MacKenzie Wilson
     */
    public function testLastNameFilledIsValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_lastName")->setValue("Rastogi");
        $page->find("css", "#member_password_second")->click();

        $errorText = $page->find("css", "#member_lastName-error" );
        $this->assertNull($errorText);
    }

    /**
     *  This function will test that the user cannot submit the form when the first name is not filled
     * is selected
     *
     * @author Ankita Rastogi and MacKenzie Wilson
     */
    public function testLastNameEmptyIsInValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_lastName")->setValue("");
        $page->find("css", "#member_password_second")->click();

        $errorText = $page->find("css", "#member_lastName-error" )->getText();
        $this->assertEquals("Last Name is required", $errorText);

    }

    /**
     * This function tests that when a last name entered is over 20 characters long, an appropriate error message is shown
     *
     * @author MacKenzie Wilson
     */
    public function testLastNameOverMaxLenInValid()
    {

        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_lastName")->setValue("Wolfeschlegelsteinhas");
        $page->find("css", "#member_password_second")->click();

        $errorText = $page->find("css", "#member_lastName-error" )->getText();
        $this->assertEquals("Last name must be between 1-20 characters without spaces", $errorText);
    }

    /**
     * This function tests that when a last name entered is 20 characters long, no error message is shown
     *
     * @author MacKenzie Wilson
     */
    public function testLastNameMaxLenValid()
    {

        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_lastName")->setValue("Wolfeschlegelsteinha");
        $page->find("css", "#member_password_second")->click();

        $errorText = $page->find("css", "#member_lastName-error" );
        $this->assertNull($errorText);
    }
    /**
     * This function tests to make sure the user can submit the form when the AddressLineOne
     * field is filled in.
     *
     * @author MacKenzie Wilson
     */
    public function testAddressLineOneFilledIsValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_addressLineOne")->setValue("AddressLineOne");
        $page->find("css", "#member_addressLineTwo")->click();

        //Assert there is no error on the page
        $errorText = $page->find("css", "#member_addressLineOne-error" );
        $this->assertNull($errorText);
    }

    /**
     * This function test to make sure that the user cannot submit the form when the AddressLineOne
     * field is empty
     *
     * @author MacKenzie Wilson
     */
    public function testAddressLineOneEmptyIsInValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_addressLineOne")->setValue("");
        $page->find("css", "#member_addressLineTwo")->click();

        $errorText = $page->find("css", "#member_addressLineOne-error" )->getText();
        $this->assertContains("Address Line One is required",$errorText);

    }

    /**
     * This function will test that when over 100 characters(101) are entered into the Address Line One
     * field an error message will be displayed.
     *
     * @author MacKenzie Wilson & Ankita Rastogi
     */
    public function testAddressLineOneOverMaxInValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();


        $page->find("css", "#member_addressLineOne")->setValue("Lorem123IpsumissimplydummytextoftheprintingandtypesettingindustryLoremIpsumhasbeentheindustrysstandag");
        $page->find("css", "#member_addressLineTwo")->click();

        $errorText = $page->find("css", "#member_addressLineOne-error" )->getText();
        $this->assertContains("Address Line One must be under 100 characters",$errorText);


    }

    /**
     * This function will test that when 100 characters are entered into the Address Line One field
     * no error message will be displayed.
     *
     * @author MacKenzie Wilson & Ankita Rastogi
     */
    public function testAddressLineOneMaxValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();


        $page->find("css", "#member_addressLineOne")->setValue("Lorem123IpsumissimplydummytextoftheprintingandtypesettingindustryLoremIpsumhasbeentheindustrysstanda");

        $page->find("css", "#member_addressLineTwo")->click();

        $errorText = $page->find("css", "#member_addressLineOne-error");
        $this->assertNull($errorText);
    }

    /**
     * This function tests to make sure that the user can submit the form when the AddressLineTwo
     * field is empty.
     *
     * @author MacKenzie Wilson
     */
    public function testAddressLineTwoEmptyIsValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_addressLineTwo")->setValue("");
        $page->find("css", "#member_city")->click();

        $errorText = $page->find("css", "#member_addressLineTwo-error" );
        $this->assertNull($errorText);

    }

    /**
     * This function tests to make sure that the user can submit the form when the AddressLineTwo
     * field is filled in.
     *
     * @author MacKenzie Wilson
     */
    public function testAddressLineTwoFilledIsValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_addressLineTwo")->setValue("AddressLineTwo");
        $page->find("css", "#member_city")->click();

        $errorText = $page->find("css", "#member_addressLineTwo-error" );
        $this->assertNull($errorText);
    }

    /**
     * This function will test that when over 100 characters are entered into the Address Line One
     * field an error message will be displayed.
     *
     * @author MacKenzie Wilson & Ankita Rastogi
     */
    public function testAddressLineTwoOverMaxInValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();


        $page->find("css", "#member_addressLineTwo")->setValue("Lorem123IpsumissimplydummytextoftheprintingandtypesettingindustryLoremIpsumhasbeentheindustrysstandag");
        $page->find("css", "#member_city")->click();

        $errorText = $page->find("css", "#member_addressLineTwo-error" )->getText();
        $this->assertContains("AddressLineTwo must be under 100 characters",$errorText);
    }

    /**
     * This function will test that when 100 characters are entered into the Address Line One field
     * no error message will be displayed.
     *
     * @author MacKenzie Wilson & Ankita Rastogi
     */
    public function testAddressLineTwoMaxValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();


        $page->find("css", "#member_addressLineTwo")->setValue("Lorem123IpsumissimplydummytextoftheprintingandtypesettingindustryLoremIpsumhasbeentheindustrysstanda");

        $page->find("css", "#member_city")->click();

        $errorText = $page->find("css", "#member_addressLineTwo-error");
        $this->assertNull($errorText);
    }

    /**
     * This function tests to make sure that the user can submit the form when the City field is filled in
     *
     * @author MacKenzie Wilson
     */
    public function testCityFilledIsValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_city")->setValue("City Here");
        $page->find("css", "#member_addressLineTwo")->click();

        $errorText = $page->find("css", "#member_city-error" );
        $this->assertNull($errorText);

    }
    /**
     * This function tests to make sure that the user cannot submit the form when the City field is empty.
     *
     * @author MacKenzie Wilson
     */
    public function testCityEmptyIsInvalid()
    {

        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_city")->setValue("");
        $page->find("css", "#member_addressLineTwo")->click();

        $errorText = $page->find("css", "#member_city-error" )->getText();
        $this->assertContains("City is required", $errorText);
    }
    /**
     * This function will test that when over 100 characters are entered into the City
     * field an error message will be displayed.
     *
     * @author MacKenzie Wilson & Ankita Rastogi
     */
    public function testCityOverMaxInValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();


        $page->find("css", "#member_city")->setValue("Lorem123IpsumissimplydummytextoftheprintingandtypesettingindustryLoremIpsumhasbeentheindustrysstandag");
        $page->find("css", "#member_addressLineTwo")->click();

        $errorText = $page->find("css", "#member_city-error" )->getText();
        $this->assertContains("City must be under 100 characters",$errorText);
    }

    /**
     * This function will test that when 100 characters are entered into the City field
     * no error message will be displayed.
     *
     * @author MacKenzie Wilson & Ankita Rastogi
     */
    public function testCityMaxValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();


        $page->find("css", "#member_city")->setValue("Lorem123IpsumissimplydummytextoftheprintingandtypesettingindustryLoremIpsumhasbeentheindustrysstanda");

        $page->find("css", "#member_addressLineOne")->click();

        $errorText = $page->find("css", "#member_city-error");
        $this->assertNull($errorText);
    }


    /**
     * This function tests to make sure that the user can submit the form when an option is
     * selected in from the province field.
     *
     * @author MacKenzie Wilson
     */
    public function testProvinceFilledIsValid()
    {

        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_province")->setValue("SK");
        $page->find("css", "#member_addressLineTwo")->click();

        $provinceField = $page->find("css", "#member_province")->getValue();
        $this->assertEquals("SK", $provinceField);

        $errorText = $page->find("css", "#member_province-error" );
        $this->assertNull($errorText);
    }

    /**
     * This function tests to make sure that the user can submit the form when an Company field is empty
     *
     * @author MacKenzie Wilson
     */
    public function testCompanyEmptyIsValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_company")->setValue("");
        $page->find("css", "#member_password_second")->click();


        $errorText = $page->find("css", "#member_company-error" );
        $this->assertNull($errorText);
    }

    /**
     * This function tests to make sure that the user can submit the form when an Company field is filled
     *
     * @author MacKenzie Wilson
     */
    public function testCompanyFilledIsValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_company")->setValue("Company information");
        $page->find("css", "#member_password_second")->click();

        $errorText = $page->find("css", "#member_company-error" );
        $this->assertNull($errorText);
    }

    /**
     * This function will test that when over 100 characters are entered into the Company
     * field an error message will be displayed.
     *
     * @author MacKenzie Wilson & Ankita Rastogi
     */
    public function testCompanyOverMaxInValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();


        $page->find("css", "#member_company")->setValue("Lorem123IpsumissimplydummytextoftheprintingandtypesettingindustryLoremIpsumhasbeentheindustrysstandag");
        $page->find("css", "#member_addressLineTwo")->click();

        $errorText = $page->find("css", "#member_company-error" )->getText();
        $this->assertContains("Company must be under 100 characters",$errorText);
    }

    /**
     * This function will test that when 100 characters are entered into the Company field
     * no error message will be displayed.
     *
     * @author MacKenzie Wilson & Ankita Rastogi
     */
    public function testCompanyMaxValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();


        $page->find("css", "#member_company")->setValue("Lorem123IpsumissimplydummytextoftheprintingandtypesettingindustryLoremIpsumhasbeentheindustrysstanda");

        $page->find("css", "#member_addressLineOne")->click();

        $errorText = $page->find("css", "#member_company-error");
        $this->assertNull($errorText);
    }



    /**
     * This function tests to make sure that the user can submit the form when an Phone field is empty
     *
     * @author MacKenzie Wilson
     */
    public function testPhoneEmptyIsValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_phone")->setValue("");
        $page->find("css", "#member_password_second")->click();

        $errorText = $page->find("css", "#member_phone-error" );
        $this->assertNull($errorText);
    }

    /**
     * This function tests to make sure that the user can submit the form when an Phone field is empty
     *
     * @author MacKenzie Wilson
     */
    public function testPhoneFilledIsValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_phone")->setValue("(303) 783-8389");
        $page->find("css", "#member_password_second")->click();

        $errorText = $page->find("css", "#member_phone-error" );
        $this->assertNull($errorText);
    }

    /**
     * This function tests to make sure that the user can submit the form when an Phone field is empty
     *
     * @author MacKenzie Wilson
     */
    public function testPhoneFormatInValid()
    {
        $mink = new Mink(array(
            'registration' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // Set the default session name
        $mink->setDefaultSessionName('registration');
        $mink->getSession()->visit('http://localhost:8000/registration');
        $session = $mink->getSession();
        $page = $session->getPage();

        $page->find("css", "#member_phone")->setValue("88899988");
        $page->find("css", "#member_password_second")->click();

        $errorText = $page->find("css", "#member_phone-error" )->getText();
        $this->assertContains("Please enter a 10-digit phone number",$errorText);
    }



}
