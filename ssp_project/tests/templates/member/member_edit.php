<?php
/**
 * User: CST221
 * Date: 1/24/2019
 * SERVER LOGIC BLOCK
 */

namespace App\Tests\Controller;

use app\Entity\Member;

use DMore\ChromeDriver\ChromeDriver;
use Behat\Mink\Mink;
use Behat\Mink\Session;
//use const http\Client\Curl\PROXY_SOCKS4A;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use function PHPSTORM_META\elementType;
use WebDriver\Exception;

class memberedit extends WebTestCase
{
    private $fixtures;

    // https://jqueryvalidation.org/rules/
    /** start chrome --disable-gpu --headless --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222
     * Set up will load fixtures and by default log you in as a board member
     * @throws
     */
    public function setUp()
    {
        $this->fixtures = $this->loadFixtures(array(
           'App\DataFixtures\AddOnlinePollsFixtures',
            'App\DataFixtures\MemberLoginTestFixture',
            'App\DataFixtures\AddMembersFixtures'
        ));

    }

    /** 1
     * This will test that a Board Member can see the "View Profile" button and is redirected to their profile page
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testThatViewProfileButtonRedirectsProperlyForBM()
    {
        //LOGIN AS A BOARD MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("bmember@member.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        //click on the link to go to the Profile page
        $page->find("css", "#updateProfile")->click();

        // Check that the page has properly redirected to the update profile page
        $url = $driver->getCurrentUrl();

        $this->assertEquals($url, 'http://localhost:8000/member/edit/2');
    }

    /** 2
     * This will test that a General Manager can see the "View Profile" button and is redirected to their profile page
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testThatViewProfileButtonRedirectsProperlyForGM()
    {
        //LOGIN AS A GENERAL MANAGER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("gmember@member.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        //click on the link to go to the Profile page
        $page->find("css", "#updateProfile")->click();

        // Check that the page has properly redirected to the update profile page
        $url = $driver->getCurrentUrl();

        $this->assertEquals($url, 'http://localhost:8000/member/edit/3');
    }

    /** 3
     * This will test that a Member can see the "View Profile" button and is redirected to their profile page
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testThatViewProfileButtonRedirectsProperlyForMember()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("member@member.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        //click on the link to go to the Profile page
        $page->find("css", "#updateProfile")->click();

        // Check that the page has properly redirected to the update profile page
        $url = $driver->getCurrentUrl();

        $this->assertEquals($url, 'http://localhost:8000/member/edit/1');
    }

    /** 4 Can't really do this test
     * This will test that public (not logged in) users cannot see the "View Profile" button and cannot
     * access the profile page
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testThatPublicUserCannotSeeViewProfileButtonOrAccessProfile()
    {

        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();

        // Check to see if the "Update Profile" button is visible
        $this->assertNotContains("Update Profile", $page->getContent());

        //visit Profile Page MANUALLY
        $mink->getSession()->visit('http://localhost:8000/member/edit/1');

        $url = $driver->getCurrentUrl();
        $this->assertEquals($url, 'http://localhost:8000/login');
    }

    /** 5
     * This will test that a member can update their first name
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testValidFirstNameUpdated()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');

        $page = $mink->getSession()->getPage();
        $driver = $mink->getSession()->getDriver();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter a valid first name in the first name field
        $page->find("css", "#member_update_firstName")->setValue("Sam");

        // Click away from the first name field

        $page->find("css", "#updateButton")->click();

        // Check error message not on screen
        $this->assertNotContains("First name must be between 1-20 characters without spaces", $page->getContent());

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost:8000/show?isUpdated=1", $driver->getCurrentUrl());

        // Assert text
        $successMessage = $page->find("css", ".modal-body")->getText();
        $this->assertEquals("Your personal information has been changed!", $successMessage);

    }

    /** 6
     * This will test that a member cannot update their first name with an invalid value
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testTooLongFirstNameNotUpdated()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter a first name that is too long in the first name field
        $page->find("css", "#member_update_firstName")->setValue(str_repeat('a', 21));

        // Click away from the first name field
        $page->find("css", "#member_update_lastName")->click();

        // Check to see if any errors are visible
        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("First name must be between 1-20 characters without spaces", $errorText);
    }

    /** 7
     * This will test that a member cannot update their first name to a blank field
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testBlankFirstNameDisplaysError()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter a first name that is too long in the first name field
        $page->find("css", "#member_update_firstName")->setValue("");

        // Click away from the first name field
        $page->find("css", "#member_update_lastName")->click();

        // Check to see if any errors are visible
        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("First name is required", $errorText);
    }

    /** 8
     * This will test that a member can update their last name
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testValidLastNameUpdated()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');

        $page = $mink->getSession()->getPage();
        $driver = $mink->getSession()->getDriver();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter a valid last name in the first name field
        $page->find("css", "#member_update_lastName")->setValue("Smith");

        // Click away from the first name field
        $page->find("css", "#updateButton")->click();

        // Check error message not on screen
        $this->assertNotContains("Last name must be between 1-20 characters without spaces", $page->getContent());

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost:8000/show?isUpdated=1", $driver->getCurrentUrl());

        // Assert text
        $successMessage = $page->find("css", ".modal-body")->getText();
        $this->assertEquals("Your personal information has been changed!", $successMessage);
    }

    /** 9
     * This will test that a member cannot update their last name with an invalid value
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testTooLongLastNameNotUpdated()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter a last name that is too long in the first name field
        $page->find("css", "#member_update_lastName")->setValue(str_repeat('a', 21));

        // Click away from the last name field
        $page->find("css", "#member_update_firstName")->click();

        // Check to see if any errors are visible
        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("Last name must be between 1-20 characters without spaces", $errorText);
    }

    /** 10
     * This will test that a member cannot update their last name to a blank field
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testBlankLastNameDisplaysError()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter a last name that is too long in the first name field
        $page->find("css", "#member_update_lastName")->setValue("");

        // Click away from the last name field
        $page->find("css", "#member_update_firstName")->click();

        // Check to see if any errors are visible
        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("Last name is required", $errorText);
    }

    /** 11
     * This will test that a member can update their username (email)
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testValidUsernameUpdated()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');

        $page = $mink->getSession()->getPage();
        $driver = $mink->getSession()->getDriver();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter a valid username in the username field
        $page->find("css", "#member_update_userName")->setValue("samprj4unique@gmail.com");

        // Click away from the first name field
        $page->find("css", "#updateButton")->click();

        // Check error message not on screen
        $this->assertNotContains("Email is required", $page->getContent());
        $this->assertNotContains("Email exceeds 50 characters", $page->getContent());

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost:8000/show?isUpdated=1", $driver->getCurrentUrl());

        // Assert text
        $successMessage = $page->find("css", ".modal-body")->getText();
        $this->assertEquals("Your personal information has been changed!", $successMessage);
    }

    /** 12
     * This will test that a member cannot update their username (email) to a value that is too long
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testTooLongUsernameNotUpdated()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter an email that is too long in the first name field
        $page->find("css", "#member_update_userName")->setValue(str_repeat('a', 5000)."@gmail.com");

        // Click away from the email field
        $page->find("css", "#member_update_firstName")->click();

        // Check to see if any errors are visible
        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("Email exceeds 50 characters", $errorText);
    }

    /** 13
     * This will test that a member cannot update their username (email) with an invalid value
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testInvalidUsernameNotUpdated()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter an email that is too long in the first name field
        $page->find("css", "#member_update_userName")->setValue("samprjresetatgmail.com");

        // Click away from the email field
        $page->find("css", "#member_update_firstName")->click();

        // Check to see if any errors are visible
        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("Email must be in standard email format", $errorText);
    }

    /** 14
     * This will test that a member cannot update their username to a blank field
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testBlankUserNameDisplaysError()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter a last name that is too long in the username field
        $page->find("css", "#member_update_userName")->setValue("");

        // Click away from the username field
        $page->find("css", "#member_update_lastName")->click();

        // Check to see if any errors are visible
        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("Email is required", $errorText);
    }

    /** 15
     * This will test that a member can update their city
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testValidCityUpdated()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');

        $page = $mink->getSession()->getPage();
        $driver = $mink->getSession()->getDriver();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter a valid username in the city field
        $page->find("css", "#member_update_city")->setValue("Regina");

        // Click away from the first name field
        $page->find("css", "#updateButton")->click();

        // Check error message not on screen
        $this->assertNotContains("City is required", $page->getContent());
        $this->assertNotContains("City cannot exceed 100 characters", $page->getContent());

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost:8000/show?isUpdated=1", $driver->getCurrentUrl());

        // Assert text
        $successMessage = $page->find("css", ".modal-body")->getText();
        $this->assertEquals("Your personal information has been changed!", $successMessage);
    }

    /** 16
     * This will test that a member cannot update their city with an invalid value
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testTooLongCityNotUpdated()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter an email that is too long in the city field
        $page->find("css", "#member_update_city")->setValue(str_repeat('a', 101));

        // Click away from the city field
        $page->find("css", "#member_update_postalCode")->click();

        // Check to see if any errors are visible
        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("City cannot exceed 100 characters", $errorText);

    }

    /** 17
     * This will test that a member cannot update their city to a blank field
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testBlankCityDisplaysError()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter a last name that is too long in the city field
        $page->find("css", "#member_update_city")->setValue("");

        // Click away from the city field
        $page->find("css", "#member_update_postalCode")->click();

        // Check to see if any errors are visible
        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("City is required", $errorText);
    }


    /** 18
     * This will test that a member can update their postal code
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testValidPostalCodeUpdated()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');

        $page = $mink->getSession()->getPage();
        $driver = $mink->getSession()->getDriver();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter a valid username in the postal code field
        $page->find("css", "#member_update_postalCode")->setValue("S7J3H8");

        // Click away from the postal code field
        $page->find("css", "#member_update_lastName")->click();

        // Click away from the first name field
        $page->find("css", "#updateButton")->click();

        // Check error message not on screen
        $this->assertNotContains("Postal code is required", $page->getContent());
        $this->assertNotContains("Postal Code should be in the format of A2A3B3 or A2A 3B3", $page->getContent());

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost:8000/show?isUpdated=1", $driver->getCurrentUrl());

        // Assert text
        $successMessage = $page->find("css", ".modal-body")->getText();
        $this->assertEquals("Your personal information has been changed!", $successMessage);
    }

    /** 19
     * This will test that a member cannot update their postal code with an invalid value
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testInvalidPostalCodeNotUpdated()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter a last name that is too long in the postal code field
        $page->find("css", "#member_update_postalCode")->setValue("S7H5");

        // Click away from the postal code field
        $page->find("css", "#member_update_company")->click();

        // Check to see if any errors are visible
        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("Postal Code should be in the format of A2A3B3 or A2A 3B3", $errorText);
    }

    /** 20
     * This will test that a member can update their province
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testValidProvinceUpdated()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');

        $page = $mink->getSession()->getPage();
        $driver = $mink->getSession()->getDriver();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter a valid username in the province field
        $page->find("css", "#member_update_province")->setValue("MB");

        // Click away from the first name field
        $page->find("css", "#updateButton")->click();

        // Check error message not on screen
        $this->assertNotContains("Please select a province", $page->getContent());

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost:8000/show?isUpdated=1", $driver->getCurrentUrl());

        // Assert text
        $successMessage = $page->find("css", ".modal-body")->getText();
        $this->assertEquals("Your personal information has been changed!", $successMessage);
    }

    /** 21
     * This will test that a member cannot update their province with an invalid value
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testInvalidProvinceNotUpdated()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter a last name that is too long in the province field
        $page->find("css", "#member_update_province")->setValue("NA");

        // Click away from the province field
        $page->find("css", "#member_update_lastName")->click();

        // Check to see if any errors are visible
        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("Please select a province", $errorText);
    }

    /** 22
     * This will test that a member can update their company
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testValidCompanyUpdated()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');

        $page = $mink->getSession()->getPage();
        $driver = $mink->getSession()->getDriver();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter a valid username in the company field
        $page->find("css", "#member_update_company")->setValue("Shady Inc.");

        // Click away from the first name field
        $page->find("css", "#updateButton")->click();

        // Check error message not on screen
        $this->assertNotContains("Company cannot exceed 100 characters", $page->getContent());

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost:8000/show?isUpdated=1", $driver->getCurrentUrl());

        // Assert text
        $successMessage = $page->find("css", ".modal-body")->getText();
        $this->assertEquals("Your personal information has been changed!", $successMessage);
    }

    /** 23
     * This will test that a member cannot update their company with a value that is too long
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testTooLongCompanyNotUpdated()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter an email that is too long in the company field
        $page->find("css", "#member_update_company")->setValue(str_repeat('a', 101));

        // Click away from the company field
        $page->find("css", "#member_update_firstName")->click();

        // Check to see if any errors are visible
        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("Company cannot exceed 100 characters", $errorText);
    }

    /** 24
     * This will test that a member cannot update their username to a blank field
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testBlankCompanyDoesNotDisplaysError()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter a last name that is too long in the company field
        $page->find("css", "#member_update_company")->setValue("");

        // Click away from the company field
        $page->find("css", "#member_update_lastName")->click();

        // Check to see if any errors are visible
        $this->assertNotContains("Company cannot exceed 100 characters", $page->getContent());
    }


    /** 25
     * This will test that a member can update their phone number
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testValidPhoneNumberUpdated()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');

        $page = $mink->getSession()->getPage();
        $driver = $mink->getSession()->getDriver();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter a valid username in the phone number field
        $page->find("css", "#member_update_phone")->setValue("3068675309");

        // Click away from the first name field
        $page->find("css", "#updateButton")->click();

        // Check error message not on screen
        $this->assertNotContains("Invalid Phone number", $page->getContent());

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost:8000/show?isUpdated=1", $driver->getCurrentUrl());

        // Assert text
        $successMessage = $page->find("css", ".modal-body")->getText();
        $this->assertEquals("Your personal information has been changed!", $successMessage);
    }

    /** 26 
     * This will test that a member cannot update their phone number with an invalid value
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testInvalidPhoneNumberNotUpdated()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter a phone number that is invalid
        $page->find("css", "#member_update_phone")->setValue("abc-123");

        // Click away from the phone number field
        $page->find("css", "#member_update_addressLineOne")->click();

        // Check to see if any errors are visible
        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("Please enter a 10-digit phone number", $errorText);
    }

    /** 27
     * This will test that a member can update their address line one
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testValidAddressOneUpdated()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');

        $page = $mink->getSession()->getPage();
        $driver = $mink->getSession()->getDriver();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter a valid address line one in the company field
        $page->find("css", "#member_update_addressLineOne")->setValue("112 There street");

        // Click away from the first name field
        $page->find("css", "#updateButton")->click();


        // Check error message not on screen
        $this->assertNotContains("Address is required", $page->getContent());
        $this->assertNotContains("Address can not exceed 100 characters", $page->getContent());

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost:8000/show?isUpdated=1", $driver->getCurrentUrl());

        // Assert text
        $successMessage = $page->find("css", ".modal-body")->getText();
        $this->assertEquals("Your personal information has been changed!", $successMessage);
    }

    /** 28
     * This will test that a member cannot update their address line one with an invalid value
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testInvalidAddressOneNotUpdated()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter an address line one is too long
        $page->find("css", "#member_update_addressLineOne")->setValue(str_repeat('a', 101));

        // Click away from the address line one field
        $page->find("css", "#member_update_firstName")->click();

        // Check to see if any errors are visible
        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("Address can not exceed 100 characters", $errorText);
    }

    /** 29
     * This will test that a member cannot update their address line one with a blank entry
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testBlankAddressOneDisplaysError()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter an address line one is too long
        $page->find("css", "#member_update_addressLineOne")->setValue("");

        // Click away from the address line one field
        $page->find("css", "#member_update_firstName")->click();

        // Check to see if any errors are visible
        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("Address is required", $errorText);
    }

    /** 30
     * This will test that a member can update their address line two
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testValidAddressTwoUpdated()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');

        $page = $mink->getSession()->getPage();
        $driver = $mink->getSession()->getDriver();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter a valid address in the address field
        $page->find("css", "#member_update_addressLineTwo")->setValue("123 There Street");

        // Click away from the first name field
        $page->find("css", "#updateButton")->click();


        // Check error message not on screen
        $this->assertNotContains("Address is required", $page->getContent());
        $this->assertNotContains("Address can not exceed 100 characters", $page->getContent());

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost:8000/show?isUpdated=1", $driver->getCurrentUrl());

        // Assert text
        $successMessage = $page->find("css", ".modal-body")->getText();
        $this->assertEquals("Your personal information has been changed!", $successMessage);
    }

    /** 31
     * This will test that a member cannot update their address line two with an invalid value
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testInvalidAddressTwoNotUpdated()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter an address line two is too long
        $page->find("css", "#member_update_addressLineTwo")->setValue(str_repeat('a', 101));

        // Click away from the address line two field
        $page->find("css", "#member_update_firstName")->click();

        // Check to see if any errors are visible
        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("Address can not exceed 100 characters", $errorText);
    }

    /** 32 
     * This will test that a member can save a blank value into an optional field
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testBlankValueOptionalField()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        // Click the "Update Profile" button
        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        //enter a blank address in line two
        $page->find("css", "#member_update_addressLineTwo")->setValue("");

        // Click away from the first name field
        $page->find("css", "#updateButton")->click();


        // Check error message not on screen
        $this->assertNotContains("Address is required", $page->getContent());
        $this->assertNotContains("Address can not exceed 100 characters", $page->getContent());

        // Check that we've been redirected properly
        $this->assertEquals("http://localhost:8000/show?isUpdated=1", $driver->getCurrentUrl());

        // Assert text
        $successMessage = $page->find("css", ".modal-body")->getText();
        $this->assertEquals("Your personal information has been changed!", $successMessage);
    }


    /** 33
     * CoryN and KateZ
     * March 5, 2019
     * This function will test that:
     * - the card is centered
     * - inputs will be of width 540 when the browser is 1920x1080
     * - labels are displayed to the left of the inputs with their text being right aligned
     * - card header is left aligned
     * - button will be left aligned to input at the bottom of the page
     */
    //start chrome --disable-gpu --headless --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222
    public function testDesktopAlignment()
    {

        $mink = new Mink(array(
            'UpdateProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('UpdateProfile');
        $mink->getSession()->visit('http://localhost:8000');

        $session =  $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $driver = $mink->getSession()->getDriver();

        $page = $mink->getSession()->getPage();

        $mink->getSession()->resizeWindow(1000, 1040);

        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        $xyInputCoor = <<<JS
        $('.form-control').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;

        // Store input coordinates in arrays
        $inputXY = $mink->getSession()->evaluateScript($xyInputCoor);

        $xyLabelCoor = <<<JS
        $('label').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth(), class: $(element).attr("class")};
        }).get();
JS;

        // Store Label elements in arrays
        $labelXY = $mink->getSession()->evaluateScript($xyLabelCoor);

        $xyButtonCoor = <<<JS
        $('button').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;
        // Store Button elements in arrays
        $buttonXY = $mink->getSession()->evaluateScript($xyButtonCoor);


        $xyCardCoor = <<<JS
        $('div.card').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, right: $(element).outerWidth() + $(element).offset().left, text: $(element).text(),
                    class: $(element).attr("class"), childClass:$(element).children().eq(0).attr("class")};
        }).get();
JS;
        // Store Button elements in arrays
        $cardXY = $mink->getSession()->evaluateScript($xyCardCoor);

        // Get top input element to compare others to
        $xInputVal = $inputXY[0]["left"];
        $xLabelVal = $labelXY[0]["left"];
        $inputWidth = 429;

        for ($i = 0; $i < count($inputXY); $i++)
        {
            // Test that all inputs are left aligned
            $this->assertEquals($xInputVal, $inputXY[$i]["left"]);

            // Each label is in the same row as its input box
            $this->assertEquals($inputXY[$i]["top"], $labelXY[$i]["top"]);

            // Test that all labels are left aligned
            //$this->assertEquals($xLabelVal, $labelXY[$i]["left"]);

            // Test that each input is the correct width
            $this->assertEquals($inputWidth, $inputXY[$i]["width"]);

            // Assert that the text is right aligned by checking the class attribute
            $this->assertContains("text-sm-right", $labelXY[$i]["class"]);
        }

        // Assert that the button is left aligned with inputs
        $this->assertEquals($xInputVal, $buttonXY[1]["left"]);

        // Assert that the button is below the last input tag
        $lastInputTop = end($inputXY)["top"];
        $buttonTop = $buttonXY[1]["top"];

        $this->assertLessThan($buttonTop, $lastInputTop);

        // Assert that the card is in the center of the page
        $this->assertEquals($cardXY[0]["left"], 1000 - $cardXY[0]["right"]);

        // Assert that the card title is "Update Profile" and is left aligned
        $headerText = $page->find("css", "h2.card-header")->getText();
        $this->assertEquals("Update Profile", $headerText);

        $this->assertContains("card-header text-sm-left text-xs-left", $cardXY[0]["childClass"]);
    }


    /** 34
     * CoryN and KateZ
     * March 5, 2019
     * This function will test that:
     * - the card is centered
     * - inputs will be of width 545 when the browser is 575 x 812
     * - labels are displayed above the inputs with their text being left aligned
     * - card header is left aligned
     * - button will be the same width as the input at the bottom of the page
     */
    //start chrome --disable-gpu --headless --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222
    public function testMobileAlignment()
    {

        $mink = new Mink(array(
            'UpdateProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('UpdateProfile');
        $mink->getSession()->visit('http://localhost:8000');


        $session =  $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $driver = $mink->getSession()->getDriver();

        $page = $mink->getSession()->getPage();

        $mink->getSession()->resizeWindow(575, 812);

        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        $xyInputCoor = <<<JS
        $('.form-control').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;

        // Store input coordinates in arrays
        $inputXY = $mink->getSession()->evaluateScript($xyInputCoor);

        $xyLabelCoor = <<<JS
        $('label').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth(), class: $(element).attr("class")};
        }).get();
JS;

        // Store Label elements in arrays
        $labelXY = $mink->getSession()->evaluateScript($xyLabelCoor);

        $xyButtonCoor = <<<JS
        $('button').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;
        // Store Button elements in arrays
        $buttonXY = $mink->getSession()->evaluateScript($xyButtonCoor);


        $xyCardCoor = <<<JS
        $('div.card').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, right: $(element).outerWidth() + $(element).offset().left, text: $(element).text(),
                    class: $(element).attr("class"), childClass:$(element).children().eq(0).attr("class")};
        }).get();
JS;
        // Store Button elements in arrays
        $cardXY = $mink->getSession()->evaluateScript($xyCardCoor);

        // Get top input element to compare others to
        $xInputVal = $inputXY[0]["left"];
        $xLabelVal = $labelXY[0]["left"];
        $inputWidth = 486;
        $yValLabel = $labelXY[0]["top"];

        for ($i = 0; $i < count($inputXY); $i++)
        {
            $this->assertEquals($yValLabel, $labelXY[$i]["top"]);

            // The height of the label
            $yValLabel += 51;

            // The height of the input and padding
            $yValLabel += 54;

            // Test that all inputs are left aligned
            $this->assertEquals($xInputVal, $inputXY[$i]["left"]);

            // Test that all labels are left aligned
            $this->assertEquals($xLabelVal, $labelXY[$i]["left"]);

            // Test that each input is the correct width
            $this->assertEquals($inputWidth, $inputXY[$i]["width"]);

            // Assert that the text is left aligned by checking the class attribute
            $this->assertContains("text-sm-right text-xs-left", $labelXY[$i]["class"]);
        }

        // Assert that the button has the same width as the inputs
        $this->assertEquals($inputWidth, $buttonXY[1]["width"]);

        // Assert that the button is below the last input tag
        $lastInputTop = end($inputXY)["top"];
        $buttonTop = $buttonXY[1]["top"];

        $this->assertLessThan($buttonTop, $lastInputTop);

        // Assert that the card is in the center (subtract 17 pixels for the scrollbar)
        $this->assertEquals($cardXY[0]["left"], 575 - $cardXY[0]["right"] - 17);

        // Assert that the card title is "Update Profile" and is left aligned
        $headerText = $page->find("css", "h2.card-header")->getText();
        $this->assertEquals("Update Profile", $headerText);

        $this->assertContains("card-header text-sm-left text-xs-left", $cardXY[0]["childClass"]);
    }


    /** 35
     * March 5, 2019
     * This function will check that error messages:
     * - are placed to the right of the input field
     * - contain a class of error_message
     * - are left aligned
     * Kate Z and Cory N
     * start chrome --disable-gpu --headless --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222
     */
    public function testErrorMessasgesDisplayedProperlyDesktop()
    {
        $mink = new Mink(array(
            'UpdateProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('UpdateProfile');
        $mink->getSession()->visit('http://localhost:8000');


        $session =  $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $driver = $mink->getSession()->getDriver();

        $page = $mink->getSession()->getPage();

        $mink->getSession()->resizeWindow(1000, 1040);

        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        $page->find('css', "#member_update_firstName")->setValue("");
        $page->find('css', "#member_update_lastName")->setValue("");
        $page->find('css', "#member_update_userName")->setValue("");
        $page->find('css', "#member_update_addressLineOne")->setValue("");
        $page->find('css', "#member_update_province")->setValue("");
        $page->find('css', "#member_update_city")->setValue("");
        $page->find('css', "#member_update_postalCode")->setValue("");
        $page->find('css', "#member_update_phone")->setValue(str_repeat(1, 50));
        $page->find('css', "#member_update_addressLineTwo")->setValue(str_repeat("a", 101));
        $page->find('css', "#member_update_company")->setValue(str_repeat("a", 101));

        $xyInputCoor = <<<JS
        $('.form-control').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;

        // Store input coordinates in arrays
        $inputXY = $mink->getSession()->evaluateScript($xyInputCoor);

        $xyErrorCoor = <<<JS
        $('.alert.alert-danger').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth(),  class: $(element).attr("class")};
        }).get();
JS;
        // Store error elements in arrays
        $errorXY = $mink->getSession()->evaluateScript($xyErrorCoor);

        // Get top input element to compare others to
        $errorXVal = $errorXY[0]["left"];

        $yVal = $inputXY[0]["top"];

        for ($i = 0; $i < count($errorXY); $i++)
        {
            // Assert that we're at the top of the input field
            $this->assertEquals($inputXY[$i]["top"], $yVal);

            // Add 38 to y-val to point us to the top of the error message
            $yVal += 38;
            $yVal += 3.59375;

            $this->assertEquals($yVal, $errorXY[$i]["top"]);

            // Add padding height and height of error message
            $yVal += 52;

            // Test that all errors are left aligned
            $this->assertEquals($errorXVal, $errorXY[$i]["left"]);

            // Test that error field contain a class of error_message
            $this->assertContains("alert alert-danger", $errorXY[$i]["class"]);
        }
    }

    /** 36
     * This method will test that the error messages are displayed underneath the input boxes, are left aligned,
     * and are the same width as the inputs while on mobile view (575 px)
     *
     * Kate Z and Cory N
     */
    public function testErrorMessasgesDisplayedProperlyMobile()
    {
        $mink = new Mink(array(
            'UpdateProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('UpdateProfile');
        $mink->getSession()->visit('http://localhost:8000');


        $session =  $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $driver = $mink->getSession()->getDriver();

        $page = $mink->getSession()->getPage();

        $mink->getSession()->resizeWindow(575, 812);

        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        $page->find("css", "#navbarDropdown")->click();
        $page->find("css", "#updateProfile")->click();

        $page->find('css', "#member_update_firstName")->setValue("");
        $page->find('css', "#member_update_lastName")->setValue("");
        $page->find('css', "#member_update_userName")->setValue("");
        $page->find('css', "#member_update_addressLineOne")->setValue("");
        $page->find('css', "#member_update_province")->setValue("");
        $page->find('css', "#member_update_city")->setValue("");
        $page->find('css', "#member_update_postalCode")->setValue("");
        $page->find('css', "#member_update_phone")->setValue(str_repeat(1, 50));
        $page->find('css', "#member_update_addressLineTwo")->setValue(str_repeat("a", 101));
        $page->find('css', "#member_update_company")->setValue(str_repeat("a", 101));

        $xyInputCoor = <<<JS
        $('.form-control').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;

        // Store input coordinates in arrays
        $inputXY = $mink->getSession()->evaluateScript($xyInputCoor);

        $xyErrorCoor = <<<JS
        $('.alert.alert-danger').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth(),  class: $(element).attr("class")};
        }).get();
JS;
        $xyLabelCoor = <<<JS
        $('label').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth(), class: $(element).attr("class")};
        }).get();
JS;

        // Store Label elements in arrays
        $labelXY = $mink->getSession()->evaluateScript($xyLabelCoor);

        // Store error elements in arrays
        $errorXY = $mink->getSession()->evaluateScript($xyErrorCoor);

        // Get top input element to compare others to
        $yValInput = $inputXY[0]["top"] - 51;

        for ($i = 0; $i < count($errorXY); $i++)
        {
            // Move to the bottom of the label
            $yValInput += 51;

            // Asset that we're at the top of the input field
            $this->assertEquals($inputXY[$i]["top"], $yValInput);

            // Add 38 to y-val to point us to the top of the error message
            $yValInput += 38;
            $yValInput += 3.59375;


            $this->assertEquals($yValInput, $errorXY[$i]["top"]);

            // Add padding height and height of error message
            $yValInput += 52;

            // Test that all errors are left aligned
            $this->assertEquals($inputXY[$i]["left"], $errorXY[$i]["left"]);

            // Test that error field contain a class of error_message
            $this->assertContains("alert alert-danger", $errorXY[$i]["class"]);

            // Test the the width of the error message box is the same as the input field
            $this->assertEquals($errorXY[$i]["width"], $inputXY[$i]["width"]);
        }




    }

    /** 37
     * This will test that a Member cannot access another member's profile page and that they are instead redirected
     * to their own
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testThatMemberCannotEditOrViewAnotherMemberProfile()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("member@member.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        $mink->getSession()->visit('http://localhost:8000/member/edit/2');

        // Check that the page has properly redirected to the update profile page
        $url = $driver->getCurrentUrl();

        $this->assertEquals($url, 'http://localhost:8000/member/edit/1');
    }

    /** 38
     * This will test that a public cannot access another member's profile page and that they are instead redirected
     * to login
     * @author Kate Zawada and Cory Nagy
     *
     * @throws
     */
    public function testThatPublicUserCannotEditOrViewAnotherMemberProfile()
    {
        //LOGIN AS A MEMBER
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();

        $mink->getSession()->visit('http://localhost:8000/member/edit/2');

        // Check that the page has properly redirected to the update profile page
        $url = $driver->getCurrentUrl();

        $this->assertEquals($url, 'http://localhost:8000/login');
    }

}
