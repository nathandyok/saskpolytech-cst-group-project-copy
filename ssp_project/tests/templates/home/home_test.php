<?php
/**
 * User: cst220
 * Date: 3/4/2019
 * SERVER LOGIC BLOCK
 */

namespace App\Tests\templates\home;

use DMore\ChromeDriver\ChromeDriver;
use Behat\Mink\Mink;
use Behat\Mink\Session;
use Liip\FunctionalTestBundle\Test\WebTestCase;

class hometest extends  WebTestCase
{
    /**
     * This function will test that the welcome banner is the first item on the page after the nav bar
     * on desktop view
     *
     * @author Dylan & Taylor
     */
    public function setUp()
    {
        $this->loadFixtures(array(
            'App\DataFixtures\AllShowsFixtures',
            'App\DataFixtures\AppFixtures',
            'App\DataFixtures\AddMembersFixtures',
            'App\DataFixtures\MemberLoginTestFixture'
        ));
    }

    public function testThatWelcomeBannerFirstItemOnPageDesktop()
    {
        // Load ChromeDriver and visit the hompage
        $mink = new Mink(array(
            'Homepage' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('Homepage');
        $mink->getSession()->visit('http://localhost:8000/');
        $page = $mink->getSession()->getPage();

        //Resize the page to desktop view
        $mink->getSession()->resizeWindow(1000,1080);

        //Assert that the banner image is there and is the second div on the page
        $this->assertTrue( $page->find('css', '.jumboContainer')->isVisible());

        //Assert that the banner has the correct class
        $welcomeBanner = $page->find('css', '.jumboContainer div');
        $this->assertEquals("jumbotron jumbotron-fluid", $welcomeBanner->getAttribute("class"), "jumbotron");
    }

    /**
     * This function will test that the welcome banner is the first item on the page after the nav bar
     * on mobile view
     *
     * @author Dylan & Taylor
     */
    public function testThatWelcomeBannerFirstItemOnPageMobile()
    {
        // Load ChromeDriver and visit the homepage
        $mink = new Mink(array(
            'Homepage' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('Homepage');
        $mink->getSession()->visit('http://localhost:8000/');
        $page = $mink->getSession()->getPage();

        //Resize the page to mobile view
        $mink->getSession()->resizeWindow(575,800);

        //Assert that the banner image is there and is the second div on the page
        $this->assertTrue( $page->find('css', '.jumboContainer')->isVisible());

        //Assert that the banner has the correct class
        $welcomeBanner = $page->find('css', '.jumboContainer div');
        $this->assertEquals("jumbotron jumbotron-fluid", $welcomeBanner->getAttribute("class"), "jumbotron");
    }



    /**
     * This function will test the welcome banner has no whitespace to the left or right of it and is not cropped
     * in desktop
     *
     * @author Dylan & Taylor
     */
    public function testThatWelcomePhotoExpandsToEdgeOfScreenDeskTop()
    {
        // Load ChromeDriver and visit the homepage
        $mink = new Mink(array(
            'Homepage' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('Homepage');
        $mink->getSession()->visit('http://localhost:8000/');
        $page = $mink->getSession()->getPage();

        //Resize the page to desktop view
        $mink->getSession()->resizeWindow(1000,1080);

        $session =  $mink->getSession();
        $session->wait(2000, '(0 === jQuery.active)');

        //Assert that the banner image is there and is the second div on the page
        $this->assertTrue($page->find('css', '.jumboContainer')->isVisible());

        $bannerWidthDifference = <<<JS
        return $('.jumboContainer').width();
JS;
        $bodyWidth = <<<JS
        return $('body').innerWidth();
JS;

        //Assert that the width of the banner is the same as the width of the page
        $this->assertEquals( $mink->getSession()->evaluateScript($bodyWidth), $mink->getSession()->evaluateScript($bannerWidthDifference));

    }

    /**
     * This function will test the welcome banner has no whitespace to the left or right of it and is not cropped
     * in mobile
     *
     * @author Dylan & Taylor
     */
    public function testThatWelcomePhotoExpandsToEdgeOfScreenMobile()
    {
        // Load ChromeDriver and visit the homepage
        $mink = new Mink(array(
            'Homepage' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('Homepage');
        $mink->getSession()->visit('http://localhost:8000/');
        $page = $mink->getSession()->getPage();

        //Resize the page to desktop view
        $mink->getSession()->resizeWindow(575,1080);

        $session =  $mink->getSession();
        $session->wait(2000, '(0 === jQuery.active)');

        //Assert that the banner image is there and is the second div on the page
        $this->assertTrue($page->find('css', '.jumboContainer')->isVisible());

        $bannerWidthDifference = <<<JS
        return $('.jumboContainer').width();
JS;
        $bodyWidth = <<<JS
        return $('body').innerWidth();
JS;

        //Assert that the width of the banner is the same as the width of the page
        $this->assertEquals( $mink->getSession()->evaluateScript($bodyWidth), $mink->getSession()->evaluateScript($bannerWidthDifference));
    }

    /**
     * This function will test that show information is properly vertically stacked on mobile
     *
     * @author Dylan & Taylor
     */
    public function testShowInformationVerticallyStackedMobileNotLoggedIn()
    {
        // Load ChromeDriver and visit the homepage
        $mink = new Mink(array(
            'Homepage' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('Homepage');
        $mink->getSession()->visit('http://localhost:8000/');
        $page = $mink->getSession()->getPage();

        //Resize the page to mobile view
        $mink->getSession()->resizeWindow(575,900);

        $session =  $mink->getSession();
        $session->wait(2000, '(0 === jQuery.active)');

        //Assert that the banner has the correct class
        $showDiv = $page->find('css', '.currentShow');

        $img = $showDiv->find("css", "img.shadow-lg");

        $showInfo = $showDiv->find("css", ".showInfo > h1");

        //Assert second child of show div is title
        $this->assertContains("Sweeney Todd", $showDiv->find("css", ".showInfo > h1")->getText());

        $allignedInformationJS = <<<JS

        var returnArray = [];

        returnArray.push($("#showImgMobile").offset().top);
        returnArray.push($("#showDescription").offset().top);
        return returnArray;
JS;

        $offsetArray = $mink->getSession()->evaluateScript($allignedInformationJS);

        //Assert that the image (0) is higher than the description (1)
        $this->assertTrue($offsetArray[0] < $offsetArray[1]);
    }

    /**
     * This function will test that all of the buttons in the footer (donate, facebook, twitter) are in the correct
     * order
     *
     * @author Dylan & Taylor
     */
    public function testFooterButtonsAreInCorrectOrder()
    {
        // Load ChromeDriver and visit the homepage
        $mink = new Mink(array(
            'Homepage' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('Homepage');
        $mink->getSession()->visit('http://localhost:8000/');
        $page = $mink->getSession()->getPage();


        //Assert that the banner image is there and is the second div on the page
        $this->assertTrue($page->find('css', 'footer')->isVisible());

        //Assert that the banner has the correct class
        $footer = $page->find('css', 'footer');

        //Test that all footer buttons have a gap of 50px between eachother
        $allignedInformationJS = <<<JS

        var returnArray = [];

        returnArray.push($("#twitter").offset().left);
        returnArray.push($("#facebook").offset().left);
        returnArray.push($("#donate").offset().left);
        
        return returnArray;
JS;

        //Get all the left offsets of the information elements
        $offsetArray = $mink->getSession()->evaluateScript($allignedInformationJS);

        $this->assertTrue($offsetArray[2] > $offsetArray[1] && $offsetArray[1] > $offsetArray[0]);
    }

    public function testFooterWidthTakesFullWidthDesktop()
    {
        // Load ChromeDriver and visit the homepage
        $mink = new Mink(array(
            'Homepage' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('Homepage');
        $mink->getSession()->visit('http://localhost:8000/');
        $page = $mink->getSession()->getPage();

        //Resize the page to desktop view
        $mink->getSession()->resizeWindow(1000,1080);

        $session =  $mink->getSession();
        $session->wait(2000, '(0 === jQuery.active)');

        //Assert that the banner image is there and is the second div on the page
        $this->assertTrue($page->find('css', 'footer')->isVisible());

        $bannerWidthDifference = <<<JS
        return $('footer').width();
JS;
        $bodyWidth = <<<JS
        return $('body').innerWidth();
JS;


        //Assert that the width of the banner is the same as the width of the page
        $this->assertEquals( $mink->getSession()->evaluateScript($bodyWidth), $mink->getSession()->evaluateScript($bannerWidthDifference));
    }

    public function testFooterTakesFullWidthMobile()
    {
        // Load ChromeDriver and visit the homepage
        $mink = new Mink(array(
            'Homepage' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('Homepage');
        $mink->getSession()->visit('http://localhost:8000/');
        $page = $mink->getSession()->getPage();

        //Resize the page to desktop view
        $mink->getSession()->resizeWindow(575,1080);

        $session =  $mink->getSession();
        $session->wait(2000, '(0 === jQuery.active)');

        //Assert that the banner image is there and is the second div on the page
        $this->assertTrue($page->find('css', 'footer')->isVisible());

        $bannerWidthDifference = <<<JS
        return $('footer').width();
JS;
        $bodyWidth = <<<JS
        return $('body').innerWidth();
JS;


        //Assert that the width of the banner is the same as the width of the page
        $this->assertEquals( $mink->getSession()->evaluateScript($bodyWidth), $mink->getSession()->evaluateScript($bannerWidthDifference));
    }

    /**
     * This function will test that when clicking on the Facebook button in the footer it opens a new tab with SPP's
     * facebook page
     *
     * @author Dylan & Taylor
     */
    public function testFacebookButtonSuccessfullyRedirect()
    {
        // Get client to start on the show page
        $client = static::createClient();
        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target donate button in navbar by its ID
        $donateLink = $crawler->filter('#facebook')->link();
        $crawler = $client->click($donateLink);
        $this->assertEquals('https://www.facebook.com/SaskatoonSummerPlayers', $crawler->getUri());
    }

    /**
     * This function will test that when clicking on the Twitter button in the footer it opens a new tab with SPP's
     * twitter page
     *
     * @author Dylan & Taylor
     */
    public function testTwitterButtonSuccessfullyRedirect()
    {
        // Get client to start on the show page
        $client = static::createClient();
        $client->request('GET', '/');
        $crawler = $client->getCrawler();

        // Target donate button in navbar by its ID
        $donateLink = $crawler->filter('#twitter')->link();
        $crawler = $client->click($donateLink);
        $this->assertEquals('https://twitter.com/saskatoonsummer', $crawler->getUri());

    }

    /**
     * This function will test that when clicking on the Donate button in the footer it opens a new tab with SPP's
     * donate page
     *
     * @author Dylan & Taylor
     */
    public function testDonateButtonSuccessfullyRedirect()
    {
        // Load ChromeDriver and visit the homepage
        $mink = new Mink(array(
            'Homepage' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('Homepage');
        $mink->getSession()->visit('http://localhost:8000/');
        $page = $mink->getSession()->getPage();

        //Click the donate button
        $page->clickLink("donate");

        //Look for the title of the CanadaHelps page
        $this->assertContains("<title>Donate Now - SASKATOON SUMMER PLAYERS</title>\n", $page->getHtml());
    }

    /**
     * This function will test that navigation bar is in hamburger mode when in mobile view
     *
     * @author Dylan & Taylor
     */
    public function testNavigationBarHamburgerMobile()
    {
        // Load ChromeDriver and visit the homepage
        $mink = new Mink(array(
            'Homepage' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('Homepage');
        $mink->getSession()->visit('http://localhost:8000/');
        $page = $mink->getSession()->getPage();

        $mink->getSession()->resizeWindow(1000,900);

        //Check that the hamburger menu is not shown to you before you are on mobile
        $this->assertFalse($page->find('css', '.navbar-toggler-icon')->isVisible());

        //Resize the page to mobile view and make sure the hamburger button is visible
        $mink->getSession()->resizeWindow(575,900);
        $this->assertTrue($page->find('css', '.navbar-toggler-icon')->isVisible());

        //Test that navigation items are not currently visible because the hamburger menu is not expanded
        $this->assertFalse($page->find('css', '.nav-link')->isVisible());

        //Click the hamborger to make the nav items visible
        $page->find("css", ".navbar-toggler")->click();
        $this->assertTrue($page->find('css', '.nav-link')->isVisible());
    }

    /**
     * This function will test that the current show is the first thing visible on all pages
     *
     * @author Dylan & Taylor
     */
    public function testCurrentShowVisibleAndAfterBanner()
    {
        // Load ChromeDriver and visit the homepage
        $mink = new Mink(array(
            'Homepage' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('Homepage');
        $mink->getSession()->visit('http://localhost:8000/');
        $page = $mink->getSession()->getPage();

        $session =  $mink->getSession();
        $session->wait(2000, '(0 === jQuery.active)');

        //Assert that the banner image is there and is the second div on the page
        $this->assertTrue($page->find('css', '.currentShow')->isVisible());

        $welcomeBannerHeight = <<<JS
        return $('.jumboContainer').offset().bottom;
JS;
        $showDivHeight = <<<JS
        return $('.currentShow').offset().top;
JS;

        //Assert that the width of the banner is the same as the width of the page
        $this->assertTrue( $mink->getSession()->evaluateScript($showDivHeight) > $mink->getSession()->evaluateScript($welcomeBannerHeight));
    }

    /**
     * This function will test that the current show is displayed properly on desktop view (information of the left
     * side of the page and the show poster on the right side of the page
     *
     * @author Dylan & Taylor
     */
    public function testCurrentShowProperOnDesktop()
    {
        // Load ChromeDriver and visit the homepage
        $mink = new Mink(array(
            'Homepage' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('Homepage');
        $mink->getSession()->visit('http://localhost:8000/');
        $page = $mink->getSession()->getPage();

        $session =  $mink->getSession();
        $session->wait(2000, '(0 === jQuery.active)');

        $mink->getSession()->resizeWindow(1000,900);

        //Assert that mobile show picture is not visible and the desktop picture is visible
        $this->assertTrue($page->find("css", "#showImgDesktop")->isVisible());
        $this->assertFalse($page->find("css", "#showImgMobile")->isVisible());

        //Test that the picture is not in the same column and the information
        $picLeft = <<<JS
         return $("#showImgDesktop").offset().left;
JS;

        $descriptionRight = <<<JS
        return $("#showImgMobile").offset().right;
JS;

        $this->assertTrue( $mink->getSession()->evaluateScript($picLeft) > $mink->getSession()->evaluateScript($descriptionRight));
    }


    /**
     * This function will test that all information font is EB Garamond
     *
     * @author Dylan & Taylor
     */
    public function testThatFontIsCorrect()
    {
        // Load ChromeDriver and visit the homepage
        $mink = new Mink(array(
            'Homepage' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('Homepage');
        $mink->getSession()->visit('http://localhost:8000/');
        $page = $mink->getSession()->getPage();

        //This is still JS instead of jQuery because the jQuery implementation is a big mess - this is much shorter
        $fontCorrect = <<<JS
            return $('body').css('font-family');
JS;

        //Assert that EB Garamond font is on the page
        $font = $mink->getSession()->evaluateScript($fontCorrect);
        $this->assertContains("EB Garamond", $font);
    }


    /**
     * This function will test that the welcome message on the banner is visible on desktop
     *
     * @author Dylan & Taylor
     */
    public function testWelcomeMessageVisibleOnDesktop()
    {
        // Load ChromeDriver and visit the homepage
        $mink = new Mink(array(
            'Homepage' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('Homepage');
        $mink->getSession()->visit('http://localhost:8000/');
        $page = $mink->getSession()->getPage();

        //Resize the page to mobile view
        $mink->getSession()->resizeWindow(1000,900);

        //Assert that the welcome message in the welcome banner is not visible
        $this->assertTrue($page->find("css", ".jumboText")->isVisible());
    }

    /**
     * This function will test that the welcome message on the banner is visible on desktop
     *
     * @author Dylan & Taylor
     */
    public function testWelcomeMessageVisibleOnMobile()
    {
        // Load ChromeDriver and visit the homepage
        $mink = new Mink(array(
            'Homepage' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('Homepage');
        $mink->getSession()->visit('http://localhost:8000/');
        $page = $mink->getSession()->getPage();

        //Resize the page to mobile view
        $mink->getSession()->resizeWindow(575,900);

        //Assert that the welcome message in the welcome banner is not visible
        $this->assertTrue($page->find("css", ".jumboText")->isVisible());
    }

    /**
     * This test will check to ensure that the layout of the navigation bar is correct across various different browser sizes.
     *
     * @author  Taylor, Dylan
     */
    public function testNavbarOptionsEqualSizeAndSpacingDesktop()
    {
        // Load ChromeDriver
        $mink = new Mink(array(
            'Homepage' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // set the default session name
        $mink->setDefaultSessionName('Homepage');
        $mink->getSession()->visit('http://localhost:8000/');
        $session = $mink->getSession();
        $session->wait(2000, '(0 === jQuery.active)');
        $page = $mink->getSession()->getPage();

        //resize to desktop
        $mink->getSession()->resizeWindow(2000,900);

        //Make sure all navbar items have to correct padding class
        $navItems = $page->findAll("css", ".nav-link");

        foreach($navItems as $item)
        {
            $this->assertContains("px-3", $item->getAttribute("class"));
        }

        //Get the offset of the nav items and make sure they are in order from left to right

        //get width of all nav items
        $homeNav = <<<JS
        return $("#homeNav").offset().left;
JS;

        $showsNav = <<<JS
        return $("#showsNav").offset().left;
JS;

        $newsNav = <<<JS
        return $("#newsNav").offset().left;
JS;

        $aboutNav = <<<JS
        return $("#aboutNav").offset().left;
JS;

        $memberNav = <<<JS
        return $("#regNav").offset().left;
JS;

        //Assert show to the right of newsletter
        $this->assertTrue($mink->getSession()->evaluateScript($showsNav) > $mink->getSession()->evaluateScript($homeNav));

        //Assert newsletter to the right of our shows
        $this->assertTrue($mink->getSession()->evaluateScript($newsNav) > $mink->getSession()->evaluateScript($showsNav));

        //Assert about us to the right of newsletter
        $this->assertTrue($mink->getSession()->evaluateScript($aboutNav) > $mink->getSession()->evaluateScript($newsNav));

        //Assert become mebmer to the right of about us
       $this->assertTrue($mink->getSession()->evaluateScript($memberNav) > $mink->getSession()->evaluateScript($aboutNav));
    }

    /**
     * test that the navbar items take up the full screen width on mobile
     *
     * @author Taylor, Dylan
     */
    public function testThatNavbarItemsFormattedCorrectOnMobile()
    {
        // Load ChromeDriver
        $mink = new Mink(array(
            'Homepage' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // set the default session name
        $mink->setDefaultSessionName('Homepage');
        $mink->getSession()->visit('http://localhost:8000/');
        $session = $mink->getSession();
        $session->wait(2000, '(0 === jQuery.active)');
        $page = $mink->getSession()->getPage();

        //resize to mobile
        $mink->getSession()->resizeWindow(575,900);

        //click the hamburger
        $page->find("css", ".navbar-toggler")->click();

        $testVarDeleteLater = $page->find("css", "#homeNav")->isVisible();

        //Make sure all navbar items have to correct padding class
        $navItems = $page->findAll("css", ".nav-link");

        foreach($navItems as $item)
        {
            $this->assertContains("px-3", $item->getAttribute("class"));
        }

        //Get the offset of the nav items and make sure they are in order from left to right

        //get width of all nav items
        $homeNav = <<<JS
        return $("#homeNav").offset().top;
JS;

        $showsNav = <<<JS
        return $("#showsNav").offset().top;
JS;

        $newsNav = <<<JS
        return $("#newsNav").offset().top;
JS;

        $aboutNav = <<<JS
        return $("#aboutNav").offset().top;
JS;

        $memberNav = <<<JS
        return $("#regNav").offset().top;
JS;

        $val1 = $mink->getSession()->evaluateScript($showsNav);
        $val2 = $mink->getSession()->evaluateScript($homeNav);


        //Assert show to the right of newsletter
        $this->assertTrue($mink->getSession()->evaluateScript($showsNav) > $mink->getSession()->evaluateScript($homeNav));

        //Assert newsletter to the right of our shows
        $this->assertTrue($mink->getSession()->evaluateScript($newsNav) > $mink->getSession()->evaluateScript($showsNav));

        //Assert about us to the right of newsletter
        $this->assertTrue($mink->getSession()->evaluateScript($aboutNav) > $mink->getSession()->evaluateScript($newsNav));

        //Assert become mebmer to the right of about us
        $this->assertTrue($mink->getSession()->evaluateScript($memberNav) > $mink->getSession()->evaluateScript($aboutNav));
    }


    /*
     * This function will test that the member has the proper navabr items when they are logged in
     * @author Dylan Taylor
     */
    public function testMemberNavbarItemsAreThere()
    {
        //login as a member
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("member@member.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();


        //Check that all required navbar items are there
        $page->find("css", "#navbarDropdown")->click();
        $this->assertTrue($page->find("css", "#volunteer")->isVisible());


        //Check that other navbar items are not there
        $this->assertNull($page->find("css", "#onlinePoll"));
        $this->assertNull($page->find("css", "#SuggestShows"));
        $this->assertNull($page->find("css", "#onlinePoll"));

    }

    /*
    * This function will test that the board member has the proper navabr items when they are logged in
    * @author Dylan Taylor
    */
    public function testBoardMemberNavbarItemsAreThere()
    {
        //login as a board member
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("bmember@member.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();


        //Check that all required navbar items are there
        $page->find("css", "#navbarDropdown")->click();
        $this->assertTrue($page->find("css", "#volunteer")->isVisible());
        $this->assertTrue($page->find("css", "#onlinePoll")->isVisible());

        //Check that other navbar items are not there
        $this->assertNull($page->find("css", "#SuggestShows"));
        $this->assertNull($page->find("css", "#editShows"));
    }

    /*
    * This function will test that the general manager has the proper navabr items when they are logged in
    * @author Dylan Taylor
    */
    public function testGMemberNavbarItemsAreThere()
    {
        //login as a general manager
        $mink = new Mink(array(
            'MemberProfile' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('MemberProfile');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("gmember@member.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();


        //Check that all required navbar items are there
        $page->find("css", "#navbarDropdown")->click();
        $this->assertTrue($page->find("css", "#volunteer")->isVisible());
        $this->assertTrue($page->find("css", "#editShows")->isVisible());
        $this->assertTrue($page->find("css", "#SuggestShows")->isVisible());
        $this->assertTrue($page->find("css", "#onlinePoll")->isVisible());
    }

    /*
     * This function will test that an error message is shown on the hompage when there is no show in the database
     */
    public function testErrorMessagePresentWhenNoShow()
    {
        //Run fixture to clear the database
        $this->loadFixtures(array(
            'App\DataFixtures\BlankFixtures'
        ));

        // Load ChromeDriver
        $mink = new Mink(array(
            'Homepage' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // set the default session name
        $mink->setDefaultSessionName('Homepage');
        $mink->getSession()->visit('http://localhost:8000/');
        $session = $mink->getSession();
        $session->wait(2000, '(0 === jQuery.active)');
        $page = $mink->getSession()->getPage();

        $this->assertContains("No shows available at this time.",$page->getHtml());
    }

    /**
     * This function will tes that the welcome image is the same height in all sizes of screen
     * @author Taylor & Dylan
     */
    public function testWelcomeImageHeightStaysTheSame()
    {
        // Load ChromeDriver
        $mink = new Mink(array(
            'Homepage' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        // set the default session name
        $mink->setDefaultSessionName('Homepage');
        $mink->getSession()->visit('http://localhost:8000/');
        $session = $mink->getSession();
        $session->wait(2000, '(0 === jQuery.active)');
        $page = $mink->getSession()->getPage();

        //resize to desktop
        $mink->getSession()->resizeWindow(1000, 900);
        $jumboHeight = <<<JS
         return $(".jumboContainer").height();
JS;
        $this->assertEquals(270, $mink->getSession()->evaluateScript($jumboHeight));

        //resize to mobile - should stay the same
        $mink->getSession()->resizeWindow(575, 900);
        $jumboHeight = <<<JS
         return $(".jumboContainer").height();
JS;
        $this->assertEquals(270, $mink->getSession()->evaluateScript($jumboHeight));
    }
}
