<?php
/**
 * User: CST221
 * Date: 1/24/2019
 * SERVER LOGIC BLOCK
 */

namespace App\Tests\Controller;

use app\Entity\member;
use app\Entity\auditionDetails;


use DMore\ChromeDriver\ChromeDriver;
use Behat\Mink\Mink;
use Behat\Mink\Session;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use function PHPSTORM_META\elementType;
use WebDriver\Exception;

class auditionlist extends WebTestCase
{
    private $fixtures;

   public $mink;
    // https://jqueryvalidation.org/rules/
    /** start chrome --disable-gpu --headless --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222
     * Set up will load fixtures and by default log you in as a board member
     * @throws
     */
    public function setUp()
    {
        $this->fixtures = $this->loadFixtures(array(
           'App\DataFixtures\AddOnlinePollsFixtures',
            'App\DataFixtures\MemberLoginTestFixture',
            'App\DataFixtures\AddMembersFixtures',
            'App\DataFixtures\AllShowsFixtures'
        ));

        $this->mink = new Mink(array(
            'Auditions' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $this->mink ->setDefaultSessionName('Auditions');
        $this->mink ->getSession()->visit('http://localhost:8000/login');

        $page = $this->mink->getSession()->getPage();

    }

    /**
     * This will test that a GM can navigate and view the Applicants Page
     *
     * @author Kate Zawada and Taylor Beverly
     */
    public function testThatGMCanViewApplicantsPage()
    {
        $mink = $this->mink;
        $page = $mink->getSession()->getPage();

        // Login
        $page->find("css","#_username")->setValue("gmember@member.com");
        $page->find("css","#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        //Should now be on homepage, navigate to the option and click to get to the Audition Details page for Marry Poppins
        $page->find("css","#navbarDropdown")->click();
        $page->find("css", "#auditionList")->click();


        $driver = $mink->getSession()->getDriver();

        //should now be on the page
        $this->assertEquals("http://localhost:8000/auditiondetails/board/applicants/", $driver->getCurrentUrl());

        $mink->stopSessions();
    }


    /** 6
     * This will test for the Applicants Page on desktop:
     *
     * Title is center-aligned and above the select a show label
     * Members are listed underneath displaying first name, last name, and email
     * Members are left-aligned
     *
     * @author Kate Zawada and Taylor Beverly
     */
    public function testApplicantListPageDisplayDesktop()
    {
        $mink = $this->mink;
        $page = $mink->getSession()->getPage();
        $session =  $mink->getSession();
        $session->wait(2000, '(0 === jQuery.active)');

        // Login
        $page->find("css","#_username")->setValue("bmember@member.com");
        $page->find("css","#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        // Navigate to Audition Interests
        $page->find("css","#navbarDropdown")->click();
        $page->find("css", "#auditionList")->click();

        // Select Hamlet to view Audition Interests
        $page->find("css", "#selectShow")->click();
        $page->find("css", "#selectShow > option:nth-child(3)")->click();

        $mink->getSession()->resizeWindow(1000, 812);

        $allignedInformationJS = <<<JS

        var returnArray = [];

        returnArray.push($("body > h1").offset().top);                                                      // 0 The top offset of the title
        returnArray.push($("body > div > div:nth-child(1) > div.col-sm-8 > label").offset().top);           // 1 The top offset of the select a show label
        returnArray.push($("#selectShow").offset().top);                                                    // 2 The top offset of the select a show drop down
        returnArray.push($("#DataTables_Table_0").offset().top);                                            // 3 The top offset for the table
        returnArray.push($("#DataTables_Table_0 > tbody > tr:nth-child(1)").offset().top);   // 4 The top offset of the first member
        returnArray.push($("#DataTables_Table_0 > tbody > tr:nth-child(1)").offset().left);  // 5 The left offset of the first member
        returnArray.push($("#DataTables_Table_0 > tbody > tr:nth-child(2)").offset().top);   // 6 The top offset of the second member
        returnArray.push($("#DataTables_Table_0 > tbody > tr:nth-child(2)").offset().left);  // 7 The left offset of the second member
        return returnArray;
JS;

        $offsetArray = $mink->getSession()->evaluateScript($allignedInformationJS);

        $this->assertTrue($offsetArray[0] < $offsetArray[1]); // Applicant title is above the select a show label
        $this->assertTrue($offsetArray[1] < $offsetArray[2]); // The select a show label is above the select show drop down
        $this->assertTrue($offsetArray[2] < $offsetArray[3]); // The select show drop down is above the applicants table
        $this->assertTrue($offsetArray[3] < $offsetArray[4]); // The member list is below the top of the table
        $this->assertTrue($offsetArray[4] < $offsetArray[6]); // Member one is above member two
        $this->assertTrue($offsetArray[5] === $offsetArray[7]); // Member one and member two are left aligned

        // Test that the title is center-aligned
        $welcomeBanner = $page->find('css', 'body > h1');
        $this->assertContains("text-center", $welcomeBanner->getAttribute("class"), "showTitle");
    }

    /** 7
     * This will test for the Applicants Page on mobile:
     *
     * Title is center-aligned
     * Member are listed underneath displaying first name and last name on one line with email on a line underneath
     * First name and last name are separated by a space and are left-aligned
     * Emails are left aligned
     *
     * @author Kate Zawada and Taylor Beverly
     */
    public function testApplicantListPageDisplayMobile()
    {
        $mink = $this->mink;
        $page = $mink->getSession()->getPage();
        $session =  $mink->getSession();
        $session->wait(2000, '(0 === jQuery.active)');

        // Login
        $page->find("css","#_username")->setValue("bmember@member.com");
        $page->find("css","#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        $page->find("css","#navbarDropdown")->click();
        $page->find("css", "#auditionList")->click();

        // Select Hamlet to view Audition Interests
        $page->find("css", "#selectShow")->click();
        $page->find("css", "#selectShow > option:nth-child(3)")->click();

        $mink->getSession()->resizeWindow(575, 812);

        $allignedInformationJS = <<<JS

        var returnArray = [];

        returnArray.push($("body > h1").offset().top);                                                      // 0 The top offset of the title
        returnArray.push($("body > div > div:nth-child(1) > div.col-sm-8 > label").offset().top);           // 1 The top offset of the select a show label
        returnArray.push($("#selectShow").offset().top);                                                    // 2 The top offset of the select a show drop down
        returnArray.push($("#DataTables_Table_0").offset().top);                                            // 3 The top offset for the table
        returnArray.push($("#DataTables_Table_0 > tbody > tr:nth-child(1)").offset().top);   // 4 The top offset of the first member
        returnArray.push($("#DataTables_Table_0 > tbody > tr:nth-child(1)").offset().left);  // 5 The left offset of the first member
        returnArray.push($("#DataTables_Table_0 > tbody > tr:nth-child(2)").offset().top);   // 6 The top offset of the second member
        returnArray.push($("#DataTables_Table_0 > tbody > tr:nth-child(2)").offset().left);  // 7 The left offset of the second member
        return returnArray;
JS;

        $offsetArray = $mink->getSession()->evaluateScript($allignedInformationJS);

        $this->assertTrue($offsetArray[0] < $offsetArray[1]); // Applicant title is above the select a show label
        $this->assertTrue($offsetArray[1] < $offsetArray[2]); // The select a show label is above the select show drop down
        $this->assertTrue($offsetArray[2] < $offsetArray[3]); // The select show drop down is above the applicants table
        $this->assertTrue($offsetArray[3] < $offsetArray[4]); // The member list is below the top of the table
        $this->assertTrue($offsetArray[4] < $offsetArray[6]); // Member one is above member two
        $this->assertTrue($offsetArray[5] === $offsetArray[7]); // Member one and member two are left aligned

        // Test that the title is center-aligned
        $welcomeBanner = $page->find('css', 'body > h1');
        $this->assertContains("text-center", $welcomeBanner->getAttribute("class"), "showTitle");
    }

    /**
     * This test will ensure that only logged-in board members (and above) can view the audition list.
     *
     * @author Taylor Beverly and Kate Zawada
     */
    public function testThatNobodyExceptBoardMembersCanViewAuditionList()
    {
        $mink = $this->mink;
        $page = $mink->getSession()->getPage();

        //////
        //Public user tries to access the Audition List page
        //////
        $mink->getSession()->visit('http://localhost:8000/auditiondetails/board/applicants/');
        $driver = $mink->getSession()->getDriver();
        // Assert that the public user is redirected to the login page
        $url = $driver->getCurrentUrl();
        $this->assertEquals($url, 'http://localhost:8000/login');

        //////
        // Regular member tries to access the Audition List page
        //////
        $page->find("css","#_username")->setValue("member@member.com");
        $page->find("css","#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();
        $mink->getSession()->visit('http://localhost:8000/auditiondetails/board/applicants/');
        $driver = $mink->getSession()->getDriver();
        // Assert that the member is redirected to the home page
        $url = $driver->getCurrentUrl();
        $this->assertEquals($url, 'http://localhost:8000/');
        $page->find("css", "#logoutButton")->click();

        //////
        // Board member accesses the Audition List page
        //////
        $mink->getSession()->visit('http://localhost:8000/login');
        $page->find("css","#_username")->setValue("bmember@member.com");
        $page->find("css","#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();
        $mink->getSession()->visit('http://localhost:8000/auditiondetails/board/applicants/');
        $driver = $mink->getSession()->getDriver();
        // Assert that the board member can view the page
        $url = $driver->getCurrentUrl();
        $this->assertEquals($url, 'http://localhost:8000/auditiondetails/board/applicants/');
        $page->find("css", "#logoutButton")->click();

        //////
        // General Manager accesses the Audition List page
        //////
        $mink->getSession()->visit('http://localhost:8000/login');
        $page->find("css","#_username")->setValue("gmember@member.com");
        $page->find("css","#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();
        $mink->getSession()->visit('http://localhost:8000/auditiondetails/board/applicants/');
        $driver = $mink->getSession()->getDriver();
        // Assert that the board member can view the page
        $url = $driver->getCurrentUrl();
        $this->assertEquals($url, 'http://localhost:8000/auditiondetails/board/applicants/');
    }

    /*
     * This function will ensure that the proper messages are displayed on the screen when a user selects a show
     * that has not had anybody click that they are interested in audition information.
     *
     * @author Taylor Beverly
     */
    public function testProperFormattingIfNoAuditionInterestsForShow()
    {
        $mink = $this->mink;
        $page = $mink->getSession()->getPage();
        $session =  $mink->getSession();
        $session->wait(2000, '(0 === jQuery.active)');

        // Login
        $page->find("css","#_username")->setValue("bmember@member.com");
        $page->find("css","#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        $page->find("css","#navbarDropdown")->click();
        $page->find("css", "#auditionList")->click();

        //Hamlet doesn't have any audition interest set up in our fixture so we will click that
        $page->find("css", "#selectShow")->click();
        $page->find("css", "#selectShow > option:nth-child(2)")->click();

        //Find the text indicating that nothing is available. This will only ever show if no audition interests for that show exist.
        $this->assertContains($page->find("css","#DataTables_Table_0 > tbody > tr > td")->getText(), "No audition requests for this show.");
    }

    /*
     * This test will ensure that the search function returns the correct record, and only that record.
     *
     * @author Taylor, Kate
     */
    public function testThatSearchFunctionWorks()
    {
        $mink = $this->mink;
        $page = $mink->getSession()->getPage();
        $session =  $mink->getSession();
        $session->wait(2000, '(0 === jQuery.active)');

        // Login
        $page->find("css","#_username")->setValue("bmember@member.com");
        $page->find("css","#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        $page->find("css","#navbarDropdown")->click();
        $page->find("css", "#auditionList")->click();

        //Hamlet doesn't have any audition interest set up in our fixture so we will click that
        $page->find("css", "#selectShow")->click();
        $page->find("css", "#selectShow > option:nth-child(3)")->click();

        $page->find("css", "#DataTables_Table_0_filter > label > input[type=\"search\"]")->setValue("aaa");

        $this->assertContains("aaa@aaa.com", $page->getHtml());
        $this->assertNotContains("#DataTables_Table_0 > tbody > tr:nth-child(2)",$page->getHtml());
    }

    public function testPagination()
    {
        $mink = $this->mink;
        $page = $mink->getSession()->getPage();
        $session =  $mink->getSession();
        $session->wait(2000, '(0 === jQuery.active)');

        // Login
        $page->find("css","#_username")->setValue("bmember@member.com");
        $page->find("css","#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        $page->find("css","#navbarDropdown")->click();
        $page->find("css", "#auditionList")->click();

        // Select Hamlet to view Audition Interests
        $page->find("css", "#selectShow")->click();
        $page->find("css", "#selectShow > option:nth-child(3)")->click();

        //Start with 10 records (10 is default, no clicky needed)
        $this->assertTrue($page->find("css","#DataTables_Table_0 > tbody > tr:nth-child(10)")->isVisible());
        $this->assertTrue($page->find("css","#DataTables_Table_0 > tbody > tr:nth-child(11)") == NULL);

        //Switch to 25 and check
        $page->find("css","#DataTables_Table_0_length > label > select")->click();
        $page->find("css", "#DataTables_Table_0_length > label > select > option:nth-child(2)")->click();
        $this->assertTrue($page->find("css","#DataTables_Table_0 > tbody > tr:nth-child(25)")->isVisible());
        $this->assertTrue($page->find("css","#DataTables_Table_0 > tbody > tr:nth-child(26)") == NULL);

        //Switch to 50 and check
        $page->find("css","#DataTables_Table_0_length > label > select")->click();
        $page->find("css", "#DataTables_Table_0_length > label > select > option:nth-child(3)")->click();
        $this->assertTrue($page->find("css","#DataTables_Table_0 > tbody > tr:nth-child(50)")->isVisible());
        $this->assertTrue($page->find("css","#DataTables_Table_0 > tbody > tr:nth-child(51)") == NULL);

        //Switch to 100 and check
        $page->find("css","#DataTables_Table_0_length > label > select")->click();
        $page->find("css", "#DataTables_Table_0_length > label > select > option:nth-child(4)")->click();
        $this->assertTrue($page->find("css","#DataTables_Table_0 > tbody > tr:nth-child(100)")->isVisible());
        $this->assertTrue($page->find("css","#DataTables_Table_0 > tbody > tr:nth-child(101)") == NULL);

        // Navigate to the second page of members and ensure there are 51 elements on the page (151 members total)
        $page->find("css", "#DataTables_Table_0_paginate > span > a:nth-child(2)")->click();
        $this->assertTrue($page->find("css","#DataTables_Table_0 > tbody > tr:nth-child(52)")->isVisible());
        $this->assertTrue($page->find("css","#DataTables_Table_0 > tbody > tr:nth-child(53)") == NULL);


    }


    /**
     * This test will ensure that when a board member selects a show from the dropdown box, only audition requests for that show appear.
     *
     * @author Taylor B, Kate Z
     */
    public function testThatOnlyAuditionsForSelectedShowAppear()
    {

        $mink = $this->mink;
        $page = $mink->getSession()->getPage();
        $session =  $mink->getSession();
        $session->wait(2000, '(0 === jQuery.active)');

        // Login
        $page->find("css","#_username")->setValue("bmember@member.com");
        $page->find("css","#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        $page->find("css","#navbarDropdown")->click();
        $page->find("css", "#auditionList")->click();


        //Switch to Sweeney Todd
        $page->find("css", "#selectShow")->click();
        $page->find("css", "#selectShow > option:nth-child(4)")->click();
        //Switch to 100 and check
        $page->find("css","#DataTables_Table_0_length > label > select")->click();
        $page->find("css", "#DataTables_Table_0_length > label > select > option:nth-child(4)")->click();
        $this->assertTrue($page->find("css","#DataTables_Table_0 > tbody > tr:nth-child(50)")->isVisible());
        $this->assertTrue($page->find("css","#DataTables_Table_0 > tbody > tr:nth-child(51)") == NULL);

        //Switch to Mary Poppins
        $page->find("css", "#selectShow")->click();
        $page->find("css", "#selectShow > option:nth-child(4)")->click();
        //Switch to 100 and check
        $page->find("css","#DataTables_Table_0_length > label > select")->click();
        $page->find("css", "#DataTables_Table_0_length > label > select > option:nth-child(4)")->click();
        $this->assertTrue($page->find("css","#DataTables_Table_0 > tbody > tr:nth-child(50)")->isVisible());
        $this->assertTrue($page->find("css","#DataTables_Table_0 > tbody > tr:nth-child(51)") == NULL);

        //Switch to A Christmas Story
        $page->find("css", "#selectShow")->click();
        $page->find("css", "#selectShow > option:nth-child(5)")->click();
        //Switch to 100 and check
        $page->find("css","#DataTables_Table_0_length > label > select")->click();
        $page->find("css", "#DataTables_Table_0_length > label > select > option:nth-child(4)")->click();
        $this->assertTrue($page->find("css","#DataTables_Table_0 > tbody > tr:nth-child(50)")->isVisible());
        $this->assertTrue($page->find("css","#DataTables_Table_0 > tbody > tr:nth-child(51)") == NULL);
    }

    public function testAlphabeticalSort()
    {
        $mink = $this->mink;
        $page = $mink->getSession()->getPage();
        $session =  $mink->getSession();
        $session->wait(2000, '(0 === jQuery.active)');

        // Login
        $page->find("css","#_username")->setValue("bmember@member.com");
        $page->find("css","#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        $page->find("css","#navbarDropdown")->click();
        $page->find("css", "#auditionList")->click();


        //Switch to Hamlet
        $page->find("css", "#selectShow")->click();
        $page->find("css", "#selectShow > option:nth-child(3)")->click();

        //Check to see that the content of the first table row email is is "aaa@aaa.com" (sorted by default)
        $this->assertEquals("aaa@aaa.com",$page->find("css","#DataTables_Table_0 > tbody > tr:nth-child(1) > td:nth-child(2)")->getText());

        //Get to the last paginated page

        $page->find("css","#DataTables_Table_0_paginate > span > a:nth-child(7)")->click();

        //Check to see that the last email value is "zzz@zzz.com"
        $this->assertEquals("zzz@zzz.com",$page->find("css","#DataTables_Table_0 > tbody > tr.even > td:nth-child(2)")->getText());

        //Switch to descending sort and do the same as above, but zzz first and aaa last.
        $page->find("css","#DataTables_Table_0 > thead > tr > th.sorting_asc")->click();
        $this->assertEquals("zzz@zzz.com",$page->find("css","#DataTables_Table_0 > tbody > tr:nth-child(1) > td:nth-child(2)")->getText());
        $page->find("css","#DataTables_Table_0_paginate > span > a:nth-child(7)")->click();
        $this->assertEquals("aaa@aaa.com",$page->find("css","#DataTables_Table_0 > tbody > tr.even > td:nth-child(2)")->getText());


    }


}
