<?php
namespace App\Tests;

use app\Entity\SSPShow;

use DMore\ChromeDriver\ChromeDriver;
use Behat\Mink\Mink;
use Behat\Mink\Session;
use Liip\FunctionalTestBundle\Test\WebTestCase;

/**
 * MINK tests for desktop and mobile on show_edit
 * @author Christopher and Nathan
 * @package App\Tests
 *
 *
 */
class show_edit extends WebTestCase
{
    /**
     * @throws \Behat\Mink\Exception\DriverException
     * @throws \Behat\Mink\Exception\UnsupportedDriverActionException
     */
    public function setUp()
    {
        $this->loadFixtures(array(
            'App\DataFixtures\MemberLoginTestFixture',
            'App\DataFixtures\AllShowsFixtures'
        ));
    }

    /**
     * This will test that a general manager cannot edit a show with a blank name or a name greater than 50 characters.
     * It will test to ensure that appropriate client-side validation error text appears when attempting to do so.
     * @author Chris and Nathan
     *
     * @throws
     */
    public function testEditShowNameBlankOrOverFiftyChars()
    {
        //LOGIN AS A GENERAL MANAGER
        $mink = new Mink(array(
            'ShowClientSideTest' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ShowClientSideTest');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("gmember@member.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        $page->find("css", "a#editShows")->click();
        $page->find("css", "button#show4")->click();

        //enter a name that is too long
        $page->find("css", "#show_name")->setValue(str_repeat('a', 55));

        // Click away from the name field
        $page->find("css", "#show_ticketLink")->click();

        // Check to see if any errors are visible
        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("Show name must be between 1-50 characters", $errorText);

        //enter a blank name
        $page->find("css", "#show_name")->setValue("");

        // Click away from the name field
        $page->find("css", "#show_ticketLink")->click();

        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("Show name is required", $errorText);
    }

    /**
     * This will test that a general manager cannot edit a show with a budget being blank, invalid type, or less than 1
     * It will test to ensure that appropriate client-side validation error text appears when attempting to do so.
     * @author Chris and Nathan
     *
     * @throws
     */
    public function testEditShowBudgetBlankBadTypeLessThanOne()
    {
        //LOGIN AS A GENERAL MANAGER
        $mink = new Mink(array(
            'ShowClientSideTest' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ShowClientSideTest');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("gmember@member.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        $page->find("css", "a#editShows")->click();
        $page->find("css", "button#show4")->click();

        $page->find("css", "#show_budget")->setValue("");

        $page->find("css", "#show_ticketLink")->click();

        // Check to see if any errors are visible
        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("Budget is required", $errorText);

        $page->find("css", "#show_budget")->setValue("jhijy");

        $page->find("css", "#show_ticketLink")->click();

        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("Budget must be a number", $errorText);

        $page->find("css", "#show_budget")->setValue(0.5);

        $page->find("css", "#show_ticketLink")->click();

        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("Budget must be at least 1 dollar", $errorText);
    }

    /**
     * This will test that a general manager cannot edit a show with a ticket price being blank, invalid type, or less than 1
     * It will test to ensure that appropriate client-side validation error text appears when attempting to do so.
     * @author Chris and Nathan
     *
     * @throws
     */
    public function testEditShowTicketPriceBlankBadTypeLessThanOne()
    {
        //LOGIN AS A GENERAL MANAGER
        $mink = new Mink(array(
            'ShowClientSideTest' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ShowClientSideTest');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("gmember@member.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        $page->find("css", "a#editShows")->click();
        $page->find("css", "button#show4")->click();

        $page->find("css", "#show_ticketPrice")->setValue("");

        $page->find("css", "#show_ticketLink")->click();

        // Check to see if any errors are visible
        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("Ticket price is required", $errorText);

        $page->find("css", "#show_ticketPrice")->setValue("jhijy");

        $page->find("css", "#show_ticketLink")->click();

        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("Ticket price must be a number", $errorText);

        $page->find("css", "#show_ticketPrice")->setValue(0.5);

        $page->find("css", "#show_ticketLink")->click();

        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("Ticket price must be at least 1 dollar", $errorText);
    }

    /**
     * This will test that a general manager cannot edit a show without a synopsis
     * It will test to ensure that appropriate client-side validation error text appears when attempting to do so.
     * @author Chris and Nathan
     *
     * @throws
     */
    public function testEditShowSynopsisBlank()
    {
        //LOGIN AS A GENERAL MANAGER
        $mink = new Mink(array(
            'ShowClientSideTest' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ShowClientSideTest');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("gmember@member.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        $page->find("css", "a#editShows")->click();
        $page->find("css", "button#show4")->click();

        //enter a name that is too long
        $page->find("css", "#show_synopsis")->setValue("");

        // Click away from the name field
        $page->find("css", "#show_ticketLink")->click();

        // Check to see if any errors are visible
        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("Synopsis is required", $errorText);
    }

    /**
     * This will test that a general manager cannot edit a show with a ticket link over 100 characters or not a url
     * It will test to ensure that appropriate client-side validation error text appears when attempting to do so.
     * @author Chris and Nathan
     *
     * @throws
     */
    public function testCreateShowTicketLinkOverHundredCharactersOrNotURL()
    {
        //LOGIN AS A GENERAL MANAGER
        $mink = new Mink(array(
            'ShowClientSideTest' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ShowClientSideTest');
        $mink->getSession()->visit('http://localhost:8000/login');
        $driver = $mink->getSession()->getDriver();
        $page = $mink->getSession()->getPage();
        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("gmember@member.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");

        $page->find("css", "#login")->click();

        $page->find("css", "a#editShows")->click();
        $page->find("css", "button#show4")->click();

        $page->find("css", "#show_ticketLink")->setValue(str_repeat('a', 102));

        //oddly enough, when setting the value for the ticket link, you must set the value of another field for the validation to take effect; this does not happen outside of the test
        $page->find("css", "#show_name")->setValue("some value");

        // Check to see if any errors are visible
        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("The ticket link must be no longer than 100 characters", $errorText);

        $page->find("css", "#show_ticketLink")->setValue("jhijy");

        $page->find("css", "#show_synopsis")->setValue("some value");

        $isVisible = $page->find("css", ".alert.alert-danger")->isVisible();

        $this->assertTrue($isVisible);

        $errorText = $page->find("css", ".alert.alert-danger")->getText();

        $this->assertEquals("Ticket link must be a website url. For example https://www.saskatoonSummerPlayers.ca/", $errorText);
    }

    /**
     * Christopher and Nathan
     * This function will test that:
     * - the card is centered
     * - inputs will be of width 540 when the browser is 1920x1080
     * - labels are displayed to the left of the inputs with their text being right aligned
     * - card header is left aligned
     * - button will be left aligned to input at the bottom of the page
     */
    //start chrome --disable-gpu --headless --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222
    public function testDesktopAlignment()
    {
        $mink = new Mink(array(
            'EditShow' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('EditShow');
        $mink->getSession()->visit('http://localhost:8000/show');

        $session = $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $page = $mink->getSession()->getPage();

        $mink->getSession()->resizeWindow(1000, 1040);

        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("gmember@member.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        $page->find("css", "a#editShows")->click();
        $page->find("css", "button#show4")->click();

        $xyInputCoor = <<<JS
        $('input.form-control').map(function(index, element) {
          return {left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;

        // Store input coordinates in arrays
        $inputXY = $mink->getSession()->evaluateScript($xyInputCoor);

        $xyCardCoor = <<<JS
        $('div.card').map(function(index, element) {
          return {left: $(element).offset().left, right: $(element).outerWidth() + $(element).offset().left, text: $(element).text(),
                    class: $(element).attr("class"), childClass:$(element).children().eq(0).attr("class")};
        }).get();
JS;
        // Store Button elements in arrays
        $cardXY = $mink->getSession()->evaluateScript($xyCardCoor);

        // Get top input element to compare others to
        $xInputVal = $inputXY[0]["left"];
        $inputWidth = 429;

        for ($i = 0; $i < count($inputXY); $i++) {
            // Test that all inputs are left aligned
            $this->assertEquals($xInputVal, $inputXY[$i]["left"]);

            // Test that each input is the correct width
            $this->assertEquals($inputWidth, $inputXY[$i]["width"]);
        }

        // Assert that the card title is "Update Profile" and is left aligned
        $headerText = $page->find("css", "h2.card-header")->getText();
        $this->assertEquals("Edit Mary Poppins", $headerText);

        $this->assertContains("card-header text-sm-left text-xs-left", $cardXY[0]["childClass"]);
    }

    /**
     * Christopher and Nathan
     * This function will test that:
     * - the card is centered
     * - inputs will be of width 545 when the browser is 575 x 812
     * - labels are displayed above the inputs with their text being left aligned
     * - card header is left aligned
     * - button will be the same width as the input at the bottom of the page
     */
    //start chrome --disable-gpu --headless --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222
    public function testMobileAlignment()
    {

        $mink = new Mink(array(
            'EditShow' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('EditShow');
        $mink->getSession()->visit('http://localhost:8000/show');

        $session = $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $page = $mink->getSession()->getPage();

        $mink->getSession()->resizeWindow(575, 812);

        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("gmember@member.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        $page->find("css", "a#editShows")->click();
        $page->find("css", "button#show4")->click();

        $xyInputCoor = <<<JS
        $('input.form-control').map(function(index, element) {
          return {left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;

        // Store input coordinates in arrays
        $inputXY = $mink->getSession()->evaluateScript($xyInputCoor);

        $xyCardCoor = <<<JS
        $('div.card').map(function(index, element) {
          return {left: $(element).offset().left, right: $(element).outerWidth() + $(element).offset().left, text: $(element).text(),
                    class: $(element).attr("class"), childClass:$(element).children().eq(0).attr("class")};
        }).get();
JS;
        // Store Button elements in arrays
        $cardXY = $mink->getSession()->evaluateScript($xyCardCoor);

        // Get top input element to compare others to
        $xInputVal = $inputXY[0]["left"];
        $inputWidth = 486;

        for ($i = 0; $i < count($inputXY); $i++) {
            // Test that all inputs are left aligned
            $this->assertEquals($xInputVal, $inputXY[$i]["left"]);

            // Test that each input is the correct width
            $this->assertEquals($inputWidth, $inputXY[$i]["width"]);
        }

        // Assert that the card title is "Update Profile" and is left aligned
        $headerText = $page->find("css", "h2.card-header")->getText();
        $this->assertEquals("Edit Mary Poppins", $headerText);

        $this->assertContains("card-header text-sm-left text-xs-left", $cardXY[0]["childClass"]);
    }
}