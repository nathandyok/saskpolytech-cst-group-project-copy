<?php
namespace App\Tests;

use app\Entity\AuditionDetails;

use DMore\ChromeDriver\ChromeDriver;
use Behat\Mink\Mink;
use Behat\Mink\Session;
use Liip\FunctionalTestBundle\Test\WebTestCase;

/**
 * MINK tests for desktop and mobile on edit audition details
 * @author Christopher and Nathan
 * @package App\Tests
 *
 *
 */
class show_edit_index extends WebTestCase
{
    /**
     * @throws \Behat\Mink\Exception\DriverException
     * @throws \Behat\Mink\Exception\UnsupportedDriverActionException
     */
    public function setUp()
    {
        $this->loadFixtures(array(
            'App\DataFixtures\MemberLoginTestFixture',
            'App\DataFixtures\AllShowsFixtures'
        ));
    }

    /**
     * Christopher and Nathan
     * This function will test that:
     * - all h3 tags for name of shwos are all properly aligned and off center to the left on the page
     */
    //start chrome --disable-gpu --headless --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222
    public function testDesktopAlignment()
    {
        $mink = new Mink(array(
            'ShowEditIndex' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ShowEditIndex');
        $mink->getSession()->visit('http://localhost:8000');

        $session = $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $driver = $mink->getSession()->getDriver();

        $page = $mink->getSession()->getPage();

        $mink->getSession()->resizeWindow(1000, 1040);

        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("gmember@member.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        $page->find("css", "a#editShows")->click();

        $xyH3Coor = <<<JS
        $('h3').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;

        // Store input coordinates in arrays
        $h3XY = $mink->getSession()->evaluateScript($xyH3Coor);

        // Get top input element to compare others to
        $xInputVal = $h3XY[0]["left"];
        $inputWidth = 206;

        for ($i = 0; $i < count($h3XY); $i++) {
            // Test that all inputs are left aligned
            $this->assertEquals($xInputVal, $h3XY[$i]["left"]);

            // Test that each input is the correct width
            $this->assertEquals($inputWidth, $h3XY[$i]["width"]);
        }

        // Assert that the button is below the last input tag
        $lastInputTop = end($h3XY)["top"];

        // Assert that the card title is "Update Profile" and is left aligned
        $headerText = $page->find("css", ".currentStatus")->getText();
        $this->assertEquals("Editable Shows", $headerText);
    }

    /**
     * Christopher and Nathan
     * This function will test that:
     * - all h3 tags for name of shows are all properly aligned and off center to the left on the page on mobile
     */
    //start chrome --disable-gpu --headless --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222
    public function testMobileAlignment()
    {

        $mink = new Mink(array(
            'ShowEditIndex' => new Session(new ChromeDriver('http://localhost:9222', null, 'http://www.google.com'))
        ));
        $mink->setDefaultSessionName('ShowEditIndex');
        $mink->getSession()->visit('http://localhost:8000');

        $session = $mink->getSession();

        $session->wait(60000, '(0 === jQuery.active)');

        $driver = $mink->getSession()->getDriver();

        $page = $mink->getSession()->getPage();

        $mink->getSession()->resizeWindow(575, 812);

        $page->find("css", "#loginButton")->click();
        $page->find("css", "#_username")->setValue("gmember@member.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        $page->find("css", "a#editShows")->click();

        $xyH3Coor = <<<JS
        $('h3').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;

        // Store input coordinates in arrays
        $h3XY = $mink->getSession()->evaluateScript($xyH3Coor);

        // Get top input element to compare others to
        $xInputVal = $h3XY[0]["left"];
        $inputWidth = 512;

        for ($i = 0; $i < count($h3XY); $i++) {
            // Test that all inputs are left aligned
            $this->assertEquals($xInputVal, $h3XY[$i]["left"]);

            // Test that each input is the correct width
            $this->assertEquals($inputWidth, $h3XY[$i]["width"]);
        }

        // Assert that the button is below the last input tag
        $lastInputTop = end($h3XY)["top"];

        // Assert that the card title is "Update Profile" and is left aligned
        $headerText = $page->find("css", ".currentStatus")->getText();
        $this->assertEquals("Editable Shows", $headerText);
    }
}