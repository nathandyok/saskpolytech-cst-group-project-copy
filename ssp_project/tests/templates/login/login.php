<?php

namespace App\Tests\templates\login;

use DMore\ChromeDriver\ChromeDriver;
use Behat\Mink\Mink;
use Behat\Mink\Session;
use Liip\FunctionalTestBundle\Test\WebTestCase;

class login extends WebTestCase
{
    private $minkBaseUrl;
    private $minkSession;
    private $fixtures;
    /**
     * Setup for our Mink tests
     * @before
     */
    public function setupMinkSession()
    {
        $this->minkBaseUrl = isset($_SERVER['MINK_BASE_URL'])
            ?  $_SERVER['MINK_BASE_URL']
            : 'http://localhost:8000';

        $driver = new ChromeDriver('http://localhost:9222', null, $this->minkBaseUrl);
        $this->minkSession= new Session($driver);
        $this->minkSession->start();
        $this->fixtures = $this->loadFixtures(array(
            'App\DataFixtures\MemberLoginTestFixture',
            'App\DataFixtures\AddMembersFixtures'
        ));
    }

    /**
     * Helper function to return the page
     * @return mixed
     */
    public function getCurrentPage()
    {
        return $this->minkSession->getPage();
    }

    /**
     * Helper function to return the driver
     * @return mixed
     */
    public function getCurrentDriver()
    {
        return $this->minkSession->getDriver();
    }

    /**
     * Helper function to return the page content
     * @return mixed
     */
    public function getCurrentPageContent()
    {
        return $this->getCurrentPage()->getContent();
    }

    /**
     * Helper function to visit a url
     * @param $url
     */
    public function visit($url)
    {
        $this->minkSession->visit($this->minkBaseUrl . $url);
    }



    /**
     * This test will make sure the login page is displayed properly on desktop
     *  - the card is centered
     * - inputs will be of width 189 when the browser is 1920x1080
     * - labels are displayed to the left of the inputs with their text being right aligned
     * - card header is left aligned
     * - button will be left aligned to input at the bottom of the page
     * start chrome --disable-gpu --headless --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222
     * Cory Nagy and MacKenzie Wilson
     * April, 2019
     */
    public function testLoginPageDisplaysProperlyOnDesktop()
    {
        $this->visit('/login');
        $page = $this->getCurrentPage();
        $driver = $this->getCurrentDriver();

        $this->minkSession->resizeWindow(1000, 1040);

        $xyInputCoor = <<<JS
        $('.form-control').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;

        // Store input coordinates in arrays
        $inputXY = $this->minkSession->evaluateScript($xyInputCoor);

        $xyButtonCoor = <<<JS
        $('button').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;
        // Store Button elements in arrays
        $buttonXY = $this->minkSession->evaluateScript($xyButtonCoor);


        $xyLabelCoor = <<<JS
        $('label').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth(), class: $(element).attr("class")};
        }).get();
JS;

        // Store Label elements in arrays
        $labelXY = $this->minkSession->evaluateScript($xyLabelCoor);


        $xyCardCoor = <<<JS
        $('div.card').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, right: $(element).outerWidth() + $(element).offset().left, text: $(element).text(),
                    class: $(element).attr("class"), childClass:$(element).children().eq(0).attr("class")};
        }).get();
JS;
        // Store Button elements in arrays
        $cardXY = $this->minkSession->evaluateScript($xyCardCoor);

        $xyLinksCoor = <<<JS
        $('.links').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth(), class: $(element).attr("class")};
        }).get();
JS;
        // Store link elements in arrays
        $linkXY = $this->minkSession->evaluateScript($xyLinksCoor);


        // Get top input element to compare others to
        $xInputVal = $inputXY[0]["left"];
        $xLabelVal = $labelXY[0]["left"];

        // input width will be different since it'll probably be smaller
        $inputWidth = 189;

        for ($i = 0; $i < count($inputXY); $i++)
        {
            // Test that all inputs are left aligned
            $this->assertEquals($xInputVal, $inputXY[$i]["left"]);

            // Each label is in the same row as its input box
            $this->assertEquals($inputXY[$i]["top"], $labelXY[$i]["top"]);

            // Test that each input is the correct width
            $this->assertEquals($inputWidth, $inputXY[$i]["width"]);

            // Assert that the text is right aligned by checking the class attribute
            $this->assertContains("text-sm-right", $labelXY[$i]["class"]);
        }

        // Assert that the button is left aligned with inputs
        $this->assertEquals($xInputVal, $buttonXY[1]["left"]);

        // Assert that the button is below the last input tag
        $lastInputTop = end($inputXY)["top"];
        $buttonTop = $buttonXY[1]["top"];

        $this->assertLessThan($buttonTop, $lastInputTop);

        //Assert that sign up link is below button
        $signUpLink = $linkXY[0]["top"];
        $this->assertLessThan($signUpLink, $buttonTop);

        // Assert the "Forgot password link" is below "Sign up" link
        $forgotPWLink = $linkXY[1]["top"];
        $this->assertLessThan($forgotPWLink, $signUpLink);

        // Assert both links are center aligned
        for ($i = 0; $i < count($linkXY); $i++) {
            $this->assertContains("justify-content-center", $linkXY[$i]["class"]);
        }

        // Assert that the card is in the center of the page
        $this->assertEquals($cardXY[0]["left"], 1000 - $cardXY[0]["right"]);

        // Assert that the card title is "Log In" and is left aligned
        $headerText = $page->find("css", "h2.card-header")->getText();
        $this->assertEquals("Log In", $headerText);

        $this->assertContains("card-header text-sm-left text-xs-left", $cardXY[0]["childClass"]);

    }

    /**
     * This test will make sure the login page is displayed properly on mobile
     *  - the card is centered
     * - inputs will be of width 503 when the browser is 1920x1080
     * - labels are displayed to the left of the inputs with their text being right aligned
     * - card header is left aligned
     * - button will be left aligned to input at the bottom of the page
     * start chrome --disable-gpu --headless --remote-debugging-address=0.0.0.0 --remote-debugging-port=9222
     * Cory Nagy and MacKenzie Wilson
     * April, 2019
     */
    public function testLoginPageDisplaysProperlyOnMobile()
    {
        $this->visit('/login');
        $page = $this->getCurrentPage();
        $driver = $this->getCurrentDriver();

        $this->minkSession->resizeWindow(575, 812);

        $xyInputCoor = <<<JS
        $('input.form-control').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;

        // Store input coordinates in arrays
        $inputXY = $this->minkSession->evaluateScript($xyInputCoor);

        $xyButtonCoor = <<<JS
        $('button').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;
        // Store Button elements in arrays
        $buttonXY = $this->minkSession->evaluateScript($xyButtonCoor);


        $xyLabelCoor = <<<JS
        $('label').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth(), class: $(element).attr("class")};
        }).get();
JS;

        // Store Label elements in arrays
        $labelXY = $this->minkSession->evaluateScript($xyLabelCoor);


        $xyCardCoor = <<<JS
        $('div.card').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, right: $(element).outerWidth() + $(element).offset().left, text: $(element).text(),
                    class: $(element).attr("class"), childClass:$(element).children().eq(0).attr("class")};
        }).get();
JS;
        // Store Button elements in arrays
        $cardXY = $this->minkSession->evaluateScript($xyCardCoor);

        $xyLinksCoor = <<<JS
        $('.links').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth(),class: $(element).attr("class")};
        }).get();
JS;
        // Store link elements in arrays
        $linkXY = $this->minkSession->evaluateScript($xyLinksCoor);


        // Get top input element to compare others to
        // Get top input element to compare others to
        $xInputVal = $inputXY[0]["left"];
        $xLabelVal = $labelXY[0]["left"];
        $inputWidth = 486;
        $yValLabel = $labelXY[0]["top"];


        for ($i = 0; $i < count($inputXY); $i++)
        {
            $this->assertEquals($yValLabel, $labelXY[$i]["top"]);

            // The height of the label
            $yValLabel += 51;

            // The height of the input and padding
            $yValLabel += 54;

            // Test that all inputs are left aligned
            $this->assertEquals($xInputVal, $inputXY[$i]["left"]);

            // Test that all labels are left aligned
            $this->assertEquals($xLabelVal, $labelXY[$i]["left"]);

            // Test that each input is the correct width
            $this->assertEquals($inputWidth, $inputXY[$i]["width"]);

            // Assert that the text is left aligned by checking the class attribute
            $this->assertContains("text-sm-right text-xs-left", $labelXY[$i]["class"]);
        }

        // Assert that the button is left aligned with inputs
        $this->assertEquals($xInputVal, $buttonXY[1]["left"]);

        // Assert that the button is below the last input tag
        $lastInputTop = end($inputXY)["top"];
        $buttonTop = $buttonXY[1]["top"];

        $this->assertLessThan($buttonTop, $lastInputTop);

        //Assert that sign up link is below button
        $signUpLink = $linkXY[0]["top"];
        $this->assertLessThan($signUpLink, $buttonTop);

        // Assert the "Forgot password link" is below "Sign up" link
        $forgotPWLink = $linkXY[1]["top"];
        $this->assertLessThan($forgotPWLink, $signUpLink);

        // Assert both links are center aligned
        for ($i = 0; $i < count($linkXY); $i++) {
            $this->assertContains("justify-content-center", $linkXY[$i]["class"]);
        }

        // Assert that the card is in the center of the page
        // Adding 17 because of padding
        $this->assertEquals($cardXY[0]["left"] + 17, 575 - $cardXY[0]["right"]);

        // Assert that the card title is "Log In" and is left aligned
        $headerText = $page->find("css", "h2.card-header")->getText();
        $this->assertEquals("Log In", $headerText);

        $this->assertContains("card-header text-sm-left text-xs-left", $cardXY[0]["childClass"]);
    }

    /**
     * This test will verify that the error message will appear below the login button on desktop
     * This function will check that error messages:
     * - contain a class of error_message
     * - are left aligned
     * Cory Nagy and MacKenzie Wilson
     * April, 2019
     */
    public function testErrorMessageDisplaysBelowButtonProperlyOnDesktop()
    {
        $this->visit('/login');
        $page = $this->getCurrentPage();
        $driver = $this->getCurrentDriver();

        $this->minkSession->resizeWindow(1000, 1040);

        $page->find("css", "#_username")->setValue("bmember@member.com");
        $page->find("css", "#_password")->setValue("Wr0ngP@ssw0rd");
        $page->find("css", "#login")->click();


        $xyButtonCoor = <<<JS
        $('button').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;
        // Store Button elements in arrays
        $buttonXY = $this->minkSession->evaluateScript($xyButtonCoor);

        $xyErrorCoor = <<<JS
        $('#invalid_credentials').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth(),  class: $(element).attr("class")};
        }).get();
JS;
        // Store error elements in arrays
        $errorXY = $this->minkSession->evaluateScript($xyErrorCoor);

        $xyCardFooterCoor = <<<JS
        $('.card-footer').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth(), class: $(element).attr("class")};
        }).get();
JS;

        // Store Label elements in arrays
        $cardFooterXY = $this->minkSession->evaluateScript($xyCardFooterCoor);

        // Get error message x value
        $errorXVal = $errorXY[0]["left"];

        // Get the top of the button and add the height of the
        // button and padding to get us to the top of the error
        $topOfError = $buttonXY[1]["top"] + 62 + 3.59375;

        // Assert we're at the top of the error message
        $this->assertEquals($errorXY[0]["top"], $topOfError);

        // Assert error message and button are left aligned
        $this->assertEquals($errorXY[0]["left"], $cardFooterXY[0]["left"]);

    }

    /**
     * This test will verify that the error message will appear below the login button on mobile and
     * are left aligned.
     * Cory Nagy and MacKenzie Wilson
     * April, 2019
     */
    public function testErrorMessageDisplaysBelowButtonProperlyOnMobile()
    {
        $this->visit('/login');
        $page = $this->getCurrentPage();
        $driver = $this->getCurrentDriver();

        $this->minkSession->resizeWindow(575, 812);

        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("Wr0ngP@ss");
        $page->find("css", "#login")->click();

        $xyButtonCoor = <<<JS
        $('button').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;
        // Store Button elements in arrays
        $buttonXY = $this->minkSession->evaluateScript($xyButtonCoor);

        $xyErrorCoor = <<<JS
        $('.alert.alert-danger').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth(),  class: $(element).attr("class")};
        }).get();
JS;
        // Store error elements in arrays
        $errorXY = $this->minkSession->evaluateScript($xyErrorCoor);

        // Get error message x value
        $errorXVal = $errorXY[0]["left"];

        // Get the top of the button and add the height of the
        // button and padding to get us to the top of the error
        $topOfError = $buttonXY[1]["top"] + 62 + 3.59375;

        // Assert we're at the top of the error message
        $this->assertEquals($errorXY[0]["top"], $topOfError);

        // Assert error message and button are left aligned
        $this->assertEquals($errorXY[0]["left"], $buttonXY[1]["left"]);

    }

    /**
     * This function will test that the client side error messages will appear properly
     * on desktop mode. Message should appear below the input fields
     * Cory Nagy and MacKenzie Wilson
     * April, 2019
     */
    public function testUsernameAndPasswordErrorMessageDisplayedProperlyOnDesktop()
    {
        $this->visit('/login');
        $page = $this->getCurrentPage();
        $driver = $this->getCurrentDriver();

        $this->minkSession->resizeWindow(1000, 1040);

        $page->find("css", "#_username")->setValue(str_repeat("Ab0", 51));
        $page->find("css", "#_password")->setValue(str_repeat("Ab0", 101));
        $page->find("css", "#login")->click();

        $xyInputCoor = <<<JS
        $('.form-control').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;

        // Store input coordinates in arrays
        $inputXY = $this->minkSession->evaluateScript($xyInputCoor);

        $xyErrorCoor = <<<JS
        $('div.alert.alert-danger').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth(),  class: $(element).attr("class")};
        }).get();
JS;
        // Store error elements in arrays
        $errorXY = $this->minkSession->evaluateScript($xyErrorCoor);

        // Get top input element to compare others to
        $errorXVal = $errorXY[0]["left"];

        $yVal = $inputXY[0]["top"];

        for ($i = 0; $i < count($errorXY); $i++) {
            // Assert that we're at the top of the input field
            $this->assertEquals($inputXY[$i]["top"], $yVal);

            // Add 38 to y-val to point us to the top of the error message
            $yVal += 54;

            $this->assertEquals($yVal, $errorXY[$i]["top"]);

            // Add padding height and height of error message
            $yVal += 82.1875;

            // Test that all errors are left aligned
            $this->assertEquals($errorXVal, $errorXY[$i]["left"]);

            // Test that error field contain a class of error_message
            $this->assertContains("alert alert-danger", $errorXY[$i]["class"]);
        }
    }

    /**
     * This function will test that the client side error messages will appear properly
     * on mobile mode. Message should appear below the input fields
     * Cory Nagy and MacKenzie Wilson
     * April, 2019
     */
    public function testUsernameAndPasswordErrorMessageDisplayedProperlyOnMobile()
    {
        $this->visit('/login');
        $page = $this->getCurrentPage();
        $driver = $this->getCurrentDriver();

        $this->minkSession->resizeWindow(575, 812);

        $page->find("css", "#_username")->setValue(str_repeat("Ab0", 51));
        $page->find("css", "#_password")->setValue(str_repeat("Ab0", 101));
        $page->find("css", "#login")->click();

        $xyInputCoor = <<<JS
        $('.form-control').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth()};
        }).get();
JS;

        // Store input coordinates in arrays
        $inputXY = $this->minkSession->evaluateScript($xyInputCoor);

        $xyErrorCoor = <<<JS
        $('div.alert.alert-danger').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth(),  class: $(element).attr("class")};
        }).get();
JS;
        // Store error elements in arrays
        $errorXY = $this->minkSession->evaluateScript($xyErrorCoor);

        // Get top input element to compare others to
        $errorXVal = $errorXY[0]["left"];

        $yVal = $inputXY[0]["top"];

        for ($i = 0; $i < count($errorXY); $i++) {
            // Assert that we're at the top of the input field
            $this->assertEquals($inputXY[$i]["top"], $yVal);

            // Add 38 to y-val to point us to the top of the error message
            $yVal += 54;

            $this->assertEquals($yVal, $errorXY[$i]["top"]);

            // Add padding height and height of error message
            $yVal += 106.1875;

            // Test that all errors are left aligned
            $this->assertEquals($errorXVal, $errorXY[$i]["left"]);

            // Test that error field contain a class of error_message
            $this->assertContains("alert alert-danger", $errorXY[$i]["class"]);
        }
    }

    /**
    * This test will make sure the banner is displayed below the navigation
    * bar with the proper text displayed on desktop for given pages in array
    * Cory Nagy and MacKenzie Wilson
    * April, 2019
    */
    public function testBannerIsDisplayedProperlyOnDesktop()
    {
        // Pages to visit
        $pagesToVisit = array("/show", "/about", "/registration", "/contact_us", "/login");
        // Text to assert when visiting each page
        $pageTitles = array("Our Shows", "About Us", "Member Registration", "Contact Us", "Log In");
        $index = 0;
        foreach ($pagesToVisit as $pageToCheck)
        {
            $this->visit($pageToCheck);
            $page = $this->getCurrentPage();
            $driver = $this->getCurrentDriver();

            $this->minkSession->resizeWindow(1000, 1040);

            $navCoor = <<<JS
        $('.navbar').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth(),  
          height: $(element).height(), class : $(element).attr("class")};
        }).get();
JS;
            $navXY = $this->minkSession->evaluateScript($navCoor);

            $bannerCoor = <<<JS
        $('.jumbotron').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth(),  
          height: $(element).outerHeight(), class : $(element).attr("class")};
        }).get();
JS;
            $bannerXY = $this->minkSession->evaluateScript($bannerCoor);

            $jumboTronTextCoor = <<<JS
        $('div.jumboText').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth(),  
          class : $(element).attr("class")};
        }).get();
JS;

            $jumboTextXY = $this->minkSession->evaluateScript($jumboTronTextCoor);

            // Assert that the jumbotron text is above the bottom of the banner
            $this->assertLessThan($bannerXY[0]["height"], $jumboTextXY[0]["top"]);

            // Get the height of the nav bar, which should be close
            // to the top of the banner. Adding 16 for padding.
            $topOfBanner = $navXY[0]["height"] + 16;

            // Assert that the bottom of the nav bar is at the top of the banner
            $this->assertEquals($topOfBanner, $bannerXY[0]["top"]);

            // Assert banner is proper size
            $this->assertEquals(270, $bannerXY[0]["height"]);

            // Assert banner header is on the page
            $jumboText = $page->find("css", ".jumboText")->getText();
            $this->assertEquals($pageTitles[$index], $jumboText);
            $index++;
        }


    }

    /**
     * This test will make sure the banner is displayed below the navigation
     * bar with the proper text displayed on mobile for given pages in array
     * Cory Nagy and MacKenzie Wilson
     * April, 2019
     */
    public function testBannerIsDisplayedProperlyOnMobile()
    {
        // Need to add contact page once it has been pushed , "/contact"
        $pagesToVisit = array("/show", "/about", "/registration", "/contact_us", "/login");
        // Text to assert when visiting each page
        $pageTitles = array("Our Shows", "About Us", "Member Registration", "Contact Us", "Log In");
        $index = 0;
        foreach ($pagesToVisit as $pageToCheck)
        {
            $this->visit($pageToCheck);
            $page = $this->getCurrentPage();
            $driver = $this->getCurrentDriver();

            $this->minkSession->resizeWindow(575, 812);

            $navCoor = <<<JS
        $('.navbar').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth(),  
          height: $(element).height(), class : $(element).attr("class")};
        }).get();
JS;
            $navXY = $this->minkSession->evaluateScript($navCoor);

            $bannerCoor = <<<JS
        $('.jumbotron').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth(),  
          height: $(element).outerHeight(), class : $(element).attr("class")};
        }).get();
JS;
            $bannerXY = $this->minkSession->evaluateScript($bannerCoor);

            $jumboTronTextCoor = <<<JS
        $('div.jumboText').map(function(index, element) {
          return {top : $(element).offset().top, left: $(element).offset().left, width: $(element).outerWidth(),  
          class : $(element).attr("class")};
        }).get();
JS;

            $jumboTextXY = $this->minkSession->evaluateScript($jumboTronTextCoor);

            // Assert that the jumbotron text is above the bottom of the banner
            $this->assertLessThan($bannerXY[0]["height"], $jumboTextXY[0]["top"]);

            // Get the height of the nav bar
            $topOfBanner = $navXY[0]["height"] + 16;

            // Assert that the bottom of the nav bar is at the top of the banner
            $this->assertEquals($topOfBanner, $bannerXY[0]["top"]);

            // Assert banner is proper size
            $this->assertEquals(270, $bannerXY[0]["height"]);

            // Assert banner header is on the page
            $jumboText = $page->find("css", ".jumboText")->getText();
            $this->assertEquals($pageTitles[$index], $jumboText);
            $index++;
        }
    }

    /**
     *  This function will verify that the regular expression is checking the
     *  entered password.
     * @author Cory and MacKenzie
     * April 10, 2019
     */
    public function testRegexForPassword()
    {
        $this->visit('/login');
        $page = $this->getCurrentPage();
        $driver = $this->getCurrentDriver();

        $this->minkSession->resizeWindow(1000, 1040);

        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("a");
        $page->find("css", "#login")->click();

        $errorText = $page->find("css", "div.alert.alert-danger")->getText();
        $this->assertEquals("Password must consist of one upper-case, one-lower-case, one number, and at least 6 characters in length", $errorText);
    }


    /**
     *  This function will verify that the regular expression is checking that
     *  the email is required
     * @author Cory and MacKenzie
     * April 10, 2019
     */
    public function testEmailIsRequired()
    {
        $this->visit('/login');
        $page = $this->getCurrentPage();
        $driver = $this->getCurrentDriver();

        $this->minkSession->resizeWindow(1000, 1040);

        $page->find("css", "#_username")->setValue("");
        $page->find("css", "#_password")->click();

        $errorText = $page->find("css", "div.alert.alert-danger")->getText();
        $this->assertEquals("Email is required", $errorText);
    }

    /**
     *  This function will verify that the regular expression is checking that
     *  the password is required
     * @author Cory and MacKenzie
     * April 10, 2019
     */
    public function testPasswordIsRequired()
    {
        $this->visit('/login');
        $page = $this->getCurrentPage();
        $driver = $this->getCurrentDriver();

        $this->minkSession->resizeWindow(1000, 1040);

        $page->find("css", "#_password")->setValue("");
        $page->find("css", "#_username")->click();

        $errorText = $page->find("css", "div.alert.alert-danger")->getText();
        $this->assertEquals("Password is required", $errorText);
    }

    /**
     * This test will test a valid login and redirection
     */
    public function testValidLogin()
    {
        $this->visit('/login');
        $page = $this->getCurrentPage();
        $driver = $this->getCurrentDriver();

        $this->minkSession->resizeWindow(1000, 1040);

        $page->find("css", "#_username")->setValue("samprj4reset@gmail.com");
        $page->find("css", "#_password")->setValue("P@ssw0rd");
        $page->find("css", "#login")->click();

        $this->assertEquals("http://localhost:8000/", $driver->getCurrentUrl());

        $this->assertEquals("Welcome to Saskatoon Summer Players", $page->find("css", ".jumboText h1")->getText());
    }



}
