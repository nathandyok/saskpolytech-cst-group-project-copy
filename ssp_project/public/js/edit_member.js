$(document).ready(function () {

    var phoneNumber = $("#member_update_phone").val();
    var areaCode = phoneNumber.substr(0, 3);
    var firstDigits = phoneNumber.substr(3, 3);
    var lastDigits = phoneNumber.substr(6, 4);

    if (phoneNumber !== "")
    {
        $("#member_update_phone").val("(" + areaCode + ") " + firstDigits + "-" + lastDigits);
    }


    // Great guide for JavaScript validation
    // http://javascript-coder.com/form-validation/jquery-form-validation-guide.phtml
    $("#updateForm").validate({
        // Rules for client side validation
        rules: {
            "member_update[firstName]": {
                required: true,
                maxlength: 20
            },
            "member_update[lastName]": {
                required: true,
                maxlength: 20
            },
            "member_update[userName]": {
                required: true,
                email: true,
                maxlength: 50
            },
            "member_update[phone]": {
                pattern: /^\(?\d{3}\)?\s?\d{3}\-?\d{4}$/
            },
            "member_update[addressLineOne]": {
                required: true,
                maxlength: 100
            },
            "member_update[addressLineTwo]": {
                maxlength: 100
            },
            "member_update[province]": {
                required: true
            },
            "member_update[city]": {
                required: true,
                maxlength: 100
            },
            "member_update[postalCode]": {
                required: true,
                pattern: /^[A-Za-z]\d[A-Za-z]\s?\d[A-Za-z]\d$/
            },
            "member_update[company]": {
                maxlength: 100
            },
        },

        // Error messages to be displayed
        messages: {
            "member_update[firstName]": {
                required: "First name is required",
                maxlength: "First name must be between 1-20 characters without spaces"
            },
            "member_update[lastName]": {
                required: "Last name is required",
                maxlength: "Last name must be between 1-20 characters without spaces"
            },
            "member_update[userName]": {
                required: "Email is required",
                maxlength: "Email exceeds 50 characters",
                email: "Email must be in standard email format"
            },
            "member_update[phone]": {
                pattern: "Please enter a 10-digit phone number"
            },
            "member_update[addressLineOne]": {
                required: "Address is required",
                maxlength: "Address can not exceed 100 characters"
            },
            "member_update[addressLineTwo]": {
                maxlength: "Address can not exceed 100 characters"
            },
            "member_update[province]": {
                required: "Please select a province"
            },
            "member_update[city]": {
                required: "City is required",
                maxlength: "City cannot exceed 100 characters"
            },
            "member_update[postalCode]": {
                required: "Postal code is required",
                pattern: "Postal Code should be in the format of A2A3B3 or A2A 3B3"
            },
            "member_update[company]": {
                maxlength: "Company cannot exceed 100 characters"
            },
        },

        // Applying classes and element types to fields
        errorClass: 'alert alert-danger',
        errorElement: 'div',

        onfocusout: function (element) {
            $(element).valid();
            $(element).first().removeClass("alert alert-danger");
        }

    });
});