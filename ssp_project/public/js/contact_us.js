$(document).ready(function () {

    $("#contact_ssp").validate({
        // Rules for client side validation
        rules: {
            "contact_ssp[name]": {
                required: true,
                maxlength: 40
            },
            "contact_ssp[email]": {
                email: true,
                maxlength: 50,
                require_from_group: [1, ".phone-group"]
            },
            "contact_ssp[phone]": {
                pattern: /^\(?\d{3}\)?\s?\d{3}\-?\d{4}$/,
                require_from_group: [1, ".phone-group"]
            },
            "contact_ssp[subject]": {
                required: true
            },
            "contact_ssp[comments]": {
                required: true
            },
        },

        messages: {
            "contact_ssp[name]": {
                required: "Name is required",
                maxlength: "Name must be 1-40 characters in length"
            },
            "contact_ssp[email]": {
                maxlength: "Email must be less than 50 characters without spaces",
                require_from_group: "Email address or phone number is required"
            },
            "contact_ssp[phone]": {
                pattern: "Please enter a 10-digit phone number",
                require_from_group: "Email address or phone number is required"
            },
            "contact_ssp[subject]": {
               required: "Choose an option from list"
            },
            "contact_ssp[comments]": {
                required: "Comment is required",
            },
        },

        errorClass: 'alert alert-danger',
        errorElement: 'div',

        onfocusout: function (element) {
            $(element).valid();
            $(element).first().removeClass("alert alert-danger");
        }
    });

    // Have window open on load if user has volunteered
    $(window).on('load', function () {
        $('#contactModal').modal('show');
    });

});