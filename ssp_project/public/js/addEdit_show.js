$(document).ready(function () {

    //FUTURE REFERENCE MAYBE
    // jQuery.validator.addMethod(
    //     "notEqualTo",
    //     function(elementValue,element,param) {
    //         return elementValue != param;
    //     },
    //     "Value cannot be {0}"
    // );
    // "show[date][date][year]": {
    //     notEqualTo: 2019
    // },

    // Great guide for JavaScript validation
    // http://javascript-coder.com/form-validation/jquery-form-validation-guide.phtml
    $("#showForm").validate({
        // Rules for client side validation
        rules: {
            "show[name]": {
                required: true,
                maxlength: 50
            },
            "show[budget]": {
                required: true,
                number: true,
                min: 1
            },
            "show[ticketPrice]": {
                required: true,
                number: true,
                min: 1
            },
            "show[synopsis]":{
                required: true
            },
            "show[pictureFile][file]": {
                extension: "png|jpe?g"
            },
            "show[ticketLink]": {
                maxlength: 100,
                url: true
            },
        },

        // Error messages to be displayed
        messages: {
            "show[name]": {
                required: "Show name is required",
                maxlength: "Show name must be between 1-50 characters"
            },
            "show[budget]": {
                required: "Budget is required",
                number: "Budget must be a number",
                min: "Budget must be at least 1 dollar"
            },
            "show[ticketPrice]": {
                required: "Ticket price is required",
                number: "Ticket price must be a number",
                min: "Ticket price must be at least 1 dollar"
            },
            "show[ticketLink]": {
                maxlength: "The ticket link must be no longer than 100 characters",
                url: "Ticket link must be a website url. For example https://www.saskatoonSummerPlayers.ca/"
            },
            "show[pictureFile][file]": {
                extension: "Invalid image file selected."
            },
            "show[synopsis]":{
                required: "Synopsis is required"
            }
        },

        // Applying classes and element types to fields
        errorClass: 'alert alert-danger',
        errorElement: 'div',

        onfocusout: function (element) {
            // Validate element on blur and remove error class from parent
            $(element).valid();
            $(element).first().removeClass("alert alert-danger");
        }
    });
});