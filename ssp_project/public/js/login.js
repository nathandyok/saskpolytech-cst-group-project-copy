$(document).ready(function () {

    // Have window open on load if user has volunteered
    $(window).on('load', function () {
        $('#justRegisteredModal').modal('show');
    });

    // This section will do client side validation for the username and password fields
    // on the login form.
    $('#loginForm').validate({
        /**
         *  Rules:
         *  - Username is required and a maximum length of 50 characters
         *  - Password is required, minimum length of 6 and maximum length of 100.
         *  - Password must have at least one lowercase, one uppercase, and one number
         */
        rules : {
            "_username" : {
                required : true,
                maxlength : 50
            },
            "_password" : {
                required : true,
                maxlength : 100,
                pattern : /^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z]).{6,}$/
            }
        },

        /**
         * Error messages to be displayed in case of error
         */
        messages : {
            "_username" : {
                required : "Email is required",
                maxlength : "Please enter a valid email address"
            },
            "_password" : {
                required : "Password is required",
                maxlength : "Must not exceed 100 characters",
                pattern : "Password must consist of one upper-case, one-lower-case, one number, and at least 6 characters in length"
            }
        },

        // Applying classes and element types to fields
        errorClass: 'alert alert-danger',
        errorElement: 'div',

        // Validate each element
        onfocusout: function (element) {
            $(element).valid();
            $(element).first().removeClass("alert alert-danger");
        }
    });
});