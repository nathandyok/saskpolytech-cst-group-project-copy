$.validator.addMethod("loginRegex", function(value, element) {
    return this.optional(element) || /^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z]).{6,}$/.test(value);
}, "Password must consist of one upper-case, one-lower-case, one number");

$(document).ready(function () {


    $("#memberReg").validate(
      {
          rules:
              {

                  "member[firstName]" :
                      {
                          maxlength: 20

                      },
                  "member[lastName]" :
                      {
                          maxlength: 20

                      },
                  "member[userName]" :
                      {
                            maxlength: 50
                      },
                  "member[password][first]":
                      {
                            loginRegex: true,
                            maxlength: 100
                      },
                  "member[password][second]":
                      {
                          loginRegex: true,
                          maxlength: 100,
                          equalTo: "#member_password_first"
                      },

                  "member[addressLineOne]":
                      {
                         maxlength: 100
                      },
                  "member[addressLineTwo]":
                      {
                          maxlength: 100
                      },
                  "member[city]":
                      {
                          maxlength: 100
                      },
                  "member[postalCode]":
                      {
                            pattern: /^[A-Za-z]\d[A-Za-z]\s?\d[A-Za-z]\d$/
                      },
                  "member[province]":
                      {
                          required: true
                      },
                  "member[company]":
                      {
                          maxlength: 100
                      },
                  "member[phone]":
                      {
                         pattern: /^\(\d{3}\)\s\d{3}\-\d{4}$/
                      }
              },
          messages: {
              "member[firstName]" :
                  {
                      required: "First Name is required",
                      maxlength: "First name must be between 1-20 characters without spaces"

                  },
              "member[lastName]" :
                  {
                      required: "Last Name is required",
                      maxlength: "Last name must be between 1-20 characters without spaces"
                  },
              "member[userName]" :
                  {
                      required: "Email is required",
                      email: "Email must be in standard email format",
                      maxlength: "Email can be no more than 50 characters"
                  },

              "member[password][first]" :
                  {
                      required: "Password is required",
                      loginRegex: "Password must consist of one upper-case, one-lower-case, one number",
                      maxlength: "Password must consist of 6-100 alpha characters"

                  },

              "member[password][second]" :
                  {
                      required: "Repeat Password is required",
                      loginRegex: "Password must consist of one upper-case, one-lower-case, one number",
                      maxlength: "Repeat Password must consist of 6-100 alpha characters",
                      equalTo: "Passwords must match."
                  },
              "member[addressLineOne]" :
                  {
                      required: "Address Line One is required",
                      maxlength:"Address Line One must be under 100 characters"

                  },
              "member[addressLineTwo]" :
                  {
                      maxlength:"AddressLineTwo must be under 100 characters"

                  },
              "member[city]" :
                  {
                      required: "City is required",
                      maxlength:"City must be under 100 characters"
                  },

              "member[postalCode]" :
                  {
                      required: "Postal code is required",
                      pattern: "Postal Code should be in the format of A2A3B3 or A2A 3B3"
                  },
              "member[province]" :
                  {
                      required: "Need to enter Province"
                  },
              "member[company]" :
                  {
                      maxlength:"Company must be under 100 characters"
                  },
              "member[phone]" :
                  {
                      pattern: "Please enter a 10-digit phone number"
                  }
          },

          // Applying classes and element types to fields
          errorClass: 'alert alert-danger block',
          errorElement: 'div',

          onfocusout: function (element) {
              $(element).valid();
              $(element).first().removeClass("alert alert-danger");
          }
      }

  )});


//http://javascript-coder.com/form-validation/jquery-form-validation-guide.phtml