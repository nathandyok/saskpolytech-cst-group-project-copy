<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use SebastianBergmann\CodeCoverage\Report\Text;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\AuditionDetailsRepository")
 */
class AuditionDetails
{
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $auditionDetails;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $directorMessage;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $howToAudition;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $synopsis;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $characterSummeries;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $noteFromDirector;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $auditionMaterials;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\SSPShow", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $show;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\member", inversedBy="auditionDetails")
     */
    private $membersAuditioned;

    /**
     * AuditionDetails constructor.
     * AuditionDetails is used for displaying information for auditions on a web page.
     * @param $auditionDetails - Specifics for the show such as time and place
     * @param $directorMessage - specific message from the director
     * @param $howToAudition - details on how to go about auditioning for the specific show
     * @param $synopsis - synopsis about the show to be displayed on the page
     * @param $characterSummeries - information on the specific characters that are a part of the show
     * @param $noteFromDirector - additional information that the director may want to share with members
     * @param $auditionMaterials - sheet music, etc. for the play that members will need
     * @param $show - the id of the show that this object is linked to
     */
    public function __construct($show, $auditionDetails='', $directorMessage='', $howToAudition='', $synopsis='', $characterSummeries='', $noteFromDirector='', $auditionMaterials='')
    {
        $this->show = $show;
        $this->auditionDetails = $auditionDetails;
        $this->directorMessage = $directorMessage;
        $this->howToAudition = $howToAudition;
        $this->synopsis = $synopsis;
        $this->characterSummeries = $characterSummeries;
        $this->noteFromDirector = $noteFromDirector;
        $this->auditionMaterials = $auditionMaterials;
        $this->membersAuditioned = new ArrayCollection();
    }

    public function getAuditionDetails(): ?string
    {
        return $this->auditionDetails;
    }

    public function setAuditionDetails(?string $auditionDetails): self
    {
        $this->auditionDetails = $auditionDetails;

        return $this;
    }

    public function getDirectorMessage(): ?string
    {
        return $this->directorMessage;
    }

    public function setDirectorMessage(?string $directorMessage): self
    {
        $this->directorMessage = $directorMessage;

        return $this;
    }

    public function getHowToAudition(): ?string
    {
        return $this->howToAudition;
    }

    public function setHowToAudition(?string $howToAudition): self
    {
        $this->howToAudition = $howToAudition;

        return $this;
    }

    public function getSynopsis(): ?string
    {
        return $this->synopsis;
    }

    public function setSynopsis(?string $synopsis): self
    {
        $this->synopsis = $synopsis;

        return $this;
    }

    public function getCharacterSummeries(): ?string
    {
        return $this->characterSummeries;
    }

    public function setCharacterSummeries(?string $characterSummeries): self
    {
        $this->characterSummeries = $characterSummeries;

        return $this;
    }

    public function getNoteFromDirector(): ?string
    {
        return $this->noteFromDirector;
    }

    public function setNoteFromDirector(?string $noteFromDirector): self
    {
        $this->noteFromDirector = $noteFromDirector;

        return $this;
    }

    public function getAuditionMaterials(): ?string
    {
        return $this->auditionMaterials;
    }

    public function setAuditionMaterials(?string $auditionMaterials): self
    {
        $this->auditionMaterials = $auditionMaterials;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getShow(): ?SSPShow
    {
        return $this->show;
    }

    public function setShow(SSPShow $show): self
    {
        $this->show = $show;

        return $this;
    }

    /**
     * @return Collection|member[]
     */
    public function getMembersAuditioned(): Collection
    {
        return $this->membersAuditioned;
    }

    public function addMembersAuditioned(member $membersAuditioned): self
    {
        if (!$this->membersAuditioned->contains($membersAuditioned)) {
            $this->membersAuditioned[] = $membersAuditioned;
        }

        return $this;
    }

    public function removeMembersAuditioned(member $membersAuditioned): self
    {
        if ($this->membersAuditioned->contains($membersAuditioned)) {
            $this->membersAuditioned->removeElement($membersAuditioned);
        }

        return $this;
    }
}
