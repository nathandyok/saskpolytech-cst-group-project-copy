<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ContactSSP
{
    /**
     * @Assert\NotBlank(
     *
     *      message = "Name must be 1-40 characters in length"
     *
     * )
     * @Assert\Length(
     *     min = 1,
     *     max = 20,
     *     minMessage = "Name must be 1-40 characters in length",
     *     maxMessage = "Name must be 1-40 characters in length"
     * )
     *
     * Name of the person contacting SSP will not be stored in the database
     */
    private $name;

    /**
     *
     * @Assert\Callback
     *
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if($this->getEmail() == "" && $this->getPhone() == "")
        {
            $context->buildViolation('Email address or phone number is required')
                ->atPath('email')
                ->addViolation();

            $context->buildViolation('Email address or phone number is required')
                ->atPath('phone')
                ->addViolation();
        }
    }

    /**
     *@Assert\Email(
     *      message = "Email must be in standard email format"
     * )
     * @Assert\Length(
     *     max = 40,
     *     maxMessage="Email cannot exceed 40 characters"
     * )
     */
    private $email;

    /**
     *@Assert\Regex(
     *     pattern="/^\(?\d{3}\)?\s?\d{3}\-?\d{4}$/",
     *     match=true,
     *     message="Please enter a 10-digit phone number"
     * )
     *
     *
     */
    private $phone;

    /**
     * @Assert\Choice(choices={"upcoming shows","past shows","ticket information","sponsor","donation","other"},
     * message="Choose an option from list")
     *
     */
    private $subject;

    /**
     *@Assert\NotBlank(
     *
     *      message = "Comment is required"
     *
     * )
     */
    private $comments;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(?string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getComments(): ?string
    {
        return $this->comments;
    }

    public function setComments(?string $comments): self
    {
        $this->comments = $comments;

        return $this;
    }
}
