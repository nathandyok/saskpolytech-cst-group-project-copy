<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MemberPaymentRepository")
 */
class MemberPayment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Member", inversedBy="memberPayments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $member;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(
     *      message = "Invalid payment authorization"
     * )
     * @Assert\Regex(
     *     pattern = "/^tok_[a-zA-Z0-9]{24}$/",
     *     match = true,
     *     message = "Something went wrong with the payment. Please try again."
     * )
     *
     *
     */
    private $stripe_token;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Regex(
     *     pattern = "/^cus_[a-zA-Z0-9]{14}$/",
     *     match = true,
     *     message = "Something went wrong with the payment. Please try again."
     * )
     *
     */
    private $stripe_customer_id;

    /**
     * @ORM\Column(type="float")
     * @Assert\GreaterThanOrEqual(1)
     * @Assert\LessThan(1000000)
     *
     */
    private $payment_amount;


    /*
     * This function will do calculations to get the payment amount in pennies for stripe to use
     * @author Dylan, Cory
     */
    public function convertAmountToPennies()
    {
        return round($this->payment_amount * 100, 0, PHP_ROUND_HALF_DOWN);
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMember(): ?Member
    {
        return $this->member;
    }

    public function setMember(?Member $member): self
    {
        $this->member = $member;

        return $this;
    }

    public function getStripeToken(): ?string
    {
        return $this->stripe_token;
    }

    public function setStripeToken(?string $stripe_token): self
    {
        $pattern = "/^tok_[a-zA-Z0-9]{24}$/";
        if (!preg_match($pattern, $stripe_token))
        {
            throw new \InvalidArgumentException("Something went wrong with the payment. Please try again.");
        }
        $this->stripe_token = $stripe_token;

        return $this;
    }

    public function getStripeCustomerId(): ?string
    {
        return $this->stripe_customer_id;
    }

    public function setStripeCustomerId(?string $stripe_customer_id): self
    {
        $pattern = "/^cus_[a-zA-Z0-9]{14}$/";
        if (!preg_match($pattern, $stripe_customer_id))
        {
            throw new \InvalidArgumentException("Something went wrong with the payment. Please try again.");
        }
        $this->stripe_customer_id = $stripe_customer_id;

        return $this;
    }

    public function getPaymentAmount(): ?float
    {
        return $this->payment_amount;
    }

    public function setPaymentAmount(?float $payment_amount): self
    {
        if ($payment_amount >= 1 && $payment_amount < 1000000)
        {
            $this->payment_amount = $payment_amount;
        }
        else{
            throw new \InvalidArgumentException("Payment amount must be greater than 1 and less than one million. Please try again.");
        }


        return $this;
    }
    
}
