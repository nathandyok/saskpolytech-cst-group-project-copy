<?php
/**
 * Created by PhpStorm.
 * User: cst229
 * Date: 1/9/2019
 * Time: 2:21 PM
 */

namespace App\DataFixtures;
use App\Entity\Member;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class MemberSearchFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $rick = new Member();
        $rick->setUserName("gmember@member.com");
        $rick->setPassword(password_hash("P@ssw0rd", PASSWORD_BCRYPT));
        $rick->setMembershipAgreement(true);
        $rick->setCity('Saskatoon');
        $rick->setPostalCode('s7k1a4');
        $rick->setProvince('SK');
        $rick->setAddressLineOne('111 There Drive');
        $rick->setFirstName("Rick");
        $rick->setLastName("Caron");
        $rick->setMemberType("Individual");
        $rick->setMemberGroups("Vice President");
        $rick->setMemberOption("Subscription");
        $rick->setCompany("SSP");
        $rick->setCriminalRecord(0);
        $rick->setRoles("ROLE_GM");
        $rick->setLastDatePaid('1548271590');
        $this->setReference('member-alpha', $rick);

        $bryce = new Member();
        $bryce->setUserName("bryce@gmail.com");
        $bryce->setPassword(password_hash("P@ssw0rd", PASSWORD_BCRYPT));
        $bryce->setMembershipAgreement(true);
        $bryce->setCity('Saskatoon');
        $bryce->setPostalCode('s7k1a4');
        $bryce->setProvince('SK');
        $bryce->setAddressLineOne('111 There Drive');
        $bryce->setFirstName("Bryce");
        $bryce->setLastName("Barrie");
        $bryce->setMemberType("Individual");
        $bryce->setMemberGroups("Member");
        $bryce->setMemberOption("Subscription");
        $bryce->setCompany("SSP");
        $bryce->setCriminalRecord(0);
        $bryce->setRoles("ROLE_MEMBER");
        $this->setReference('member-alpha', $bryce);


        $wade = new Member();
        $wade->setUserName("wade@gmail.com");
        $wade->setPassword(password_hash("P@ssw0rd", PASSWORD_BCRYPT));
        $wade->setMembershipAgreement(true);
        $wade->setCity('Saskatoon');
        $wade->setPostalCode('s7k1a4');
        $wade->setProvince('SK');
        $wade->setAddressLineOne('111 There Drive');
        $wade->setFirstName("Wade");
        $wade->setLastName("Lahoda");
        $wade->setMemberType("Individual");
        $wade->setMemberGroups("Member");
        $wade->setMemberOption("Subscription");
        $wade->setCompany("SSP");
        $wade->setCriminalRecord(0);
        $wade->setRoles("ROLE_MEMBER");
        $this->setReference('member-alpha', $wade);

        $rob = new Member();
        $rob->setUserName("rob@gmail.com");
        $rob->setPassword(password_hash("P@ssw0rd", PASSWORD_BCRYPT));
        $rob->setMembershipAgreement(true);
        $rob->setCity('Saskatoon');
        $rob->setPostalCode('s7k1a4');
        $rob->setProvince('SK');
        $rob->setAddressLineOne('111 There Drive');
        $rob->setFirstName("Rob");
        $rob->setLastName("Miller");
        $rob->setMemberType("Individual");
        $rob->setMemberGroups("Member");
        $rob->setMemberOption("Subscription");
        $rob->setCompany("SSP");
        $rob->setCriminalRecord(0);
        $rob->setRoles("ROLE_MEMBER");
        $this->setReference('member-alpha', $rob);

        $manager->persist($rick);
        $manager->persist($bryce);
        $manager->persist($wade);
        $manager->persist($rob);

        $manager->flush();
    }
}