<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190411142359 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE address (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, house_apart_num INTEGER NOT NULL, street_name VARCHAR(50) NOT NULL, city VARCHAR(50) NOT NULL, province VARCHAR(50) NOT NULL, postal_code VARCHAR(6) NOT NULL)');
        $this->addSql('CREATE TABLE audition_details (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, show_id INTEGER NOT NULL, audition_details CLOB DEFAULT NULL, director_message CLOB DEFAULT NULL, how_to_audition CLOB DEFAULT NULL, synopsis CLOB DEFAULT NULL, character_summeries CLOB DEFAULT NULL, note_from_director CLOB DEFAULT NULL, audition_materials CLOB DEFAULT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_898AC183D0C1FC64 ON audition_details (show_id)');
        $this->addSql('CREATE TABLE audition_details_member (audition_details_id INTEGER NOT NULL, member_id INTEGER NOT NULL, PRIMARY KEY(audition_details_id, member_id))');
        $this->addSql('CREATE INDEX IDX_819A71075ABEEC51 ON audition_details_member (audition_details_id)');
        $this->addSql('CREATE INDEX IDX_819A71077597D3FE ON audition_details_member (member_id)');
        $this->addSql('CREATE TABLE document (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title VARCHAR(255) NOT NULL, body CLOB DEFAULT NULL)');
        $this->addSql('CREATE TABLE member (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, first_name VARCHAR(20) NOT NULL, membership_agreement BOOLEAN NOT NULL, last_name VARCHAR(20) NOT NULL, user_name VARCHAR(50) NOT NULL, password VARCHAR(64) NOT NULL, member_type VARCHAR(50) NOT NULL, member_option VARCHAR(50) NOT NULL, last_date_paid INTEGER DEFAULT NULL, city VARCHAR(100) NOT NULL, postal_code VARCHAR(10) NOT NULL, province VARCHAR(100) NOT NULL, company VARCHAR(100) DEFAULT NULL, phone VARCHAR(20) DEFAULT NULL, address_line_one VARCHAR(100) NOT NULL, address_line_two VARCHAR(100) DEFAULT NULL, member_groups VARCHAR(100) DEFAULT NULL, criminal_record BOOLEAN DEFAULT NULL, roles VARCHAR(20) NOT NULL)');
        $this->addSql('CREATE TABLE member_payment (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, member_id INTEGER NOT NULL, stripe_token VARCHAR(255) NOT NULL, stripe_customer_id VARCHAR(255) DEFAULT NULL, payment_amount DOUBLE PRECISION NOT NULL)');
        $this->addSql('CREATE INDEX IDX_D2DC33997597D3FE ON member_payment (member_id)');
        $this->addSql('CREATE TABLE member_volunteer (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, member_id INTEGER NOT NULL, age INTEGER NOT NULL, volunteer_options CLOB DEFAULT NULL --(DC2Type:array)
        , additional_info CLOB DEFAULT NULL)');
        $this->addSql('CREATE INDEX IDX_1168081E7597D3FE ON member_volunteer (member_id)');
        $this->addSql('CREATE TABLE online_poll (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name CLOB DEFAULT NULL, description CLOB DEFAULT NULL, options CLOB DEFAULT NULL --(DC2Type:array)
        )');
        $this->addSql('CREATE TABLE password_reset (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, member_id INTEGER NOT NULL, recovery_value VARCHAR(255) NOT NULL, time_generated DATETIME NOT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B10172527597D3FE ON password_reset (member_id)');
        $this->addSql('CREATE TABLE sspshow (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, location_id INTEGER NOT NULL, name VARCHAR(50) NOT NULL, date DATETIME NOT NULL, ticket_price DOUBLE PRECISION DEFAULT NULL, picture_path VARCHAR(100) DEFAULT NULL, ticket_link VARCHAR(100) DEFAULT NULL, synopsis CLOB NOT NULL, end_date DATETIME DEFAULT NULL, status VARCHAR(50) DEFAULT NULL, budget DOUBLE PRECISION DEFAULT NULL)');
        $this->addSql('CREATE INDEX IDX_27C28C7564D218E ON sspshow (location_id)');
        $this->addSql('CREATE TABLE suggested_show (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, suggested_title VARCHAR(255) NOT NULL)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE audition_details');
        $this->addSql('DROP TABLE audition_details_member');
        $this->addSql('DROP TABLE document');
        $this->addSql('DROP TABLE member');
        $this->addSql('DROP TABLE member_payment');
        $this->addSql('DROP TABLE member_volunteer');
        $this->addSql('DROP TABLE online_poll');
        $this->addSql('DROP TABLE password_reset');
        $this->addSql('DROP TABLE sspshow');
        $this->addSql('DROP TABLE suggested_show');
    }
}
