<?php

namespace App\Form;

use App\Entity\ContactSSP;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\CallbackTransformer;

class ContactSSPType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('email', EmailType::class,array(    'required' => false, 'label' => 'Email'))
            ->add('phone', TelType::class, array(
                'required' => false,
                'attr' => array(
                    'placeholder' => '(xxx) xxx-xxxx'
                ),
                'label' => 'Phone (Optional)'
            ))
            ->add('subject', ChoiceType::class, array(
                'choices' =>array(
                    'Please select subject' =>'',
                    'Upcoming shows' => 'upcoming shows',
                    'Past Shows' => 'past shows',
                    'Ticket Information' => 'ticket information',
                    'Becoming a Sponsor' => 'sponsor',
                     'Donation' => 'donation',
                      'Other' => 'other',
                )
            ))
            ->add('comments', TextType::class, array('required' => true) )
            ->get('phone')
            ->addModelTransformer(new CallbackTransformer(
                function ($phoneForm){
                    return $phoneForm;
                },
                function ($phoneDB)
                {
                    return $phoneDB;
                }
            ))

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ContactSSP::class,
        ]);
    }
}
