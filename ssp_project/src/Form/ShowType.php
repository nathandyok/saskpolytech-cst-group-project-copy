<?php

namespace App\Form;

use App\Entity\SSPShow;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ShowType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,['label'=>'Show Name', 'label_attr' => ['class' =>'text-sm-right text-xs-left'], 'required' => true])
            ->add('date',  DateTimeType::class,['label'=>'Start Date', 'label_attr' => ['class' =>'text-sm-right text-xs-left']])
            ->add('budget', NumberType::class,['label'=>'Budget', 'label_attr' => ['class' =>'text-sm-right text-xs-left']])
            ->add('ticketPrice', NumberType::class,['label'=>'Ticket Price', 'label_attr' => ['class' =>'text-sm-right text-xs-left']])
            ->add('location', null, ['label'=>'Location', 'label_attr' => ['class' =>'text-sm-right text-xs-left']])
            ->add('synopsis', TextareaType::class,['label'=>'Synopsis', 'label_attr' => ['class' =>'text-sm-right text-xs-left'], 'required'=> true])
            ->add('pictureFile', VichImageType::class, ['label'=>'Image', 'label_attr' => ['class' =>'text-sm-right text-xs-left'],
                'required' => false, 'delete_label' => 'Delete the existing image?', 'download_label' => false, 'download_uri' => false, 'image_uri' => false])
            ->add('ticketLink',  TextType::class, ['required' => false, 'label'=>'Ticket Link', 'label_attr' => ['class' =>'text-sm-right text-xs-left']])
            ->add('endDate',  DateTimeType::class,['label'=>'End Date', 'label_attr' => ['class' =>'text-sm-right text-xs-left']])
            ->add('status', ChoiceType::class, ['choices' => [
                "No Status" => "No Status", //form will not accept an empty string as a status.... no idea why -Nathan
                'Sold Out' => 'Sold Out',
                'Archived' => 'archived',
                'New' => 'New'
            ], 'label'=>'Show Status', 'label_attr' => ['class' =>'text-sm-right text-xs-left']])
        ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SSPShow::class,
        ]);
    }
}
