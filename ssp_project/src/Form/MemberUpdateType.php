<?php

namespace App\Form;

use App\Entity\Member;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;


class MemberUpdateType extends AbstractType
{
    /** This function will build our form object for member registration page
     * that will be passed into the template
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('firstName', TextType::class,['label'=>'First Name', 'label_attr' => ['class' =>'label text-sm-right text-xs-left'], 'required'=>false ])
            ->add('lastName', TextType::class, ['label'=>'Last Name', 'label_attr' => ['class' =>'label text-sm-right text-xs-left'], 'required'=>false ])
            ->add('userName', EmailType::class,['label'=>'Email', 'label_attr' => ['class' =>'label text-sm-right text-xs-left'], 'required'=>false ])

            ->add('addressLineOne', TextType::class,['label'=>'Address Line One', 'label_attr' => ['class' =>'label text-sm-right text-xs-left'], 'required'=>false ])
            ->add('addressLineTwo', TextType::class, ['label'=>'Address Line Two (Optional)', 'label_attr' => ['class' =>'label text-sm-right text-xs-left'], 'required'=>false ])
            ->add('city', TextType::class, ['label'=>'City', 'label_attr' => ['class' =>'label text-sm-right text-xs-left'], 'required'=>false ])
            ->add('postalCode', TextType::class, ['label'=>'Postal Code', 'label_attr' => ['class' =>'label text-sm-right text-xs-left'], 'required'=>false ])
            ->add('province', ChoiceType::class, array(
                'choices' =>array(
                    'Please Enter a Province' =>'',
                    'Alberta' => 'AB',
                    'British Columbia' => 'BC',
                    'Manitoba' => 'MB',
                    'New Brunswick' => 'NB',
                    'Newfoundland and Labrador' => 'NL',
                    'Northwest Territories' => 'NT',
                    'Nova Scotia' => 'NS',
                    'Nunavut' => 'NU',
                    'Ontario' => 'ON',
                    'Prince Edward Island' => 'PE',
                    'Quebec' => 'QC',
                    'Saskatchewan' => 'SK',
                    'Yukon' => 'YT'
             ), "label_attr"=>["class" => "label text-sm-right text-xs-left"]
            ))
            ->add('company', TextType::class,['label'=>'Company (Optional)', 'label_attr' => ['class' =>'label text-sm-right text-xs-left'], 'required'=>false ])
            ->add('phone', TelType::class, ['label'=>'Phone (Optional)', 'label_attr' => ['class' =>'label text-sm-right text-xs-left'], 'attr'=> ['placeholder'=>'(xxx) xxx-xxxx'], 'required'=>false])

            ->get('phone')
                ->addModelTransformer(new CallbackTransformer(
                    function ($phoneForm){
                        return $phoneForm;
                    },
                    function ($phoneDB)
                    {
                        return $phoneDB;
                    }
                ))

            ;
    }

    /**
     * This method will link our form to our class that holds the
     * underlying data when embedding forms
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Member::class,
        ]);
    }
}
