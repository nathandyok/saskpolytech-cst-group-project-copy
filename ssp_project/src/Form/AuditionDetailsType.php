<?php

namespace App\Form;

use App\Entity\AuditionDetails;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class AuditionDetailsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('auditionDetails',TextareaType::class,['label'=>'Audition Details', 'label_attr' => ['class' =>'text-sm-right text-xs-left'], 'required'=>false ])
            ->add('directorMessage',TextareaType::class,['label'=>'Director Message', 'label_attr' => ['class' =>'text-sm-right text-xs-left'], 'required'=>false ])
            ->add('howToAudition',TextareaType::class,['label'=>'How To Audition', 'label_attr' => ['class' =>'text-sm-right text-xs-left'], 'required'=>false ])
            ->add('synopsis',TextareaType::class,['label'=>'Synopsis', 'label_attr' => ['class' =>'text-sm-right text-xs-left'], 'required'=>false ])
            ->add('characterSummeries',TextareaType::class,['label'=>'Character Summaries', 'label_attr' => ['class' =>'text-sm-right text-xs-left'], 'required'=>false ])
            ->add('noteFromDirector',TextareaType::class,['label'=>'Note From Director', 'label_attr' => ['class' =>'text-sm-right text-xs-left'], 'required'=>false ])
            ->add('auditionMaterials',TextareaType::class,['label'=>'Audition Materials', 'label_attr' => ['class' =>'text-sm-right text-xs-left'], 'required'=>false ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AuditionDetails::class,
        ]);
    }
}
