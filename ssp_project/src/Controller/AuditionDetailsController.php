<?php

namespace App\Controller;

use App\Entity\AuditionDetails;
use App\Entity\Member;
use App\Entity\SSPShow;
use App\Form\AuditionDetailsType;
use App\Repository\AuditionDetailsRepository;
use function Sodium\add;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\Tools\Pagination\Paginator;


/**
 * @Route("/auditiondetails")
 */
class AuditionDetailsController extends AbstractController
{
    /**
     * @Route("/", name="audition_details_index", methods="GET")
     */
    public function index(): Response
    {
        //Call the voters to check if membership time is up
        if (!$this->isGranted("activeMember", new Member()))
        {
            //Membership is over a year old so log the user out and redirects to the homepage.
            return $this->redirectToRoute("logout");
        }

        return $this->render('audition_details/index.html.twig');
    }

    /**
     * @Route("/member/audition/", name="audition_details_show", methods="GET")
     */
    public function show(Request $request): Response
    {
        //Call the voters to check if membership time is up
        if (!$this->isGranted("activeMember", new Member()))
        {
            //Membership is over a year old so log the user out and redirects to the homepage.
            return $this->redirectToRoute("logout");
        }

        $showID = $request->query->get('showID');
        $memberObj = $this->getUser();
        $show = $this->getDoctrine()->getRepository(SSPShow::class)->find($showID);

        $arrayOfAuditionDetails = $this->getDoctrine()->getRepository(AuditionDetails::class)->findBy(array('show'=>$showID));

        $alreadyAuditioned = false;

        if($arrayOfAuditionDetails == null)
        {
            $auditionDetail = null;
        }
        else
        {
            $auditionDetail = $arrayOfAuditionDetails[0];
            $auditionArray = $auditionDetail->getMembersAuditioned();
            if ($auditionArray->contains($memberObj))
            {
                $alreadyAuditioned = true;
            }
        }

        $form = $this->createForm(AuditionDetailsType::class, $auditionDetail);
        $form->handleRequest($request);

        return $this->render('audition_details/show.html.twig', [
            'show' => $show,
            'audition_detail' => $auditionDetail,
            'already_auditioned' => $alreadyAuditioned
        ]);
    }

    /**
     * @Route("/admin/edit/", name="audition_details_edit", methods="GET|POST")
     */
    public function edit(Request $request): Response
    {
        //Call the voters to check if membership time is up
        if (!$this->isGranted("activeMember", new Member()))
        {
            //Membership is over a year old so log the user out and redirects to the homepage.
            return $this->redirectToRoute("logout");
        }

        $id = $request->query->get('showID');
        $show = $this->getDoctrine()->getRepository(SSPShow::class)->find($id);

        $arrayOfAuditionDetails = $this->getDoctrine()->getRepository(AuditionDetails::class)->findBy(array('show'=>$id));
        $auditionDetail = $arrayOfAuditionDetails[0];

        $form = $this->createForm(AuditionDetailsType::class, $auditionDetail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($auditionDetail);
            $entityManager->flush();
            return $this->redirectToRoute('edit_index');
        }
        return $this->render('audition_details/edit.html.twig', [
            'show' => $show,
            'audition_detail' => $auditionDetail,
            'form' => $form->createView(),
        ]);
    }

    /**
     * This function will write an audition interest object to the database for the specified show with the logged in user's member ID.
     *
     * @param Request
     * @return Response
     * @Route("/signup/", name="audition_to_db", methods="GET|POST")
     */
    public function addAuditionRequest(Request $request) : Response
    {
        $mID = $this->getUser()->getId();
        $member = $this->getDoctrine()->getRepository(Member::class)->findOneBy(array('id'=>$mID));

        //Retrieve ID
        $id = $request->query->get('showID');

        $show = $this->getDoctrine()->getRepository(SSPShow::class)->find($id);

        $arrayOfAuditionDetails = $this->getDoctrine()->getRepository(AuditionDetails::class)->findBy(array('show'=>$id));
        $auditionDetail = $arrayOfAuditionDetails[0];
        $auditionDetail->addMembersAuditioned($member);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($auditionDetail);
        $entityManager->flush();

        return $this->render('audition_details/show.html.twig', [
            'show' => $show,
            'audition_detail' => $auditionDetail,
            'already_auditioned' => true
        ]);
    }

    /**
     * This function will list out the members that have applied to audition for a specific show
     *
     * @return Response
     * @param AuditionDetailsRepository $auditionDetailsRepository
     * @Route("/board/applicants/", name="audition_interest_list", methods="GET|POST")
     */
    public function auditionInterestList(Request $request) : Response
    {
        //Call the voters to check if membership time is up
        if (!$this->isGranted("activeMember", new Member()))
        {
            //Membership is over a year old so log the user out and redirects to the homepage.
            return $this->redirectToRoute("logout");
        }

        if($this->getUser() != null)
        {
            $mID = $this->getUser()->getId();
            $member = $this->getDoctrine()->getRepository(Member::class)->findOneBy(array('id'=>$mID));

            if (!$this->isGranted("ROLE_BM", $member ) && !$this->isGranted("ROLE_GM", $member ))
            {
                //Membership is over a year old so log the user out and redirects to the homepage.
                return $this->redirectToRoute("home_page");
            }
        }
        else
        {
            //Membership is over a year old so log the user out and redirects to the homepage.
            return $this->redirectToRoute("login");
        }

        $arrayOfAuditionDetails = $this->getDoctrine()->getRepository(AuditionDetails::class)->findAll();


        //Getting the value from the select box for the show's id
        //The twig deals with null values, which only occurs when no ID is provided. The user is prompted to select a show in that case.
        $showID = $request->query->get('selectShow');

        //Retrieve the show and related audition detail and pass it to the twig template
        $show = $this->getDoctrine()->getRepository(SSPShow::class)->findOneBy(array('id'=>$showID));
        $auditionDetail = $this->getDoctrine()->getRepository(AuditionDetails::class)->findOneBy(array('show'=>$show));

        return $this->render('audition_details/list.html.twig', [
            'audition_detail_list' => $arrayOfAuditionDetails,
            'audition_detail' => $auditionDetail
        ]);
    }
}
