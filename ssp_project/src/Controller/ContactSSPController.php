<?php

namespace App\Controller;

use App\Entity\ContactSSP;
use App\Form\ContactSSPType;
use App\Repository\ContactSSPRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/contact_us")
 */
class ContactSSPController extends AbstractController
{
    /**
     * @Route("/", name="contactSSP", methods={"GET","POST"})
     */
    public function new(Request $request, \Swift_Mailer $mailer): Response
    {
        $contactSSP = new ContactSSP();
        $form = $this->createForm(ContactSSPType::class, $contactSSP);
        $form->handleRequest($request);

        //Variable  for contact us modal
        $contactUsRequest = $request->query->get('contactUsRequest');

        if ($form->isSubmitted() && $form->isValid()) {

            $emailAddress = "samprj4reset@gmail.com";
            $SSPEmail = $this->getParameter('MAILER_URL');
            $SSPEmail = explode('/', $SSPEmail)[2];
            $SSPEmail = explode(':', $SSPEmail)[0];

            $userComment = strtolower($form->get('comments')->getData());
            $userSubject = strtolower($form->get('subject')->getData());
            $userPhone = strtolower($form->get('phone')->getData());
            $userEmail = strtolower($form->get('email')->getData());
            $userName = strtolower($form->get('name')->getData());

            $message = (new \Swift_Message($userSubject))
                ->setFrom($emailAddress)
                ->setTo($SSPEmail)
                ->setBody(
                    '<html>' .
                    ' <body>' .
                    "Name: " . $userName .
                    "Phone: " . $userPhone .
                    "Email: " . $userEmail .
                    "Comment: " . $userComment.
                    ' </body>' .
                    '</html>',
                    'text/html' // Mark the content-type as HTML
                );


            $mailer->send($message, $failures);
            //$this->addFlash('success', 'Thank you for contacting us!');
           return $this->redirectToRoute('contactSSP', ['contactUsRequest' => true]);
        }


        return $this->render('contact_ssp/_form.html.twig', [
            'contactSSP' => $contactSSP,
            'form' => $form->createView(),
            'contactUsRequest' => $contactUsRequest

        ]);
    }
}
