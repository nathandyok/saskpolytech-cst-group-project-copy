<?php

namespace App\Repository;

use App\Entity\AuditionInterest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AuditionInterest|null find($id, $lockMode = null, $lockVersion = null)
 * @method AuditionInterest|null findOneBy(array $criteria, array $orderBy = null)
 * @method AuditionInterest[]    findAll()
 * @method AuditionInterest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuditionInterestRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AuditionInterest::class);
    }

    // /**
    //  * @return AuditionInterest[] Returns an array of AuditionInterest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AuditionInterest
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
