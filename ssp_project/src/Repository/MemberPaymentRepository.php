<?php

namespace App\Repository;

use App\Entity\MemberPayment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MemberPayment|null find($id, $lockMode = null, $lockVersion = null)
 * @method MemberPayment|null findOneBy(array $criteria, array $orderBy = null)
 * @method MemberPayment[]    findAll()
 * @method MemberPayment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MemberPaymentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MemberPayment::class);
    }

    // /**
    //  * @return MemberPayment[] Returns an array of MemberPayment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MemberPayment
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
