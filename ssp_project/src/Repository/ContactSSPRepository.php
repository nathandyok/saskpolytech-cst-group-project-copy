<?php

namespace App\Repository;

use App\Entity\ContactSSP;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ContactSSP|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContactSSP|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContactSSP[]    findAll()
 * @method ContactSSP[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactSSPRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ContactSSP::class);
    }

    // /**
    //  * @return ContactSSP[] Returns an array of ContactSSP objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ContactSSP
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
