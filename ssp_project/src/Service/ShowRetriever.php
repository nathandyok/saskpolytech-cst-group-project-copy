<?php

namespace App\Service;

use App\Repository\ShowRepository;

/**
 * Class ShowRetriever
 *
 * This class contains functions that will retreive show objects from the database.
 *
 * @package App\Service
 *
 */
class ShowRetriever
{

    // -1 returns past shows
    // 0 returns current shows
    // 1 returns future shows
    public function RetrieveShow(ShowRepository $showRepository, $showResponse)
    {
        $returnArray = array();
        $currentDay = new \DateTime('now');

        switch ($showResponse)
        {
            case 0:
            case 1:
                $returnArray = $showRepository->createQueryBuilder('S')
                    ->where('S.endDate > :dateToCheck')
                    ->andWhere('S.status != :archived')
                    ->setParameter('dateToCheck', $currentDay)
                    ->setParameter('archived', "archived")
                    ->orderBy('S.endDate', 'ASC')
                    ->getQuery()
                    ->execute();
                break;

            case -1:
                $returnArray = $showRepository->createQueryBuilder('S')
                    ->where('S.endDate < :dateToCheck')
                    ->andWhere('S.status != :archived')
                    ->setParameter('dateToCheck', $currentDay)
                    ->setParameter('archived', "archived")
                    ->orderBy('S.endDate', 'DESC')
                    ->getQuery()
                    ->execute();
                break;
        }

        return $returnArray;
    }

}