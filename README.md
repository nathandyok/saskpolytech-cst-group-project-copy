# README #

## Saskatoon Summer Players - Website Project ##

Project root: G:/ssp_project
Naming conventions: Symfony default for folder structure, Capital class names, camelCase variables and functions

.gitignore: Follow standards Symfony created

Branch naming: 	lowercase with story number followed by description, spaces as underscore 
		ie. 1a_member_logs_in


Team Members: 
	Taylor Beverly
	Cory Nagy
	Christopher Boechler
	Dylan Sies
	MacKenzie Wilson
	Kate Zawada
	Nathan Dyok